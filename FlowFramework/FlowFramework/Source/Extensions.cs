﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace FlowFramework
{
    public static partial class Extensions
    {
        /// <summary>
        /// コピーを生成する処理
        /// </summary>
        /// <typeparam name="T">型</typeparam>
        /// <param name="a">コピー元対象</param>
        /// <returns>コピー結果対象</returns>
        public static T DeepClone<T>(this T a)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, a);
                stream.Position = 0;

                return (T)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Enumメンバ名で値を解析する処理
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="value">文字列</param>
        /// <param name="result">解析結果</param>
        /// <returns>解析成功結果</returns>
        public static bool GetEnumByName<TEnum>(string value, out TEnum result) where TEnum : struct, Enum
        {
            result = default;

            return (Enum.GetNames(typeof(TEnum)).Any(v => v == value)
                && (Enum.TryParse(value, out result)));
        }

        /// <summary>
        /// 生バイト配列から文字列を生成する処理
        /// </summary>
        /// <param name="source">バイト配列</param>
        /// <param name="encoding">エンコーディング</param>
        /// <returns>文字列</returns>
        public static string DecodeNativeBuffer(this byte[] source, Encoding encoding)
        {
            // 生バイト配列がnullの場合、例外を発生
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            // エンコーディングがnullの場合、例外を発生
            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            try
            {
                // 生バイトをエンコーディング
                var decoded = encoding.GetString(source);

                // 終端文字以降を切り捨て
                if (-1 != decoded.IndexOf('\0'))
                {
                    decoded = decoded.Substring(0, decoded.IndexOf('\0'));
                }

                return decoded;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 文字列をバイト配列に変換する処理
        /// </summary>
        /// <param name="backingField">バイト配列</param>
        /// <param name="encoding">エンコーディング</param>
        /// <param name="value">文字列</param>
        /// <param name="throwOnBufferTooSmall">バッファーが短い時に例外を発生する振る舞いを有効するフラグ</param>
        public static void EncodeToNativeBuffer(this byte[] backingField, Encoding encoding, string value, bool throwOnBufferTooSmall = false)
        {
            // バイト配列がnullの場合、例外を発生
            if (backingField == null)
            {
                throw new ArgumentNullException(nameof(backingField));
            }

            // エンコーディングがnullの場合、例外を発生
            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            // 文字列がnullの場合、バイト配列をクリア
            if (value == null)
            {
                Array.Clear(backingField, 0, backingField.Length);
            }
            // 上記以外は文字列をバイト配列に変換
            else
            {
                int requiredSize = encoding.GetByteCount(value);
                if (backingField.Length < requiredSize)
                {
                    if (throwOnBufferTooSmall)
                    {
                        throw new ArgumentException(
                            $"Buffer too small ({backingField.Length}) to fit the required {requiredSize} byte(s).",
                            nameof(backingField));
                    }
                    else
                    {
                        Array.Clear(backingField, 0, backingField.Length);
                    }
                }
                else
                {
                    int written = encoding.GetBytes(value, 0, value.Length, backingField, 0);
                    Array.Clear(backingField, written, backingField.Length - written);
                }
            }
        }

        /// <summary>
        /// UIツリーに親コントロールを検索する処理
        /// </summary>
        /// <typeparam name="T">型</typeparam>
        /// <param name="child">親コントロール型</param>
        /// <returns>親コントロール</returns>
        public static T FindParentOfType<T>(this DependencyObject child) where T : DependencyObject
        {
            DependencyObject parentDepObj = child;

            do
            {
                // 子コントロールから親コントロールを取得
                parentDepObj = VisualTreeHelper.GetParent(parentDepObj);
                T parent = parentDepObj as T;

                if (parent != null)
                {
                    return parent;
                }
            }
            while (parentDepObj != null);

            return null;
        }

        /// <summary>
        /// Finds a Child of a given item in the visual tree. 
        /// </summary>
        /// <param name="parent">A direct parent of the queried item.</param>
        /// <typeparam name="T">The type of the queried item.</typeparam>
        /// <param name="childName">x:Name or Name of child. </param>
        /// <returns>The first parent item that matches the submitted type parameter. 
        /// If not matching item can be found, 
        /// a null parent is being returned.</returns>
        public static T FindFirstChild<T>(this DependencyObject parent, string childName)
           where T : DependencyObject
        {
            // Confirm parent and childName are valid. 
            if (parent == null)
            {
                return null;
            }

            T foundChild = null;

            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);

            for (int i = 0; i < childrenCount; i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);

                // If the child is not of the request child type child
                T childType = child as T;
                if (childType == null)
                {
                    // recursively drill down the tree
                    foundChild = child.FindFirstChild<T>(childName);

                    // If the child is found, break so we do not overwrite the found child. 
                    if (foundChild != null)
                    {
                        break;
                    }
                }
                else if (false == string.IsNullOrEmpty(childName))
                {
                    var frameworkElement = child as FrameworkElement;

                    // If the child's name is set for search
                    if ((frameworkElement != null) && (frameworkElement.Name == childName))
                    {
                        // if the child's name is of the request name
                        foundChild = (T)child;
                        break;
                    }
                }
                else
                {
                    // child element found.
                    foundChild = (T)child;
                    break;
                }
            }

            return foundChild;
        }

        public static TimeSpan Min(TimeSpan t1, TimeSpan t2)
        {
            return (t1 < t2 ? t1 : t2);
        }

        public static TimeSpan Max(TimeSpan t1, TimeSpan t2)
        {
            return (t1 > t2 ? t1 : t2);
        }

        /// <summary>
        /// オーバーフローしないDateTimeとTimeSpanの加減
        /// </summary>
        /// <param name="date">日付</param>
        /// <param name="value">TimeSpan</param>
        /// <returns>日付</returns>
        public static DateTime AddSafe(this DateTime date, TimeSpan value)
        {
            Debug.Assert(date.Kind != DateTimeKind.Unspecified);

            long ticks = Math.Max(DateTime.MinValue.Ticks, Math.Min(DateTime.MaxValue.Ticks, date.Ticks + value.Ticks));

            return new DateTime(ticks, date.Kind);
        }

        public static bool TryAdd<K, V>(this IDictionary<K, V> dic, K key, V value)
        {
            if (true == dic.ContainsKey(key))
            {
                return false;
            }

            dic.Add(key, value);

            return true;
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source == null) throw new ArgumentNullException("source");
            if (action == null) throw new ArgumentNullException("action");

            foreach (var item in source)
            {
                action(item);
            }
        }

        public static string NormalizeLength(this string value, int maxLength)
        {
            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string NormalizeLengthEnd(this string value, int maxLength)
        {
            return value.Length <= maxLength ? value : value.Substring(value.Length - maxLength, maxLength);
        }

        public static bool CopyTo(this SecureString source, SecureString target)
        {
            if (source == null)
            {
                throw new ArgumentNullException("s1");
            }
            if (target == null)
            {
                throw new ArgumentNullException("s2");
            }

            if (target.IsReadOnly())
            {
                return false;
            }

            IntPtr ss_bstr1_ptr = IntPtr.Zero;

            try
            {
                ss_bstr1_ptr = Marshal.SecureStringToBSTR(source);

                string str1 = Marshal.PtrToStringBSTR(ss_bstr1_ptr);

                target.Clear();
                foreach (char s in str1)
                {
                    target.AppendChar(s);
                }

                return true;
            }
            finally
            {
                if (ss_bstr1_ptr != IntPtr.Zero)
                {
                    Marshal.ZeroFreeBSTR(ss_bstr1_ptr);
                }
            }
        }

        public static string AsString(this SecureString source)
        {
            string res = string.Empty;

            IntPtr res_ptr = IntPtr.Zero;

            try
            {
                res_ptr = Marshal.SecureStringToBSTR(source);

                res = Marshal.PtrToStringBSTR(res_ptr);
            }
            finally
            {
                if (res_ptr != IntPtr.Zero)
                {
                    Marshal.ZeroFreeBSTR(res_ptr);
                }
            }

            return res;
        }

        public static bool EqualTo(this SecureString source, SecureString target)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }
            if (target == null)
            {
                return false;
            }

            if (source.Length != target.Length)
            {
                return false;
            }

            return source.AsString().Equals(target.AsString());
        }
    }
}
