﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowFramework
{
    public class FlowException : Exception
    {
        public FlowException(string message, Exception innerException = null)
            : base(message, innerException)
        {
        }
    }

    public class FlowException<TEnum> : FlowException where TEnum : struct, Enum
    {
        public TEnum ErrorCode { get; }

        public FlowException(TEnum errorCode, Exception innerException = null)
            : base($"Error code: {errorCode}", innerException)
        {
            ErrorCode = errorCode;
        }
    }
}
