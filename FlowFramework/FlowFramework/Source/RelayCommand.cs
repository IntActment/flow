﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FlowFramework
{
    /// <summary>
    /// ICommandを実装するクラス
    /// </summary>
    public class RelayCommandBase : ICommand
    {
        private readonly Predicate<object> m_canExecute;
        private readonly Action<object> m_execute;

        private bool m_inAction;

        public bool InAction => m_inAction;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="execute">コマンドが実行するAction</param>
        /// <param name="lockExecute">コマンド実行中で無効させるフラグ</param>
        public RelayCommandBase(Action<object> execute, bool lockExecute = false)
        {
            m_execute = execute;
            m_canExecute = (o) => (false == lockExecute) || (false == m_inAction);
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="execute">コマンドが実行するAction</param>
        /// <param name="canExecute">コマンドが実行可能かどうかを表すPredicate<object></param>
        /// <param name="lockExecute">コマンド実行中で無効させるフラグ</param>
        public RelayCommandBase(Action<object> execute, Predicate<object> canExecute, bool lockExecute = false)
        {
            m_execute = execute;
            m_canExecute = (o) => ((false == lockExecute) || (false == m_inAction)) && (true == canExecute(o));
        }

        /// <summary>
        /// コマンドが実行可能かどうかを取得します
        /// </summary>
        /// <param name="parameter">パラメータ</param>
        /// <returns>結果</returns>
        public bool CanExecute(object parameter)
        {
            Debug.Assert(null != m_canExecute);

            return m_canExecute(parameter);
        }

        private event EventHandler m_canExecuteChanged;

        /// <summary>
        /// コマンドが実行可能かどうかイベント
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
                m_canExecuteChanged += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
                m_canExecuteChanged -= value;
            }
        }

        /// <summary>
        /// 有効/無効状態を手動で更新する処理
        /// </summary>
        public virtual void Invalidate()
        {
            m_canExecuteChanged?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// コマンドを実行します
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            try
            {
                m_inAction = true;
                Invalidate();
                m_execute?.Invoke(parameter);
            }
            catch
            {
                throw;
            }
            finally
            {
                m_inAction = false;
                Invalidate();
            }
        }
    }

    /// <summary>
    /// ViewModelがViewに公開するコマンドを表します
    /// </summary>
    public sealed class RelayCommand : RelayCommandBase
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="execute">コマンドが実行するAction</param>
        /// <param name="lockExecute">コマンド実行中で無効させるフラグ</param>
        public RelayCommand(Action execute, bool lockExecute = false)
            : base((o) => execute(), lockExecute)
        {

        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="execute">コマンドが実行するAction</param>
        /// <param name="canExecute">コマンドが実行可能かどうかを表すFunc<bool></param>
        /// <param name="lockExecute">コマンド実行中で無効させるフラグ</param>
        public RelayCommand(Action execute, Func<bool> canExecute, bool lockExecute = false)
            : base((o) => execute(), (o) => canExecute(), lockExecute)
        {

        }

        /// <summary>
        /// コマンドが実行可能かどうかを取得します
        /// </summary>
        /// <returns>結果</returns>
        public bool CanExecute()
        {
            return CanExecute(null);
        }

        /// <summary>
        /// コマンドを実行します
        /// </summary>
        public void Execute()
        {
            Execute(null);
        }
    }

    /// <summary>
    /// ViewModelがViewに公開するコマンドを表します
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class RelayCommand<T> : RelayCommandBase
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="execute">コマンドが実行するAction</param>
        /// <param name="lockExecute">コマンド実行中で無効させるフラグ</param>
        public RelayCommand(Action<T> execute, bool lockExecute = false)
            : base((o) => execute((T)o), lockExecute)
        {

        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="execute">コマンドが実行するAction</param>
        /// <param name="canExecute">コマンドが実行可能かどうかを表すPredicate<T></param>
        /// <param name="lockExecute">コマンド実行中で無効させるフラグ</param>
        public RelayCommand(Action<T> execute, Predicate<T> canExecute, bool lockExecute = false)
            : base((o) => execute((T)o), (o) => canExecute((T)o), lockExecute)
        {

        }

        /// <summary>
        /// コマンドが実行可能かどうかを取得します
        /// </summary>
        /// <param name="parameter">パラメータ</param>
        /// <returns>結果</returns>
        public bool CanExecute(T parameter)
        {
            return CanExecute((object)parameter);
        }

        /// <summary>
        /// コマンドを実行します
        /// </summary>
        /// <param name="parameter">パラメータ</param>
        public void Execute(T parameter)
        {
            Execute((object)parameter);
        }
    }
}
