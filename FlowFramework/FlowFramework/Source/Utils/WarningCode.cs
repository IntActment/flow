﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using static FlowFramework.Utils.Message;

namespace FlowFramework.Utils
{
    /// <summary>
    /// ボタン組み合わせ
    /// </summary>
    [Flags]
    public enum WarningButtons : uint
    {
        Close = WarningButton.Close,
        Continue = WarningButton.Continue,
        Cancel = WarningButton.Cancel,
        CancelContinue = Cancel | Continue,
        CancelClose = Cancel | Close,
        Yes = WarningButton.Yes,
        No = WarningButton.No,
        OK = WarningButton.OK,
        YesNo = Yes | No,
        YesNoCancel = Yes | No | Cancel,
    }

    /// <summary>
    /// 警告コードクラス
    /// </summary>
    public class WarningCode
    {
        /// <summary>
        /// 説明テキスト
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// ボタン組み合わせ
        /// </summary>
        public WarningButtons Buttons { get; set; } = WarningButtons.Close;

        /// <summary>
        /// 警告コード対象を検索する処理
        ///  [Strings/WarningCodes.xaml]で検索
        /// </summary>
        /// <param name="key">警告リソース名</param>
        /// <returns>警告コード対象</returns>
        public static WarningCode Find(string key)
        {
            // キーをもとに表示するワーニングコードを取得
            object obj = WarningCodeDictionary.WarningCodes[key];
            if (null == obj)
            {
                throw new KeyNotFoundException($"key \"{key}\" was not found!");
            }

            // ワーニングコードを取得し返す
            WarningCode ec = obj as WarningCode;
            if (null == ec)
            {
                throw new InvalidCastException($"key \"{key}\" type is {obj.GetType()}, not an WarningCode!");
            }

            return ec;
        }
    }
}
