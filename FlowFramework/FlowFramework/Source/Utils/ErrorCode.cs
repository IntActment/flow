﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using static FlowFramework.Utils.Message;

namespace FlowFramework.Utils
{
    [Flags]
    public enum ErrorButtons : uint
    {
        Close = ErrorButton.Close,
        OK = ErrorButton.OK,
        Retry = ErrorButton.Retry,
        Cancel = ErrorButton.Cancel,
        CloseRetry = Close | Retry,
        RetryCancel = Retry | Cancel,
        OKRetry = OK | Retry,
        Shutdown = ErrorButton.Shutdown,
        ShutdownRetry = Shutdown | Retry,
        Continue = ErrorButton.Continue,
        ShutdownContinue = Shutdown | Continue,
        ShutdownIgnoreRetry = Shutdown | Ignore | Retry,
        Ignore = ErrorButton.Ignore,
        IgnoreRetry = Ignore | Retry,
        IgnoreShutdown = Ignore | Shutdown,
    }

    public class ErrorCode
    {
        /// <summary>
        /// エラー説明テキスト内容
        /// </summary>
        public string Description { get; set; }

        public ErrorButtons Buttons { get; set; } = ErrorButtons.Close;

        /// <summary>
        /// [Strings/ErrorCodes.xaml]で検索
        /// </summary>
        /// <param name="key">エラーコードキー</param>
        /// <returns>エラーコード記録（検索失敗の場合：null）</returns>
        public static ErrorCode Find(string key)
        {
            object obj = ErrorCodeDictionary.ErrorCodes[key];
            if (null == obj)
            {
                throw new KeyNotFoundException($"key \"{key}\" was not found!");
            }

            ErrorCode ec = obj as ErrorCode;
            if (null == ec)
            {
                throw new InvalidCastException($"key \"{key}\" type is {obj.GetType()}, not an ErrorCode!");
            }

            return ec;
        }
    }
}
