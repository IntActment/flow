﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

using FlowFramework.Controls;
using FlowFramework.View;

namespace FlowFramework.Utils
{
    /// <summary>
    /// エラー・警告ダイアログを表示するクラス
    /// </summary>
    public static class Message
    {
        /// <summary>
        /// エラーダイアログのボタン定義
        /// </summary>
        public enum ErrorButton : uint
        {
            /// <summary>
            /// 閉じる
            /// </summary>
            Close = 1,

            /// <summary>
            /// OK
            /// </summary>
            OK = 2,

            /// <summary>
            /// 再試行
            /// </summary>
            Retry = 4,

            /// <summary>
            /// アプリ終了
            /// </summary>
            Shutdown = 8,

            /// <summary>
            /// 続行
            /// </summary>
            Continue = 16,

            /// <summary>
            /// 無視
            /// </summary>
            Ignore = 32,

            /// <summary>
            /// キャンセル
            /// </summary>
            Cancel = 64,
        }

        /// <summary>
        /// 警告ダイアログのボタン定義
        /// </summary>
        public enum WarningButton : uint
        {
            /// <summary>
            /// 閉じる
            /// </summary>
            Close = 1,

            /// <summary>
            /// 続行
            /// </summary>
            Continue = 2,

            /// <summary>
            /// キャンセル
            /// </summary>
            Cancel = 4,

            /// <summary>
            /// はい
            /// </summary>
            Yes = 8,

            /// <summary>
            /// いいえ
            /// </summary>
            No = 16,

            /// <summary>
            /// OK
            /// </summary>
            OK = 32,
        }

        /// <summary>
        /// エラーダイアログのモーダル表示処理
        /// </summary>
        /// <param name="descriptionFormat">エラー説明テキスト（フォーマット）</param>
        /// <param name="buttons">ボタン組み合わせ</param>
        /// <param name="descParams">エラー説明テキスト（フォーマット）のパラメータ</param>
        /// <returns>ダイアログ表示結果</returns>
        public static ErrorButton ShowError(string descriptionFormat, ErrorButtons buttons, params object[] descParams)
        {
            return UI.Run(() =>
            {
                // エラーダイアログの表示
                return ErrorDialog.ShowError(descriptionFormat, buttons, descParams);
            });
        }

        /// <summary>
        /// エラーダイアログのモーダル表示処理
        /// </summary>
        /// <param name="errorCode">エラーコード対象</param>
        /// <param name="descParams"></param>
        /// <returns>ダイアログ表示結果</returns>
        public static ErrorButton ShowError(ErrorCode errorCode, params object[] descParams)
        {
            // エラーダイアログの表示
            return ShowError(errorCode.Description, errorCode.Buttons, descParams);
        }

        /// <summary>
        /// 警告ダイアログのモーダル表示処理
        /// </summary>
        /// <param name="descriptionFormat">ワーニング説明テキスト（フォーマット）</param>
        /// <param name="buttons">ボタン組み合わせ</param>
        /// <param name="descParams">ワーニング説明テキスト（フォーマット）のパラメータ</param>
        /// <returns>ダイアログ表示結果</returns>
        public static WarningButton ShowWarning(string descriptionFormat, WarningButtons buttons, params object[] descParams)
        {
            return UI.Run(() =>
            {
                // ワーニングダイアログの表示
                return WarningDialog.ShowWarning(descriptionFormat, buttons, descParams);
            });
        }

        /// <summary>
        /// 警告ダイアログのモーダル表示処理
        /// </summary>
        /// <param name="warnCode">ワーニングコード対象</param>
        /// <param name="descParams">ワーニング説明テキスト（フォーマット）のパラメータ</param>
        /// <returns></returns>
        public static WarningButton ShowWarning(WarningCode warnCode, params object[] descParams)
        {
            // ワーニングダイアログの表示
            return ShowWarning(warnCode.Description, warnCode.Buttons, descParams);
        }
    }
}
