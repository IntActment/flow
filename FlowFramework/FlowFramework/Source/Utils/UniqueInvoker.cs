﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace FlowFramework.Utils
{
    /// <summary>
    /// インヴォーカーインタフェース
    /// </summary>
    public interface IInvoker
    {
        /// <summary>
        /// トークンを登録する処理
        /// </summary>
        /// <param name="newToken">トークン対象</param>
        /// <returns>トークンノード</returns>
        LinkedListNode<CancelToken> NewToken(CancelToken newToken);

        /// <summary>
        /// トークンでキャンセルする処理
        /// </summary>
        /// <param name="node">トークンノード</param>
        /// <returns>処理結果</returns>
        bool Cancel(LinkedListNode<CancelToken> node);
    }

    /// <summary>
    /// キャンセルトークンインタフェース
    /// </summary>
    public interface ICancelToken
    {
        /// <summary>
        /// キャンセル要求あり/なしフラグ
        /// </summary>
        bool IsCancelRequested { get; }

        /// <summary>
        /// キャンセル要求あり状態で特別例外を発生する処理
        /// </summary>
        void CheckAndThrow();
    }

    /// <summary>
    /// キャンセルトークンクラス
    /// </summary>
    public class CancelToken : ICancelToken
    {
        /// <summary>
        /// エラーコード
        /// </summary>
        public enum ErrorCode
        {
            /// <summary>
            /// エラーなし
            /// </summary>
            None = 0,

            /// <summary>
            /// キャンセル
            /// </summary>
            Cancelled = 1,
        }

        private object m_flagLock = new object();

        private bool m_isCancelRequested = false;

        private IInvoker m_invoker;

        private LinkedListNode<CancelToken> m_node;

        private ErrorCode m_errorCode = ErrorCode.None;

        /// <summary>
        /// キャンセル要求あり/なしフラグ
        /// </summary>
        public bool IsCancelRequested
        {
            get { lock (m_flagLock) return m_isCancelRequested; }
        }

        /// <summary>
        /// キャンセル要求あり状態で特別例外を発生する処理
        /// </summary>
        public void CheckAndThrow()
        {
            lock (m_flagLock)
            {
                if ((true == m_isCancelRequested) && (m_errorCode == ErrorCode.None))
                {
                    m_errorCode = ErrorCode.Cancelled;
                    throw new FlowException<ErrorCode>(ErrorCode.Cancelled);
                }
            }
        }

        /// <summary>
        /// トークンでキャンセルする処理
        /// </summary>
        public bool Cancel()
        {
            lock (m_flagLock)
            {
                if (true == m_isCancelRequested)
                {
                    return false;
                }

                m_isCancelRequested = true;
            }

            return m_invoker.Cancel(m_node);
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="invoker">インウォーカーインスタンス</param>
        public CancelToken(IInvoker invoker)
        {
            m_invoker = invoker ?? throw new ArgumentNullException(nameof(invoker));
            m_node = invoker.NewToken(this);
        }
    }

    /// <summary>
    /// インウォーカークラス
    /// </summary>
    public class UniqueInvoker : IInvoker
    {
        /// <summary>
        /// 優先
        /// </summary>
        public enum Priority
        {
            /// <summary>
            /// 普通
            /// </summary>
            Normal = 0,

            /// <summary>
            /// 高い
            /// </summary>
            High = 1,
        }

        public delegate void Method(ICancelToken cancelToken);
        public delegate Task AsyncMethod(ICancelToken cancelToken);

        private LinkedList<CancelToken> m_cancelTokenStack;
        private object m_stackLock = new object();

        private Priority m_priority = Priority.Normal;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public UniqueInvoker()
        {
            m_cancelTokenStack = new LinkedList<CancelToken>();
        }

        /// <summary>
        /// 渡されたデリゲートを実行する処理
        /// </summary>
        /// <param name="priority">優先</param>
        /// <param name="method">処理対象</param>
        /// <returns>キャンセルトークン対象</returns>
        public CancelToken Invoke(Priority priority, Method method)
        {
            return Invoke_Internal(priority, (token) => { method(token); return Task.CompletedTask; });
        }

        /// <summary>
        /// 渡されたデリゲートを実行する処理
        /// </summary>
        /// <param name="priority">優先</param>
        /// <param name="method">処理対象</param>
        /// <returns>キャンセルトークン対象</returns>
        public CancelToken Invoke(Priority priority, AsyncMethod method)
        {
            return Invoke_Internal(priority, async (token) => await method(token));
        }

        /// <summary>
        /// 渡されたデリゲートを実行する処理
        /// </summary>
        /// <param name="priority">優先</param>
        /// <param name="method">処理対象</param>
        /// <returns>キャンセルトークン対象</returns>
        private CancelToken Invoke_Internal(Priority priority, AsyncMethod method)
        {
            LinkedListNode<CancelToken> lastTokenNode;
            lock (m_stackLock)
            {
                lastTokenNode = m_cancelTokenStack.Last;
            }

            if (null != lastTokenNode)
            {
                // 優先度がノーマルの場合は処理中断
                if (priority == Priority.Normal)
                {
                    Console.WriteLine($"Operation was cancelled due to priority reason ({priority} vs {m_priority}).");
                    return null;
                }

                lastTokenNode.Value.Cancel();
            }

            m_priority = priority;

            CancelToken newToken;
            LinkedListNode<CancelToken> node;

            lock (m_stackLock)
            {
                newToken = new CancelToken(this);
                node = m_cancelTokenStack.Last;
            }

            // 優先度がノーマル以外の場合(ハイ)は処理を実行する
            Dispatcher.CurrentDispatcher.Invoke(new Func<Task>(async () =>
            {
                if (newToken.IsCancelRequested)
                {
                    return;
                }

                try
                {
                    await method(newToken);
                }
                catch (FlowException<CancelToken.ErrorCode>)
                {

                }
                finally
                {
                    newToken.Cancel();
                }
            }));

            return newToken;
        }

        /// <summary>
        /// トークンを登録する処理
        /// </summary>
        /// <param name="newToken">トークン対象</param>
        /// <returns>トークンノード</returns>
        LinkedListNode<CancelToken> IInvoker.NewToken(CancelToken newToken)
        {
            lock (m_stackLock)
            {
                return m_cancelTokenStack.AddLast(newToken);
            }
        }

        /// <summary>
        /// トークンでキャンセルする処理
        /// </summary>
        /// <param name="node">トークンノード</param>
        /// <returns>処理結果</returns>
        bool IInvoker.Cancel(LinkedListNode<CancelToken> node)
        {
            lock (m_stackLock)
            {
                if (null != node.List)
                {
                    // トークンを削除する。(要求をキャンセルする。)
                    Debug.Assert(node.List == m_cancelTokenStack);

                    m_cancelTokenStack.Remove(node);

                    return true;
                }

                return false;
            }
        }
    }
}
