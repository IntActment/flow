﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace FlowFramework.Utils
{
    public class ErrorCodeDictionary : SharedResourceDictionary
    {
        public static ErrorCodeDictionary ErrorCodes { get; } = new ErrorCodeDictionary();

        public ErrorCodeDictionary()
        {
            if ((ErrorCodes != this) && (ErrorCodes != null))
            {
                ErrorCodes.MergedDictionaries.Add(this);
            }
        }
    }
}
