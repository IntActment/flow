﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace FlowFramework.Utils
{
    public class WarningCodeDictionary : SharedResourceDictionary
    {
        public static WarningCodeDictionary WarningCodes { get; } = new WarningCodeDictionary();

        public WarningCodeDictionary()
        {
            if ((WarningCodes != this) && (WarningCodes != null))
            {
                WarningCodes.MergedDictionaries.Add(this);
            }

        }
    }
}
