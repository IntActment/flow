﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FlowFramework.Converters
{
    [ValueConversion(typeof(int), typeof(string))]
    public class TimePickerConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((byte)value).ToString("D2");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte val;
            byte.TryParse((string)value, out val);

            return val;
        }
    }
}
