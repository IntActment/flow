﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FlowFramework.Converters
{
    /// <summary>
    /// int ⇒ string
    /// </summary>
    [ValueConversion(typeof(int), typeof(string))]
    public class IntToStringConverter : IValueConverter
    {
        /// <summary>
        /// 変換処理
        /// </summary>
        /// <param name="value">int値</param>
        /// <param name="targetType">型</param>
        /// <param name="parameter">未使用</param>
        /// <param name="culture">未使用</param>
        /// <returns>文字列</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // スピンボックスの数値を文字列に変換するために使用
            return value.ToString();
        }

        /// <summary>
        /// 逆変換処理
        /// </summary>
        /// <param name="value">string値</param>
        /// <param name="targetType">型</param>
        /// <param name="parameter">未使用</param>
        /// <param name="culture">未使用</param>
        /// <returns>int数字</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (true == int.TryParse((string)value, out int val))
            {
                return val;
            }

            return 0;
        }
    }
}
