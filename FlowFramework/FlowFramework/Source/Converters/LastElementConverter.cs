﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FlowFramework.Converters
{

    [ValueConversion(typeof(IList), typeof(object))]
    class LastElementConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            IList collection = value as IList;

            if (null == collection)
            {
                throw new InvalidCastException("value is not a collection");
            }

            if (collection.Count == 0)
            {
                return null;
            }

            return collection[collection.Count - 1];
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }
}
