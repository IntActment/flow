﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace FlowFramework.Converters
{
    /// <summary>
    /// bool ⇒ Visibility（Visible・Collapsed）
    /// </summary>
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class VisibilityCollapsedConverter : IValueConverter
    {
        /// <summary>
        /// 変換処理
        /// </summary>
        /// <param name="value">値</param>
        /// <param name="targetType">型</param>
        /// <param name="parameter">未使用</param>
        /// <param name="culture">未使用</param>
        /// <returns>Visibility</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool bValue)
            {
                return bValue == true ? Visibility.Visible : Visibility.Collapsed;
            }
            else if ((value is Enum) && (parameter is string param) && (Enum.IsDefined(value.GetType(), param)))
            {
                return Equals(value, Enum.Parse(value.GetType(), param)) ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                return Equals(value, parameter) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        /// <summary>
        /// 未使用
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
