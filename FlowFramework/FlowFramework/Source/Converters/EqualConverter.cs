﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FlowFramework.Converters
{
    /// <summary>
    /// 同一チェック
    /// </summary>
    [ValueConversion(typeof(object), typeof(bool))]
    public class EqualConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType.IsEnum)
            {
                return (int)value == (int)parameter;
            }

            return (int)value == int.Parse((string)parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }

    /// <summary>
    /// 同一チェック
    /// </summary>
    [ValueConversion(typeof(object), typeof(bool))]
    public class NotEqualConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !object.Equals(value, parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }
}
