﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FlowFramework.Converters
{
    /// <summary>
    /// (double)value / (double[2])parameter ⇒ double
    /// </summary>
    [ValueConversion(typeof(double), typeof(double))]
    class DivConverter : IValueConverter
    {
        /// <summary>
        /// 変換処理
        /// </summary>
        /// <param name="value">値</param>
        /// <param name="targetType">型</param>
        /// <param name="parameter">パラメータ</param>
        /// <param name="culture">ロケール</param>
        /// <returns>変換結果</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // フォントサイズから幅を算出するために使用

            // object型をdouble型に変換
            double div1 = (double)value;

            // object型をstring型に変換
            var parameters = ((string)parameter).Split(',');

            // parametersが空の場合
            if (parameters.Length < 1)
            {
                throw new InvalidOperationException();
            }

            // parametersの先頭をdouble型に変換
            double div2 = double.Parse(parameters[0]);

            // 0を返さないように、parameter[1]を返す
            if ((0 == div1) || (0 == div2))
            {
                if (parameters.Length == 2)
                {
                    return double.Parse(parameters[1]);
                }
            }

            return div1 / div2;
        }

        /// <summary>
        /// 未使用
        /// </summary>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }

    /// <summary>
    /// コンバータクラス
    /// </summary>
    public class MultipleDivConverter : IMultiValueConverter
    {
        /// <summary>
        /// 変換処理
        /// </summary>
        /// <param name="values">値配列</param>
        /// <param name="targetType">型</param>
        /// <param name="parameter">未使用</param>
        /// <param name="culture">未使用</param>
        /// <returns>変換結果</returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            // フォントサイズから幅を算出するために使用
            // return values[0] / values[1]

            double actualParentSize = (double)values[0];
            int count = (int)values[1];

            if (0 == count)
            {
                throw new Exception();
            }

            return actualParentSize / (double)count;
        }

        /// <summary>
        /// 未使用
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
