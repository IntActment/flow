﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

using FlowFramework.Model;

namespace FlowFramework.Converters
{
    class ValueWrapperConverter<T, WrapperDerived> : TypeConverter where WrapperDerived : ValueWrapperBase<T>
    {
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return false;
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if ((typeof(T) == sourceType) || (sourceType == typeof(string)))
            {
                return true;
            }

            return base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if ((null == value) && (true == ValueWrapperMetadata.Defaults.TryGetValue(typeof(WrapperDerived), out object val)))
            {
                return val;
            }
            else if (true == ValueWrapperMetadata.Constructors.TryGetValue(typeof(WrapperDerived), out Func<string, object> ctor))
            {
                return ctor((string)value);
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if ((typeof(T) == destinationType) || (destinationType == typeof(string)))
            {
                return true;
            }

            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if ((null == value) && (true == ValueWrapperMetadata.DefaultValues.TryGetValue(typeof(WrapperDerived), out object val)))
            {
                return val;
            }
            else if (value is WrapperDerived tVal)
            {
                return tVal.GetRawValueString();
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    public class ValueWrapperTypeDescriptor : CustomTypeDescriptor
    {
        private Type m_valueWrapperDerivedType;

        public ValueWrapperTypeDescriptor(Type valueWrapperDerivedType)
        {
            m_valueWrapperDerivedType = valueWrapperDerivedType;
        }

        public override TypeConverter GetConverter()
        {
            Type type = m_valueWrapperDerivedType;
            Type baseType;

            while ((baseType = type.BaseType) != typeof(object))
            {
                if ((true == baseType.IsGenericType) && (baseType.GetGenericTypeDefinition() == typeof(ValueWrapperBase<>)))
                {
                    var genericT = baseType.GetGenericArguments()[0];
                    var converterType = typeof(ValueWrapperConverter<,>).MakeGenericType(genericT, m_valueWrapperDerivedType);

                    return (TypeConverter)Activator.CreateInstance(converterType);
                }

                type = type.BaseType;
            }

            throw new InvalidCastException();
        }
    }

    public class ValueWrapperDescriptionProvider : TypeDescriptionProvider
    {
        public override ICustomTypeDescriptor GetTypeDescriptor(Type valueWrapperDerivedType, object instance)
        {
            return new ValueWrapperTypeDescriptor(valueWrapperDerivedType);
        }
    }
}
