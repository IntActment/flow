﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

using FlowFramework.Model;

namespace FlowFramework.Converters
{
    /// <summary>
    /// bool値を反対する
    /// </summary>
    [ValueConversion(typeof(bool), typeof(bool))]
    class BoolInvertConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }
}
