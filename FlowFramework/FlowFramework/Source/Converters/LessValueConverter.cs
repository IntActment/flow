﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FlowFramework.Converters
{
    /// <summary>
    /// 先頭アイテム値は最小値かどうかチェック
    /// </summary>
    [ValueConversion(typeof(object), typeof(bool))]
    class LessValueConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double val = (double)values.First();
            bool res = values.All(x => (val < (double)x));

            return res;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// value < parameter
    /// </summary>
    [ValueConversion(typeof(double), typeof(bool))]
    class LessValueSingleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (double)value < double.Parse((string)parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }

    /// <summary>
    /// value < parameter
    /// </summary>
    [ValueConversion(typeof(int), typeof(bool))]
    class GreaterIntValueSingleConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (int)value > int.Parse((string)parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }
}