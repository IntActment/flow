﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FlowFramework.Converters
{
    /// <summary>
    /// UniformGridの列数を計算するクラス
    /// </summary>
    public class UniformGridConverterCol : IMultiValueConverter
    {
        /// <summary>
        /// 変換処理
        /// </summary>
        /// <param name="values">値配列</param>
        /// <param name="targetType">型</param>
        /// <param name="parameter">範囲文字列”最小値~最大値”</param>
        /// <param name="culture">未使用</param>
        /// <returns>変換結果</returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double actualParentSize = (double)values[0];    // 枠コントロール（枠の親）の幅
            int count = (int)values[1];                     // 全体枠数

            if (0 == count)
            {
                return 0;
            }

            string[] maxmin = ((string)parameter).Split('~');

            double min = double.Parse(maxmin[0].Trim());    // 最小幅
            double max = double.Parse(maxmin[1].Trim());    // 最大幅

            // 全体枠数と親コントロールの幅によって、一枠の幅を計算する
            double calcWidth = (int)(actualParentSize / (double)count);

            if (calcWidth < min)
            {
                // 最小値以下の場合
                count = (int)(actualParentSize / min);
            }
            else if (calcWidth > max)
            {
                // 最大値以上の場合
                count = (int)(actualParentSize / max);
            }

            // 一行分の枠数
            return count;
        }

        /// <summary>
        /// 未使用
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// 1枠の幅を計算するクラス
    /// </summary>
    public class UniformGridConverterSize : IMultiValueConverter
    {
        /// <summary>
        /// 変換処理
        /// </summary>
        /// <param name="values">値配列</param>
        /// <param name="targetType">型</param>
        /// <param name="parameter">範囲文字列”最小値~最大値”</param>
        /// <param name="culture">未使用</param>
        /// <returns>変換結果</returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double actualParentSize = (double)values[0];    // 枠コントロール（枠の親）の幅
            int count = (int)values[1];                     // 全体枠数

            if (0 == count)
            {
                throw new Exception();
            }

            string[] maxmin = ((string)parameter).Split('~');

            double min = double.Parse(maxmin[0].Trim());    // 最小幅
            double max = double.Parse(maxmin[1].Trim());    // 最大幅

            // 全体枠数と親コントロールの幅によって、一枠の幅を計算する
            double calcWidth = (int)(actualParentSize / (double)count);

            if (calcWidth < min)
            {
                // 最小値以下の場合
                return min;
            }
            else if (calcWidth > max)
            {
                // 最大値以上の場合
                return max;
            }

            // 1枠の幅
            return calcWidth;
        }

        /// <summary>
        /// 未使用
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
