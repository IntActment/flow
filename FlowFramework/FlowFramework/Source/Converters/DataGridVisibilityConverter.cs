﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace FlowFramework.Converters
{
    /// <summary>
    /// DataGridHeadersVisibility ⇒ Visibility
    /// </summary>
    [ValueConversion(typeof(DataGridHeadersVisibility), typeof(Visibility))]
    class DataGridVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (((DataGridHeadersVisibility)value == DataGridHeadersVisibility.Column)
                || (DataGridHeadersVisibility)value == DataGridHeadersVisibility.All) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }
}
