﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace FlowFramework.Converters
{
    /// <summary>
    /// 複数値の同一チェック
    /// </summary>
    [ValueConversion(typeof(object), typeof(bool))]
    public class MultipleEqualConverter : IMultiValueConverter
    {
        /// <summary>
        /// 変換処理
        /// </summary>
        /// <param name="values">値配列</param>
        /// <param name="targetType">型</param>
        /// <param name="parameter">未使用</param>
        /// <param name="culture">未使用</param>
        /// <returns>変換結果</returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            // ポート番号の重複チェックで使用
            object val = values.First();
            bool res = values.All(x => object.Equals(x, val));

            return res;
        }

        /// <summary>
        /// 未使用
        /// </summary>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
