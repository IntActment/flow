﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

using FlowFramework.Model;

namespace FlowFramework.Converters
{
    /// <summary>
    /// DateTime ⇒ string (HH : mm : ss)
    /// </summary>
    [ValueConversion(typeof(DateTime), typeof(string))]
    public class DateTimeHourMinuteStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime dt = (DateTime)value;
            return $"{dt.Hour:D2}:{dt.Minute:D2}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }

    /// <summary>
    /// DateTime ⇒ string (HH : mm : ss)
    /// </summary>
    [ValueConversion(typeof(DateTime), typeof(string))]
    public class DateTimeHourMinuteSecondStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime dt = (DateTime)value;
            return  $"{dt.Hour:D2}:{dt.Minute:D2}:{dt.Second:D2}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }

    public class DateTimeStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime dt = (DateTime)value;
            return $"[ {dt.Year}/{dt.Month}/{dt.Day} {dt.Hour:D2}:{dt.Minute:D2}:{dt.Second:D2} ]";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }

    /// <summary>
    /// DateTime ⇒ string (YYYY年 M月 D日 )
    /// </summary>
    [ValueConversion(typeof(DateTime), typeof(string))]
    public class DateTimeMonthDayStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime di = (DateTime)value;
            return di.Year + "年 " + di.Month + "月 " + di.Day + "日";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            // 無効
            return null;
        }
    }
}
