﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace FlowFramework.Converters
{
    /// <summary>
    /// (double) ⇒ d,0,0,0
    /// </summary>
    [ValueConversion(typeof(double), typeof(Thickness))]
    public class ThicknessLeftConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            return new Thickness(System.Convert.ToDouble(value), 0, 0, 0);
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// (double) ⇒ 0,d,0,0
    /// </summary>
    [ValueConversion(typeof(double), typeof(Thickness))]
    public class ThicknessTopConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            return new Thickness(0, System.Convert.ToDouble(value), 0, 0);
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// (double) ⇒ 0,0,d,0
    /// </summary>
    [ValueConversion(typeof(double), typeof(Thickness))]
    public class ThicknessRightConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            return new Thickness(0, 0, System.Convert.ToDouble(value), 0);
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }

    /// <summary>
    /// (double) ⇒ 0,0,0,d
    /// </summary>
    [ValueConversion(typeof(double), typeof(Thickness))]
    public class ThicknessBottomConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            return new Thickness(0, 0, 0, System.Convert.ToDouble(value));
        }

        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
