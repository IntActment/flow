﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace FlowFramework.ViewModel
{
    public interface IPassCode
    {
        SecureString PassCode { get; }

        void Clear();
    }
}
