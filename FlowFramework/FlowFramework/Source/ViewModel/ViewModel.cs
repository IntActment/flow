﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

using FlowFramework.Utils;
using FlowFramework.View;

namespace FlowFramework.ViewModel
{
    /// <summary>
    /// ウィンドウライフタイムと紐づくインタフェース
    /// </summary>
    public interface IViewModel
    {
        /// <summary>
        /// オーナーウィンドウを指定する処理
        /// </summary>
        /// <param name="windowBase">オーナーウィンドウ</param>
        void SetWindowOwner(WindowBase windowBase);
    }

    /// <summary>
    /// ウィンドウライフタイムと紐づくクラス
    /// </summary>
    public abstract class ViewModel<Win> : NotifyStatic, IViewModel where Win : WindowBase, new()
    {
        /// <summary>
        /// オーナーウィンドウを指定する処理
        /// </summary>
        /// <param name="windowBase">オーナーウィンドウ</param>
        void IViewModel.SetWindowOwner(WindowBase windowBase)
        {
            Debug.Assert(null != windowBase);

            Window = (Win)windowBase;

            Debug.Assert(null != Window);

            Window.Loaded += WindowLoaded;
            Window.Closed += WindowClosed;
        }

        /// <summary>
        /// メインウィンドウインスタンス
        /// </summary>
        public Win Window { get; set; }

        /// <summary>
        /// ウィンドウを閉じるイベント処理
        /// </summary>
        /// <param name="sender">センダー</param>
        /// <param name="e">イベントパラメータ</param>
        private void WindowClosed(object sender, EventArgs e)
        {
            OnUnloaded();

            if (null != Window)
            {
                Window.Loaded -= WindowLoaded;
                Window.Closed -= WindowClosed;
            }
        }

        /// <summary>
        /// ウィンドウロード終了イベント処理
        /// </summary>
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            OnLoaded();
        }

        /// <summary>
        /// ウィンドウロード終了イベント処理
        /// </summary>
        protected abstract void OnLoaded();

        /// <summary>
        /// ウィンドウアンロード終了イベント処理
        /// </summary>
        protected abstract void OnUnloaded();
    }
}
