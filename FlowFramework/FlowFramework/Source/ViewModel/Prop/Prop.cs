﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FlowFramework.Utils;

namespace FlowFramework.ViewModel.Prop
{
    /// <summary>
    /// プローパティ対象クラス
    /// </summary>
    /// <typeparam name="T">データ型</typeparam>
    public class Prop<T> : NotifyStatic
    {
        protected static readonly Type TYPE = typeof(T);

        public delegate void PropSetter(ref T oldVal, in T value);

        private T m_value;

        private T m_initValue;

        /// <summary>
        /// セッター処理
        /// </summary>
        public PropSetter Setter { get; set; } = (ref T oldVal, in T newVal) => oldVal = newVal;

        /// <summary>
        /// データ変更イベント処理
        /// </summary>
        protected virtual void OnChange()
        {
            // 何もしない
        }

        /// <summary>
        /// 値が初期値と同一チェックを行う処理
        /// </summary>
        /// <returns></returns>
        protected bool CheckIsValueDiffers()
        {
            // m_valueとm_initValueがnullの場合、リセット不要
            if (ReferenceEquals(null, m_value))
            {
                if (ReferenceEquals(null, m_initValue))
                {
                    return false;
                }
            }
            // m_valueとm_initValueが同じの場合、リセット不要
            else if (m_value.Equals(m_initValue))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// データの値を初期値にする処理
        /// </summary>
        /// <returns>
        /// 結果：
        /// リセット成功：true
        /// リセット不要：false
        /// </returns>
        /// 未使用
        public virtual bool Reset()
        {
            if (false == CheckIsValueDiffers())
            {
                return false;
            }

            // 上以外はm_initValueを初期値に設定
            Value = m_initValue;

            return true;
        }

        /// <summary>
        /// 値
        /// </summary>
        public T Value
        {
            get => m_value;
            set
            {
                if (true == Equals(value, m_value))
                {
                    return;
                }

                Setter(ref m_value, in value);

                OnPropertyRaised(nameof(Value));

                OnChange();
            }
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="initValue">初期値</param>
        public Prop(in T initValue)
        {
            // 初期値の設定
            m_initValue = initValue;
            m_value = m_initValue;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Prop()
            : this(default)
        {

        }
    }

    /// <summary>
    /// 拡張クラス
    /// </summary>
    public static class PropExtension
    {
        public delegate void Subscriber<T>(in T value);

        /// <summary>
        /// セッターを指定する処理
        /// </summary>
        /// <typeparam name="T">データ型</typeparam>
        /// <param name="prop">プロパティ対象</param>
        /// <param name="subscriber">処理対象</param>
        /// <returns>プロパティ対象</returns>
        public static Prop<T> SetSubscriber<T>(this Prop<T> prop, Subscriber<T> subscriber)
        {
            // 新しい値を設定する
            prop.Setter = (ref T old, in T value) =>
            {
                old = value;

                subscriber(in value);
            };

            return prop;
        }
    }
}
