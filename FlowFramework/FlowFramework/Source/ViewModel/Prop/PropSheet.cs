﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FlowFramework.Model;
using FlowFramework.Utils;

namespace FlowFramework.ViewModel.Prop
{
    /// <summary>
    /// プロパティシートインタフェース
    /// </summary>
    public interface IPropSheet
    {
        /// <summary>
        /// 子プロパティ対象一覧
        /// </summary>
        LinkedList<IPropEditable> Props { get; }

        /// <summary>
        /// 子プロパティシート一覧
        /// </summary>
        LinkedList<IPropSheet> Sheets { get; }

        /// <summary>
        /// 親シート
        /// </summary>
        IPropSheet ParentSheet { get; }

        /// <summary>
        /// 絶対名
        /// </summary>
        string FullName { get; }

        /// <summary>
        /// 当シートの有無状態フラグ
        /// </summary>
        /// <remarks>
        /// 無効の場合、持ちデータの有無チェック（IsValid）がいつも通る。
        /// </remarks>
        bool IsEnabled { get; }

        /// <summary>
        /// 当プロパティ対象の親シートの有効・無効状態フラグ
        /// </summary>
        bool IsParentEnabled { set; }

        /// <summary>
        /// 当シートの有無状態フラグ
        /// </summary>
        /// <remarks>
        /// 無効の場合、持ちデータの有無チェック（IsValid）がいつも通る。
        /// </remarks>
        bool IsValid { get; }

        /// <summary>
        /// データ変化フラグ
        /// </summary>
        bool IsChanged { get; }

        /// <summary>
        /// IsValid値を計算して更新する処理
        /// </summary>
        /// <param name="thisIsValid">性能改善用フラグ：falseの場合、何も計算せずにIsValidをfalseに指定する</param>
        void UpdateIsValidState(bool thisIsValid);

        /// <summary>
        /// IsChanged値を計算して更新する処理
        /// </summary>
        /// <param name="thisIsChanged">性能改善用フラグ：falseの場合、何も計算せずにIsChangedをfalseに指定する</param>
        void UpdateIsChangedState(bool thisIsChanged);

        /// <summary>
        /// 全て子プロパティ対象のデータの値を初期値に指定する処理
        /// </summary>
        /// <returns>
        /// 結果：
        /// リセット成功：true
        /// リセット不要：false
        /// </returns>
        /// 未使用
        bool Reset();

        /// <summary>
        /// 親シートから当シートを削除する処理
        /// </summary>
        void RemoveMe();
    }

    /// <summary>
    /// プロパティシートクラス
    /// </summary>
    public abstract class PropSheet : NotifyStatic, IPropSheet
    {
        /// <summary>
        /// 子プロパティ対象一覧
        /// </summary>
        public LinkedList<IPropEditable> Props { get; private set; } = new LinkedList<IPropEditable>();

        /// <summary>
        /// 子プロパティシート一覧
        /// </summary>
        public LinkedList<IPropSheet> Sheets { get; private set; } = new LinkedList<IPropSheet>();

        /// <summary>
        /// 親シート
        /// </summary>
        public IPropSheet ParentSheet { get; private set; } = null;

        /// <summary>
        /// 親シートの子供リストに追加した項目ノード
        /// </summary>
        protected LinkedListNode<IPropSheet> ParentSheetNode { get; private set; } = null;

        /// <summary>
        /// シート名
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// 絶対名
        /// </summary>
        public string FullName { get; }

        #region Validation

        /// <summary>
        /// 当シートの有無状態フラグ
        /// </summary>
        /// <remarks>
        /// 無効の場合、持ちデータの有無チェック（IsValid）がいつも通る。
        /// </remarks>
        public bool IsValid { get; protected set; } = true;

        /// <summary>
        /// 全て子シートデータの有効無効チェック
        /// </summary>
        /// <returns>true：全て子シートは有効</returns>
        protected bool ValidateChildrenSheets()
        {
            // 全てのシートの有効無効チェック
            foreach (var sheet in Sheets)
            {
                if (false == sheet.IsValid)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// IsValid値を計算して値を更新する処理
        /// </summary>
        /// <param name="thisIsValid">性能改善用フラグ：falseの場合、何も計算せずにIsValidをfalseに指定する</param>
        void IPropSheet.UpdateIsValidState(bool thisIsValid)
        {
            bool val;
            
            // シートが無効の場合、値を有効に設定
            if (false == m_isEnabled)
            {
                val = true;
            }
            else
            {
                // 子シートの有効無効を取得
                val = ValidateChildrenSheets();

                // シートが無効の場合は、値を無効に設定
                if (false == thisIsValid)
                {
                    val = thisIsValid;
                }
                // 上記以外の場合は各シートの有効無効を取得し、値を設定する
                else
                {
                    if (true == val)
                    {
                        foreach (var prop in Props)
                        {
                            if (false == prop.IsValid)
                            {
                                val = false;
                                break;
                            }
                        }
                    }
                }
            }

            // 値が有効の場合、更新処理を行う
            if (val != IsValid)
            {
                IsValid = val;
                OnPropertyRaised(nameof(IsValid));

                if (null != ParentSheet)
                {
                    ParentSheet.UpdateIsValidState(IsValid);
                }
            }
        }

        #region IsParentEnabled

        private bool m_isParentEnabled = true;

        /// <summary>
        /// 当プロパティ対象の親シートの有効・無効状態フラグ
        /// </summary>
        bool IPropSheet.IsParentEnabled
        {
            set
            {
                if (value == m_isParentEnabled)
                {
                    return;
                }

                // データの有効無効状態が変更された場合に更新処理を行う
                m_isParentEnabled = value;
                OnPropertyRaised();
                OnPropertyRaised(nameof(IsEnabled));

                (this as IPropSheet).UpdateIsValidState(true);
            }
        }

        #endregion

        #region IsEnabled

        private bool m_isEnabled = true;

        /// <summary>
        /// 当シートの有無状態フラグ
        /// </summary>
        /// <remarks>
        /// 無効の場合、持ちデータの有無チェックを行う。
        /// </remarks>
        public bool IsEnabled
        {
            get => m_isEnabled && m_isParentEnabled;
            set
            {
                if (value == m_isEnabled)
                {
                    return;
                }

                // データの有効無効状態が変更された場合に更新処理を行う
                m_isEnabled = value;
                OnPropertyRaised();

                foreach (var prop in Props)
                {
                    prop.IsParentEnabled = value;
                }

                foreach (var sheet in Sheets)
                {
                    sheet.IsParentEnabled = value;
                }

                (this as IPropSheet).UpdateIsValidState(true);
            }
        }

        #endregion

        #endregion

        #region Changes

        /// <summary>
        /// 全て子プロパティ対象のデータの値を初期値に指定する処理
        /// </summary>
        /// <returns>
        /// 結果：
        /// リセット成功：true
        /// リセット不要：false
        /// </returns>
        /// 未使用
        public bool Reset()
        {
            // データが変更された場合、リセット処理を行う
            if (true == IsChanged)
            {
                bool res = false;

                foreach (var sheet in Sheets)
                {
                    res |= sheet.Reset();
                }

                foreach (var prop in Props)
                {
                    res |= prop.Reset(true);
                }

                IsChanged = false;
                OnPropertyRaised(nameof(IsChanged));

                return res;
            }

            return false;
        }

        /// <summary>
        /// データ変化フラグ
        /// </summary>
        public bool IsChanged { get; protected set; } = false;

        /// <summary>
        /// IsChanged値を計算して更新する処理
        /// </summary>
        /// <param name="thisIsChanged">性能改善用フラグ：falseの場合、何も計算せずにIsChangedをfalseに指定する</param>
        void IPropSheet.UpdateIsChangedState(bool thisIsChanged)
        {
            bool val = false;

            // 変更フラグがONの場合、そのまま代入する
            if (true == thisIsChanged)
            {
                val = thisIsChanged;
            }
            else
            {
                foreach (var prop in Props)
                {
                    // 子シートの変更フラグを取得し、更新する
                    if (true == prop.IsChanged)
                    {
                        val = true;
                        break;
                    }
                }

                // 各シートの変更フラグを取得し、更新する
                foreach (var sheet in Sheets)
                {
                    if (true == sheet.IsChanged)
                    {
                        val = true;
                        break;
                    }
                }
            }

            // データの変更フラグが変更された場合に更新処理を行う
            if (val != IsChanged)
            {
                IsChanged = val;
                OnPropertyRaised(nameof(IsChanged));

                if (null != ParentSheet)
                {
                    ParentSheet.UpdateIsChangedState(IsChanged);
                }
            }
        }

        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="name">シート名</param>
        /// <param name="parentSheet">親シート</param>
        public PropSheet(string name, PropSheet parentSheet = null)
        {
            Name = name;
            FullName = name;

            if (null != parentSheet)
            {
                ParentSheetNode = parentSheet.Sheets.AddLast(this);
                ParentSheet = parentSheet;
                (parentSheet as IPropSheet).UpdateIsValidState(IsValid);

                FullName = $"{ParentSheet.FullName}.{Name}";
            }
        }

        /// <summary>
        /// 親シートから当シートを削除する処理
        /// </summary>
        public void RemoveMe()
        {
            Debug.Assert(null != ParentSheetNode);
            Debug.Assert(null != ParentSheetNode.List);

            // 全て子プロパティ対象を削除うする
            while (Props.Count > 0)
            {
                Props.First.Value.RemoveMe();
            }

            // 全て子シートを削除うする
            while (Sheets.Count > 0)
            {
                Sheets.First.Value.RemoveMe();
            }

            // 親のリストから未登録
            ParentSheetNode.List.Remove(ParentSheetNode);
            ParentSheetNode = null;

            // 元の親を更新する
            (ParentSheet as IPropSheet).UpdateIsValidState(true);
            (ParentSheet as IPropSheet).UpdateIsChangedState(false);
            ParentSheet = null;
        }
    }
}
