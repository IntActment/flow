﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FlowFramework.Model;
using FlowFramework.Utils;

namespace FlowFramework.ViewModel.Prop
{
    /// <summary>
    /// PropEditableのデータ型のデフォールト有効チェック処理定義テーブルクラス
    /// </summary>
    public static class PropDefaults
    {
        /// <summary>
        /// デフォールト有効チェック処理定義テーブル
        /// </summary>
        public static readonly Dictionary<Type, Predicate<object>> Validators = new Dictionary<Type, Predicate<object>>()
        {
            { typeof(string), (val) => (null != val) && (false == string.IsNullOrEmpty(((string)val).Trim(' ', '　'))) },
            { typeof(IPv4), (val) => ((IPv4)val).IsValid },
            { typeof(IPv4?), (val) => (null != val) && ((IPv4)val).IsValid },
        };
    }
}
