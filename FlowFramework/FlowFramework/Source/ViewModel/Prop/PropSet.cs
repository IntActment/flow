﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowFramework.ViewModel.Prop
{
    /// <summary>
    /// RadioButton専用プロパティ対象クラス
    /// </summary>
    public class PropSet : PropEditable<bool>
    {
        /// <summary>
        /// プロパティ対象グループ
        /// </summary>
        public const string AllSiblings = "";

        /// <summary>
        /// グループ
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="name">プロパティ名</param>
        /// <param name="propSheet">親シート</param>
        /// <param name="initValue">初期値</param>
        /// <param name="group">グループ</param>
        /// <param name="subscriber">データの有効・無効チェックする処理対象</param>
        public PropSet(string name, IPropSheet propSheet, in bool initValue = default, string group = AllSiblings, Subscriber<bool> subscriber = null)
            : base(name, propSheet, initValue)
        {
            Group = group;

            if (null != subscriber)
            {
                this.SetSubscriber(subscriber);
                subscriber(initValue);
            }
        }

        /// <summary>
        /// データ変更イベント処理
        /// </summary>
        protected override void OnChange()
        {
            base.OnChange();

            UpdateGroup();
        }

        /// <summary>
        /// 同一グループのPropSetの値を指定する処理
        /// </summary>
        private void UpdateGroup()
        {
            if (true == Value)
            {
                // 同一グループの値を設定する
                foreach (var prop in ParentSheet.Props)
                {
                    if ((this != prop) && (prop is PropSet propSet) && (propSet.Group == Group))
                    {
                        propSet.Value = false;
                    }
                }
            }
        }
    }
}
