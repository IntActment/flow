﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FlowFramework.Model;
using FlowFramework.Utils;

namespace FlowFramework.ViewModel.Prop
{
    /// <summary>
    /// プローパティ対象インタフェース
    /// </summary>
    public interface IPropEditable : IValidatable
    {
        /// <summary>
        /// 絶対名
        /// </summary>
        string FullName { get; }

        /// <summary>
        /// データ変化フラグ
        /// </summary>
        bool IsChanged { get; }

        /// <summary>
        /// 親シート
        /// </summary>
        IPropSheet ParentSheet { get; }

        /// <summary>
        /// データ値を初期値に指定処理
        /// </summary>
        /// <param name="silent">”親シートに知らせない”フラグ</param>
        /// <returns>
        /// 結果：
        /// リセット成功：true
        /// リセット不要：false
        /// </returns>
        /// 未使用
        bool Reset(bool silent);

        /// <summary>
        /// 当プロパティ対象の有効・無効状態フラグ
        /// </summary>
        bool IsEnabled { get; }

        /// <summary>
        /// 当プロパティ対象の親シートの有効・無効状態フラグ
        /// </summary>
        bool IsParentEnabled { set; }

        /// <summary>
        /// データ有効・無効チェックと一緒に他プロパティ対象の
        /// データ有効・無効チェックを行う対象一覧
        /// </summary>
        LinkedList<IPropEditable> ValidationMembers { get; }

        /// <summary>
        /// データ有効・無効チェックの内部処理
        /// </summary>
        void Validate_internal();

        HashSet<Tuple<LinkedListNode<IPropEditable>, IPropEditable>> MemberOfLists { get; }

        /// <summary>
        /// 親シートから当プロパティ対象を削除する処理
        /// </summary>
        void RemoveMe();
    }

    /// <summary>
    /// プローパティ対象クラス
    /// </summary>
    /// <typeparam name="T">データ型</typeparam>
    public class PropEditable<T> : Prop<T>, IPropEditable
    {
        /// <summary>
        /// 親シート
        /// </summary>
        public IPropSheet ParentSheet { get; private set; } = null;

        /// <summary>
        /// 親シートの子供リストに追加した項目ノード
        /// </summary>
        protected LinkedListNode<IPropEditable> ParentSheetNode { get; private set; } = null;

        /// <summary>
        /// データの有効・無効チェック処理対象
        /// </summary>
        protected Predicate<T> Validator { get; set; } = (value) => true;

        /// <summary>
        /// データ有効・無効チェックと一緒に他プロパティ対象の
        /// データ有効・無効チェックを行う対象一覧
        /// </summary>
        public LinkedList<IPropEditable> ValidationMembers { get; }

        public HashSet<Tuple<LinkedListNode<IPropEditable>, IPropEditable>> MemberOfLists { get; }

        #region IsParentEnabled

        private bool m_isParentEnabled = true;

        /// <summary>
        /// 当プロパティ対象の親シートの有効・無効状態フラグ
        /// </summary>
        bool IPropEditable.IsParentEnabled
        {
            set
            {
                if (value == m_isParentEnabled)
                {
                    return;
                }

                m_isParentEnabled = value;
                OnPropertyRaised();
                OnPropertyRaised(nameof(IsEnabled));

                Validate();
            }
        }

        #endregion

        #region IsEnabled

        private bool m_isEnabled = true;

        /// <summary>
        /// 当プロップの有無状態フラグ
        /// </summary>
        /// <remarks>
        /// 無効の場合、持ちデータの有無チェック（IsValid）がいつも通る。
        /// </remarks>
        public bool IsEnabled
        {
            get => m_isEnabled && m_isParentEnabled;
            set
            {
                if (value == m_isEnabled)
                {
                    return;
                }

                m_isEnabled = value;
                OnPropertyRaised();

                Validate();
            }
        }

        #endregion

        /// <summary>
        /// データ変化フラグ
        /// </summary>
        public bool IsChanged { get; protected set; } = false;

        /// <summary>
        /// データ有効・無効状態フラグ
        /// </summary>
        public bool IsValid { get; protected set; } = true;

        /// <summary>
        /// データの有効・無効チェック処理対象を指定する処理
        /// </summary>
        /// <param name="validator">データの有効・無効チェック処理対象</param>
        /// <returns>当プロパティ対象</returns>
        public PropEditable<T> SetValidator(Predicate<T> validator)
        {
            Validator = (val) => (!IsEnabled) || validator(val);
            Validate();

            return this;
        }

        /// <summary>
        /// データ有効・無効チェック処理
        /// </summary>
        public void Validate()
        {
            (this as IPropEditable).Validate_internal();

            foreach (IPropEditable member in ValidationMembers)
            {
                member.Validate_internal();
            }
        }

        /// <summary>
        /// データ有効・無効チェックの内部処理
        /// </summary>
        void IPropEditable.Validate_internal()
        {
            bool res = Validator(Value);

            if (res != IsValid)
            {
                IsValid = res;
                OnPropertyRaised(nameof(IsValid));

                ParentSheet.UpdateIsValidState(res);
            }
        }

        /// <summary>
        /// データ変更イベント処理
        /// </summary>
        protected override void OnChange()
        {
            Validate();

            IsChanged = CheckIsValueDiffers();
            OnPropertyRaised(nameof(IsChanged));

            ParentSheet.UpdateIsChangedState(IsChanged);
        }

        /// <summary>
        /// データの値を初期値に指定する処理
        /// </summary>
        /// <param name="silent">”親シートに知らせない”フラグ</param>
        /// <returns>
        /// 結果：
        /// リセット成功：true
        /// リセット不要：false
        /// </returns>
        /// 未使用
        public bool Reset(bool silent)
        {
            if (true == IsChanged)
            {
#if DEBUG
                if (false == base.Reset())
                {
                    throw new InvalidOperationException();
                }
#else
                base.Reset();
#endif
                IsChanged = false;
                OnPropertyRaised(nameof(IsChanged));

                if (false == silent)
                {
                    ParentSheet.UpdateIsChangedState(IsChanged);
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// データの値を初期値に指定する処理
        /// </summary>
        /// <returns>
        /// 結果：
        /// リセット成功：true
        /// リセット不要：false
        /// </returns>
        /// 未使用
        public override bool Reset()
        {
            return Reset(false);
        }

        /// <summary>
        /// プロパティ名
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// 絶対名
        /// </summary>
        public string FullName { get; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="name">プロパティ名</param>
        /// <param name="propSheet">親シート</param>
        /// <param name="initValue">初期値</param>
        /// <param name="subscriber">データの有効・無効チェックする処理対象</param>
        public PropEditable(string name, IPropSheet propSheet, in T initValue = default, Subscriber<T> subscriber = null)
            : base(initValue)
        {
            // シートのフルネームを取得
            Name = name;
            FullName = $"{propSheet.FullName}.{Name}";

            // 各データの有効無効のリスト
            ValidationMembers = new LinkedList<IPropEditable>();
            MemberOfLists = new HashSet<Tuple<LinkedListNode<IPropEditable>, IPropEditable>>();

            // 親シートに各データを格納
            ParentSheet = propSheet;
            ParentSheetNode = ParentSheet.Props.AddLast(this);

            // 各データ有効/無効チェック
            Predicate<object> validator;
            if (typeof(IValidatable).IsAssignableFrom(TYPE))
            {
                Validator = (val) => (!IsEnabled) || ((IValidatable)val).IsValid;
            }
            else if (true == PropDefaults.Validators.TryGetValue(TYPE, out validator))
            {
                Validator = (val) => (!IsEnabled) || validator(val);
            }

            Validate();

            if (null != subscriber)
            {
                this.SetSubscriber(subscriber);
                subscriber(initValue);
            }
        }

        /// <summary>
        /// 親シートから当プロパティ対象を削除する処理
        /// </summary>
        public void RemoveMe()
        {
            Debug.Assert(null != ParentSheetNode.List);

            // 親のリストから未登録
            ParentSheetNode.List.Remove(ParentSheetNode);
            ParentSheetNode = null;

            // 元の親を更新する
            ParentSheet.UpdateIsValidState(true);
            ParentSheet.UpdateIsChangedState(false);
            ParentSheet = null;

            // バリデーション関係を削除
            foreach (var tuple in MemberOfLists)
            {
                if (tuple.Item1.List != null)
                {
                    tuple.Item1.List.Remove(tuple.Item1);
                }
            }

            MemberOfLists.Clear();
        }
    }

    public delegate void Subscriber<T>(in T value);

    /// <summary>
    /// 拡張クラス
    /// </summary>
    public static class PropEditableExtension
    {
        /// <summary>
        /// データの有効・無効チェックする処理対象を指定する処理
        /// </summary>
        /// <typeparam name="T">データ型</typeparam>
        /// <typeparam name="P">プロパティ対象型</typeparam>
        /// <param name="prop">プロパティ対象</param>
        /// <param name="subscriber">データの有効・無効チェックする処理対象</param>
        /// <returns>プロパティ対象</returns>
        public static P SetSubscriber<T, P>(this P prop, Subscriber<T> subscriber) where P : PropEditable<T>
        {
            prop.Setter = (ref T old, in T value) =>
            {
                old = value;

                subscriber(in value);
            };

            return prop;
        }

        /// <summary>
        /// データ有効・無効チェックと一緒に他プロパティ対象の
        /// データ有効・無効チェックを行う対象一覧に追加する処理
        /// </summary>
        /// <typeparam name="P"></typeparam>
        /// <param name="prop">プロパティ対象</param>
        /// <param name="member">他プロパティ対象</param>
        /// <returns>プロパティ対象</returns>
        public static P AddValidationMember<P>(this P prop, IPropEditable member) where P : IPropEditable
        {
            Debug.Assert((IPropEditable)prop != member);

            member.MemberOfLists.Add(new Tuple<LinkedListNode<IPropEditable>, IPropEditable>(prop.ValidationMembers.AddLast(member), prop));

            return prop;
        }
    }
}
