﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using FlowFramework.Utils;
using FlowFramework.View;

namespace FlowFramework.ViewModel
{
    public delegate void ScreenAction(IScreen screen);

    /// <summary>
    /// 画面インタフェース
    /// </summary>
    public interface IScreen
    {
        /// <summary>
        /// 当画面のウィンドウインスタンス
        /// </summary>
        WindowBase Window { get; }

        /// <summary>
        /// 画面を表示する処理
        /// </summary>
        void Show();

        /// <summary>
        /// 画面を非表示する処理
        /// </summary>
        void Hide();

        /// <summary>
        /// モーダル表示フラグ
        /// </summary>
        bool IsModal { get; set; }

        /// <summary>
        /// モーダル画面終了前の実行する処理
        /// </summary>
        ScreenAction OnBeforeModalClose { get; set; }

        /// <summary>
        /// 当画面がオーナー画面とする処理
        /// </summary>
        /// <remarks>
        /// 「オーナー画面」の意味は、該当ウィンドウが閉じられた時に、アプリも終了される。
        /// </remarks>
        void BecomeOwner();
    }

    /// <summary>
    /// 拡張クラス
    /// </summary>
    public static partial class Extensions
    {
        /// <summary>
        /// モーダル画面作成と表示
        /// </summary>
        /// <typeparam name="ScreenOtherT">モーダル画面型</typeparam>
        /// <typeparam name="ScreenT">オーナー画面型</typeparam>
        /// <param name="thisScreen">オーナー画面インスタンス</param>
        /// <param name="beforeShow">モーダル画面表示前の処理</param>
        /// <param name="beforeClose">モーダル画面閉じる前の処理</param>
        public static void CreateScreenModal<ScreenOtherT, ScreenT>(this ScreenT thisScreen, Action<ScreenOtherT> beforeShow = null, Action<ScreenOtherT> beforeClose = null)
            where ScreenT : class, IScreen
            where ScreenOtherT : class, IScreen
        {
            var otherScreen = ScreenHelper<ScreenOtherT>.Create(false);
            otherScreen.Window.Owner = thisScreen.Window;
            otherScreen.IsModal = true;

            if (null != beforeClose)
            {
                otherScreen.OnBeforeModalClose = delegate (IScreen s) { beforeClose((ScreenOtherT)s); };
            }

            beforeShow?.Invoke(otherScreen);

            otherScreen.Show();
            otherScreen.Window.ShowDialog();
        }
    }

    /// <summary>
    /// 画面を作成するヘルパークラス
    /// </summary>
    /// <typeparam name="ScreenT">画面型</typeparam>
    public static class ScreenHelper<ScreenT> where ScreenT : class, IScreen
    {
        private static Type m_windowType;

        /// <summary>
        /// ウィンドウ型を取得する
        /// </summary>
        public static Type WindowType
        {
            get
            {
                if (null == m_windowType)
                {
                    Type screenType = typeof(ScreenT);
                    Type baseType;

                    while ((baseType = screenType.BaseType) != typeof(object))
                    {
                        if ((true == baseType.IsGenericType) && (baseType.GetGenericTypeDefinition() == typeof(Screen<>)))
                        {
                            m_windowType = baseType.GetGenericArguments()[0];

                            return m_windowType;
                        }

                        screenType = screenType.BaseType;
                    }

                    throw new InvalidOperationException();
                }

                return m_windowType;
            }
        }

        /// <summary>
        /// 画面を作成する処理
        /// </summary>
        /// <returns>画面インスタンス</returns>
        public static ScreenT Create(bool makeOwner)
        {
            WindowBase window = (WindowBase)Activator.CreateInstance(WindowType);

            var res = (ScreenT)window.ViewModels[typeof(ScreenT)];

            if (true == makeOwner)
            {
                res.BecomeOwner();
            }

            return res;
        }

        /// <summary>
        /// 画面を作成して表示する処理
        /// </summary>
        /// <returns>画面インスタンス</returns>
        public static ScreenT CreateAndShow(bool makeOwner)
        {
            var res = Create(makeOwner);
            res.Show();

            return res;
        }
    }

    /// <summary>
    /// 画面を閉じる許可
    /// </summary>
    public enum CloseResult
    {
        /// <summary>
        /// 閉じてもいい
        /// </summary>
        DoClose,

        /// <summary>
        /// 閉じない
        /// </summary>
        CancelClosing,
    }

    /// <summary>
    /// 画面のベースクラス
    /// </summary>
    public class Screen<WinT> : ViewModel<WinT>, IScreen where WinT : WindowBase, new()
    {
        #region コールバックハンドラー

        /// <summary>
        /// ウィンドウロード終了イベント処理
        /// </summary>
        protected override void OnLoaded()
        {
            Worker.AssertMainThread();

            Window.Closing += WindowClosing;
            Window.IsVisibleChanged += IsVisibleChanged;
        }

        /// <summary>
        /// ウィンドウ表示状態変更イベント処理
        /// </summary>
        /// <param name="sender">センダー</param>
        /// <param name="e">イベントパラメータ</param>
        private void IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            OnPropertyRaised(nameof(IsVisible));
        }

        /// <summary>
        /// ウィンドウを閉じるイベント処理
        /// </summary>
        /// <param name="sender">センダー</param>
        /// <param name="e">イベントパラメータ</param>
        private void WindowClosing(object sender, CancelEventArgs e)
        {
            // ウィンドウを閉じる防ぐ
            e.Cancel = CheckAllowClose() == CloseResult.CancelClosing;
        }

        /// <summary>
        /// ウィンドウアンロード終了イベント処理
        /// </summary>
        protected override void OnUnloaded()
        {
            Worker.AssertMainThread();

            (this as IScreen).OnBeforeModalClose?.Invoke(this);
            (this as IScreen).OnBeforeModalClose = null;

            Window.Closing -= WindowClosing;
            Window.IsVisibleChanged -= IsVisibleChanged;

            Type type = GetType();
            LinkedList<IScreen> screenList;

            if (false == m_screens.TryGetValue(type, out screenList))
            {
                Debug.Assert(false);
            }

            screenList.Remove(m_screenListNode);
            m_screenListNode = null;

            if (0 == screenList.Count)
            {
                m_screens.Remove(type);
            }
        }

        #endregion

        /// <summary>
        /// 当画面のウィンドウインスタンス
        /// </summary>
        WindowBase IScreen.Window => Window;

        /// <summary>
        /// モーダル表示フラグ
        /// </summary>
        bool IScreen.IsModal { get; set; } = false;

        /// <summary>
        /// 画面インスタンスマップ
        /// </summary>
        private static Dictionary<Type, LinkedList<IScreen>> m_screens { get; }

        private LinkedListNode<IScreen> m_screenListNode;

        /// <summary>
        /// 画面型によって画面一覧を取得する処理
        /// </summary>
        /// <typeparam name="ScreenT">画面型</typeparam>
        /// <returns>
        /// 存在する場合：画面インスタンス配列
        /// 存在しない場合：null
        /// </returns>
        public static ScreenT[] GetScreens<ScreenT>() where ScreenT : class, IScreen
        {
            if (true == m_screens.TryGetValue(typeof(ScreenT), out LinkedList<IScreen> screenList))
            {
                Debug.Assert(0 != screenList.Count);

                return ((IEnumerable<ScreenT>)screenList).ToArray();
            }

            return null;
        }

        /// <summary>
        /// 画面型によって最初画面を取得する処理
        /// </summary>
        /// <typeparam name="ScreenT">画面型</typeparam>
        /// <returns>
        /// 存在する場合：画面インスタンス
        /// 存在しない場合：null
        /// </returns>
        public static ScreenT GetScreenFirst<ScreenT>() where ScreenT : class, IScreen
        {
            if (true == m_screens.TryGetValue(typeof(ScreenT), out LinkedList<IScreen> screenList))
            {
                Debug.Assert(0 != screenList.Count);

                return (ScreenT)screenList.First.Value;
            }

            return null;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        protected Screen()
        {
            Worker.AssertMainThread();

            Type type = GetType();
            LinkedList<IScreen> screenList;

            if (false == m_screens.TryGetValue(type, out screenList))
            {
                screenList = new LinkedList<IScreen>();

                m_screens.Add(type, screenList);
            }

            m_screenListNode = screenList.AddLast(this);
        }

        /// <summary>
        /// スタチックコンストラクタ
        /// </summary>
        static Screen()
        {
            Worker.AssertMainThread();

            m_screens = new Dictionary<Type, LinkedList<IScreen>>();
        }

        /// <summary>
        /// モーダル画面終了前の実行する処理
        /// </summary>
        ScreenAction IScreen.OnBeforeModalClose { get; set; }

        /// <summary>
        /// ウィンドウを閉じる防げるイベント処理
        /// </summary>
        /// <returns>
        /// 画面閉じる許可
        /// </returns>
        protected virtual CloseResult CheckAllowClose()
        {
            return CloseResult.DoClose;
        }

        /// <summary>
        /// 画面を閉じる処理
        /// </summary>
        public void Close()
        {
            Window.Close();
        }

        #region オーナー機能

        /// <summary>
        /// 当画面がオーナー画面となっているかどうかフラグ
        /// </summary>
        /// <remarks>
        /// 「オーナー画面」の意味は、該当ウィンドウが閉じられた時に、アプリも終了される。
        /// </remarks>
        public bool IsOwnerScreen => Window == Application.Current.MainWindow;

        /// <summary>
        /// 当画面がオーナー画面とする処理
        /// </summary>
        /// <remarks>
        /// 「オーナー画面」の意味は、該当ウィンドウが閉じられた時に、アプリも終了される。
        /// </remarks>
        public void BecomeOwner()
        {
            Application.Current.MainWindow = Window;
        }

        #endregion

        /// <summary>
        /// 画面表示状態フラグ
        /// </summary>
        public bool IsVisible => Window.IsVisible;

        /// <summary>
        /// 画面を表示する処理
        /// </summary>
        public virtual void Show()
        {
            if (true == (this as IScreen).IsModal)
            {
                return;
            }

            Debug.Assert(false == Window.IsVisible);

            Window.Show();
        }

        /// <summary>
        /// 画面を非表示する処理
        /// </summary>
        public virtual void Hide()
        {
            if (true == (this as IScreen).IsModal)
            {
                return;
            }

            Debug.Assert(true == Window.IsVisible);

            Window.Hide();
        }
    }
}
