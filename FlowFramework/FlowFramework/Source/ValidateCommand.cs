﻿using System;

namespace FlowFramework
{
    public interface IValidateCommand<T>
    {
        string GetError(in T val);
    }

    public class ValidateCommand<T> : IValidateCommand<T>
    {
        public delegate string ValidateDelegate(in T val);

        protected ValidateDelegate m_filter;

        public ValidateCommand(ValidateDelegate validator)
        {
            m_filter = validator ?? throw new NullReferenceException(nameof(validator));
        }

        public string GetError(in T val)
        {
            try
            {
                return m_filter(in val);
            }
            catch
            {
                return null;
            }
        }
    }
}
