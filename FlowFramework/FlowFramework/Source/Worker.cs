﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace FlowFramework
{
    public class Worker : Flow
    {
        public static Flow Instance { get; }

        [Conditional("DEBUG")]
        public static new void AssertThread()
        {
            Instance.AssertThread();
        }

        static Worker()
        {
            Instance = new Worker();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        private Worker()
            : base("Flow Worker")
        { }

        public static new Dispatcher Dispatcher => Instance.Dispatcher;

        /// <summary>
        /// ワーカースレッドで処理を実行する処理
        /// </summary>
        /// <param name="action">処理</param>
        /// <param name="mode">実行モード</param>
        public static new void Invoke(Action action, RunMode mode = RunMode.AlwaysInvoke)
        {
            Instance.Invoke(action, mode);
        }

        /// <summary>
        /// ワーカースレッドで処理を実行する処理
        /// </summary>
        /// <param name="action">処理</param>
        /// <param name="mode">実行モード</param>
        public static new void Run(Action action, RunMode mode = RunMode.AlwaysInvoke)
        {
            Instance.Run(action, mode);
        }

        /// <summary>
        /// ワーカースレッドで処理を実行する処理
        /// </summary>
        /// <typeparam name="TResult">戻り値の型</typeparam>
        /// <param name="func">処理対象</param>
        /// <returns>処理対象の戻り値</returns>
        /// <param name="mode">実行モード</param>
        public static new TResult Run<TResult>(Func<TResult> func, RunMode mode = RunMode.AlwaysInvoke)
        {
            return Instance.Run(func, mode);
        }

        /// <summary>
        /// ワーカースレッドで処理を実行する処理
        /// </summary>
        /// <typeparam name="TResult">戻り値の型</typeparam>
        /// <param name="func">処理対象</param>
        /// <returns>処理対象の戻り値</returns>
        public static new TResult Run<TResult>(Func<Task<TResult>> func)
        {
            return Instance.Run(func);
        }

        /// <summary>
        /// ワーカースレッドで処理を実行する処理
        /// </summary>
        /// <typeparam name="TResult">戻り値の型</typeparam>
        /// <param name="func">処理対象</param>
        /// <returns>処理対象の戻り値</returns>
        public static new void Run(Func<Task> func)
        {
            Instance.Run(func);
        }
    }
}
