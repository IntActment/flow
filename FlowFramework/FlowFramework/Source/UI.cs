﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Threading;

namespace FlowFramework
{
    public class UI : Flow
    {
        [Conditional("DEBUG")]
        public static new void AssertThread()
        {
            Main.AssertThread();
        }

        /// <summary>
        /// 未使用コンストラクタ
        /// </summary>
        private UI()
            : base(null)
        { }

        public static new Dispatcher Dispatcher => Main.Dispatcher;

        /// <summary>
        /// UIスレッドで処理を実行する処理
        /// </summary>
        /// <param name="action">処理</param>
        /// <param name="mode">実行モード</param>
        public static new void Invoke(Action action, RunMode mode = RunMode.AlwaysInvoke)
        {
            Main.Invoke(action, mode);
        }

        /// <summary>
        /// UIスレッドで処理を実行する処理
        /// </summary>
        /// <param name="action">処理</param>
        /// <param name="mode">実行モード</param>
        public static new void Run(Action action, RunMode mode = RunMode.AlwaysInvoke)
        {
            Main.Run(action, mode);
        }

        /// <summary>
        /// UIスレッドで処理を実行する処理
        /// </summary>
        /// <typeparam name="TResult">戻り値の型</typeparam>
        /// <param name="func">処理対象</param>
        /// <returns>処理対象の戻り値</returns>
        /// <param name="mode">実行モード</param>
        public static new TResult Run<TResult>(Func<TResult> func, RunMode mode = RunMode.AlwaysInvoke)
        {
            return Main.Run(func, mode);
        }

        /// <summary>
        /// UIスレッドで処理を実行する処理
        /// </summary>
        /// <typeparam name="TResult">戻り値の型</typeparam>
        /// <param name="func">処理対象</param>
        /// <returns>処理対象の戻り値</returns>
        public static new TResult Run<TResult>(Func<Task<TResult>> func)
        {
            return Main.Run(func);
        }

        /// <summary>
        /// UIスレッドで処理を実行する処理
        /// </summary>
        /// <typeparam name="TResult">戻り値の型</typeparam>
        /// <param name="func">処理対象</param>
        /// <returns>処理対象の戻り値</returns>
        public static new void Run(Func<Task> func)
        {
            Main.Run(func);
        }
    }
}
