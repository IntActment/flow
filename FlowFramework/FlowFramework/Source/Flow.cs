﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace FlowFramework
{
    public class Flow
    {
        public enum RunMode
        {
            /// <summary>
            /// 実行先スレッドは当スレッドであっても、Dispatcher.BeginInvokeで次フレームでアクショを実行する
            /// </summary>
            AlwaysInvoke,

            /// <summary>
            /// 実行先スレッドは当スレッドの場合、すぎ実行する（Dispatcher.BeginInvokeを使わない）。
            /// </summary>
            Optimal,
        }

        public delegate void FlowExceptionDelegate(Flow caller, Flow target, RunMode mode, Exception e);

        #region Event OnExceptionRaise
        private static readonly object m_eventLock = new object();
        private static FlowExceptionDelegate m_onExceptionRaise;

        public static event FlowExceptionDelegate OnExceptionRaise
        {
            add
            {
                lock (m_eventLock)
                {
                    m_onExceptionRaise += value;
                }
            }
            remove
            {
                lock (m_eventLock)
                {
                    m_onExceptionRaise -= value;
                }
            }
        }

        private static readonly object m_notifyLock = new object();

        private static bool m_isNotifying = false;

        protected static void NotifyException(Flow caller, Flow target, RunMode mode, Exception e)
        {
            bool isNotifying;

            lock (m_notifyLock)
            {
                isNotifying = m_isNotifying;
            }

            if (false == isNotifying)
            {
                lock (m_notifyLock)
                {
                    m_isNotifying = true;
                }

                try
                {
                    m_onExceptionRaise?.Invoke(caller, target, mode, e);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"OnExceptionRaiseイベント実行中で例外発生した：: {ex.Message}");
                }
                finally
                {
                    lock (m_notifyLock)
                    {
                        m_isNotifying = false;
                    }
                }
            }
            else
            {
                Debug.WriteLine($"Flow：OnExceptionRaiseイベント実行中で例外発生してOnExceptionRaiseイベント実行された。");
            }
        }

        #endregion

        private int m_threadID = -1;
        private static List<Flow> m_flowList;

        public static Flow Main { get; }

        [ThreadStatic]
        public static Flow Current = null;

        public Thread Thread { get; }

        public static bool IsMain => Current == Main;

        public Dispatcher Dispatcher { get; private set; } = null;

        /// <summary>
        /// デザインモードチェック
        /// </summary>
        public static bool IsInDesignMode => (bool)DesignerProperties.IsInDesignModeProperty.GetMetadata(typeof(DependencyObject)).DefaultValue;

        [Conditional("DEBUG")]
        public void AssertThread()
        {
            // デザインモードでチェックしない
            if (true == IsInDesignMode)
            {
                return;
            }

            Debug.Assert(Thread.CurrentThread.ManagedThreadId == m_threadID);
        }

        [Conditional("DEBUG")]
        public static void AssertMainThread()
        {
            // デザインモードでチェックしない
            if (true == IsInDesignMode)
            {
                return;
            }

            Debug.Assert(Thread.CurrentThread.ManagedThreadId == Main.m_threadID);
        }

        static Flow()
        {
            m_flowList = new List<Flow>();
            Main = new Flow();

            Application.Current.Exit += CurrentDomain_ProcessExit;
        }

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            for (int i = m_flowList.Count - 1; i >= 0; i--)
            {
                m_flowList[i].Dispatcher.InvokeShutdown();
            }
        }

        private Flow()
        {
            Debug.Assert(null == Main);
            Debug.Assert(m_flowList.Count == 0);

            Current = this;

            Dispatcher = Dispatcher.CurrentDispatcher;
            m_threadID = Thread.CurrentThread.ManagedThreadId;
            m_flowList.Add(this);
        }

        public Flow(string threadName)
        {
            Thread Create()
            {
                ManualResetEvent dispatcherReadyEvent = new ManualResetEvent(false);

                Thread thread = new Thread(new ThreadStart(() =>
                {
                    Dispatcher = Dispatcher.CurrentDispatcher;
                    m_threadID = Thread.CurrentThread.ManagedThreadId;

                    Thread.CurrentThread.Name = threadName;

                    Current = this;

                    dispatcherReadyEvent.Set();
                    Dispatcher.Run();
                }));

                thread.Start();

                dispatcherReadyEvent.WaitOne();

                m_flowList.Add(this);

                return thread;
            }

            if (true == IsMain)
            {
                Thread = Create();
            }
            else
            {
                Thread = Main.Run(Create);
            }
        }

        /// <summary>
        /// 処理を実行する処理
        /// </summary>
        /// <param name="action">処理対象</param>
        /// <param name="mode">実行モード</param>
        public void Invoke(Action action, RunMode mode = RunMode.AlwaysInvoke)
        {
            Debug.Assert(null != action);

            if ((mode == RunMode.Optimal) && (Thread.CurrentThread.ManagedThreadId == m_threadID))
            {
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    NotifyException(Current, Current, mode, e);
                }
            }
            else
            {
                var caller = Current;

                Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action(() =>
                {
                    try
                    {
                        action();
                    }
                    catch (Exception e)
                    {
                        NotifyException(caller, Current, mode, e);
                    }
                }));
            }
        }

        /// <summary>
        /// 処理を実行する処理
        /// </summary>
        /// <param name="action">処理対象</param>
        /// <param name="mode">実行モード</param>
        public void Run(Action action, RunMode mode = RunMode.AlwaysInvoke)
        {
            Debug.Assert(null != action);

            if ((mode == RunMode.Optimal) && (Thread.CurrentThread.ManagedThreadId == m_threadID))
            {
                try
                {
                    action();
                }
                catch (Exception e)
                {
                    NotifyException(Current, Current, mode, e);
                }
            }
            else
            {
                DispatcherFrame frame = new DispatcherFrame();
                var caller = Current;

                Action Do = () =>
                {
                    try
                    {
                        action();
                    }
                    catch (Exception e)
                    {
                        NotifyException(caller, Current, mode, e);
                    }
                    finally
                    {
                        frame.Continue = false;
                    }
                };

                Dispatcher.BeginInvoke(DispatcherPriority.Background, Do);
                Dispatcher.PushFrame(frame);
            }
        }

        /// <summary>
        /// 処理を実行する処理
        /// </summary>
        /// <typeparam name="TResult">戻り値の型</typeparam>
        /// <param name="func">処理対象</param>
        /// <param name="mode">実行モード</param>
        /// <returns>処理対象の戻り値</returns>
        public TResult Run<TResult>(Func<TResult> func, RunMode mode = RunMode.AlwaysInvoke)
        {
            Debug.Assert(null != func);

            TResult result = default;

            if ((mode == RunMode.Optimal) && (Thread.CurrentThread.ManagedThreadId == m_threadID))
            {
                try
                {
                    result = func();
                }
                catch (Exception e)
                {
                    NotifyException(Current, Current, mode, e);
                }
            }
            else
            {
                DispatcherFrame frame = new DispatcherFrame();
                var caller = Current;

                Action Do = () =>
                {
                    try
                    {
                        result = func();
                    }
                    catch (Exception e)
                    {
                        NotifyException(caller, Current, mode, e);
                    }
                    finally
                    {
                        frame.Continue = false;
                    }
                };

                Dispatcher.BeginInvoke(DispatcherPriority.Background, Do);
                Dispatcher.PushFrame(frame);
            }

            return result;
        }

        /// <summary>
        /// 処理を実行する処理
        /// </summary>
        /// <typeparam name="TResult">戻り値の型</typeparam>
        /// <param name="func">処理対象</param>
        /// <returns>処理対象の戻り値</returns>
        public TResult Run<TResult>(Func<Task<TResult>> func)
        {
            Debug.Assert(null != func);

            TResult result = default;

            DispatcherFrame frame = new DispatcherFrame();
            var caller = Current;

            // ワーカースレッドで渡されたデリゲートを実行する
            Dispatcher.Invoke(new Func<Task>(async () =>
            {
                try
                {
                    result = await func();
                }
                catch (Exception e)
                {
                    NotifyException(caller, Current, RunMode.AlwaysInvoke, e);
                }
                finally
                {
                    frame.Continue = false;
                }
            }));

            Dispatcher.PushFrame(frame);

            return result;
        }

        /// <summary>
        /// 処理を実行する処理
        /// </summary>
        /// <typeparam name="TResult">戻り値の型</typeparam>
        /// <param name="func">処理対象</param>
        /// <returns>処理対象の戻り値</returns>
        public void Run(Func<Task> func)
        {
            Debug.Assert(null != func);

            DispatcherFrame frame = new DispatcherFrame();
            var caller = Current;

            async Task Proc()
            {
                try
                {
                    await func();
                }
                catch (Exception e)
                {
                    NotifyException(caller, Current, RunMode.AlwaysInvoke, e);
                }
                finally
                {
                    frame.Continue = false;
                }
            }

            // ワーカースレッドで渡されたデリゲートを実行する
            Dispatcher.Invoke(Proc);

            Dispatcher.PushFrame(frame);
        }
    }
}
