﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlowFramework.Controls
{
    /// <summary>
    /// コンテンツを拡大・縮小・移動するコントロール
    /// </summary>
    public class ZoomControl : ContentControl
    {
        /// <summary>
        /// ズーム最小値定義
        /// </summary>
        public const double ZOOM_MIN = 1.0;

        /// <summary>
        /// ズーム最大値定義
        /// </summary>
        public const double ZOOM_MAX = 10.0;

        /// <summary>
        /// ズーム変更ステップ
        /// </summary>
        public const double ZOOM_STEP = 0.1;

        /// <summary>
        /// ズーム値
        /// </summary>
        public double Zoom
        {
            get { return (double)GetValue(ZoomProperty); }
            set { SetValue(ZoomProperty, value); }
        }

        private static readonly DependencyProperty ZoomProperty = DependencyProperty.Register(
            nameof(Zoom),
            typeof(double),
            typeof(ZoomControl),
            new FrameworkPropertyMetadata(
                ZOOM_MIN,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                ZoomChanged,
                CoerceZoom,
                false,
                UpdateSourceTrigger.PropertyChanged
        ));

        /// <summary>
        /// ズーム変更時処理
        /// </summary>
        /// <param name="d">ズームコントロール</param>
        /// <param name="e">イベントパラメータ</param>
        private static void ZoomChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            // 前回値と今回値の取得
            double newZoom = (double)e.NewValue;
            double oldZoom = (double)e.OldValue;
            ZoomControl control = (ZoomControl)d;

            bool isDragging = control.m_isDragging;
            Point mousePos = new Point();

            // ドラッグ中またはマウスポインタがコントロール内にある場合はマウスポインタを取得
            if ((true == isDragging) || control.IsMouseOver)
            {
                mousePos = Mouse.GetPosition(control);
            }

            // ズームコントロールのX/Y座標を更新
            control.m_translate.X = mousePos.X - ((mousePos.X - control.m_translate.X) / oldZoom) * newZoom;
            control.m_translate.Y = mousePos.Y - ((mousePos.Y - control.m_translate.Y) / oldZoom) * newZoom;

            // ズームコントロール内を更新
            control.BoundTranslate(newZoom);
            control.UpdateTransform(newZoom, control.m_translate);
        }

        /// <summary>
        /// ズームコントロールの最小/最大倍率チェック
        /// </summary>
        /// <param name="d">ズームコントロール</param>
        /// <param name="value">拡大/縮小倍率</param>
        /// <returns>最小値</returns>
        private static object CoerceZoom(DependencyObject d, object value)
        {
            double zoom = (double)value;
            ZoomControl control = (ZoomControl)d;

            // 拡大/縮小倍率が最大倍率より大きい場合は最大倍率を返す
            if (zoom > ZOOM_MAX)
            {
                return ZOOM_MAX;
            }

            // 拡大/縮小倍率が最小倍率より小さい場合は最小倍率を返す
            if (zoom < ZOOM_MIN)
            {
                return ZOOM_MIN;
            }

            return zoom;
        }

        /// <summary>
        /// 子供コントロール（コンテンツ）の表示を更新する処理
        /// </summary>
        /// <param name="zoom">ズーム値</param>
        /// <param name="pos">移動先位置</param>
        private void UpdateTransform(double zoom, Point pos)
        {
            TranslateTransform translate = new TranslateTransform(pos.X, pos.Y);
            ScaleTransform scale = new ScaleTransform(zoom, zoom);

            TransformGroup group = new TransformGroup();
            group.Children.Add(scale);
            group.Children.Add(translate);

            m_container.RenderTransform = group;
        }

        private Point m_translate;

        /// <summary>
        /// マウスウィールスクロールイベント処理
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);

            // マウスを上回転した場合は1.1倍拡大する
            if (e.Delta > 0)
            {
                SetCurrentValue(ZoomProperty, Zoom + ZOOM_STEP);
            }
            // マウスを下回転した場合は1.1倍縮小する
            else
            {
                SetCurrentValue(ZoomProperty, Zoom - ZOOM_STEP);
            }

            e.Handled = true;
        }

        /// <summary>
        /// マウスの左ボタンダブルクリックイベント処理
        /// </summary>
        /// <param name="e">イベントパラメータ</param>
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            SetCurrentValue(ZoomProperty, ZOOM_MIN);
        }

        private bool m_isDragging = false;
        private Point m_initPoint;

        /// <summary>
        /// マウスの左ボタン押下イベント処理
        /// </summary>
        /// <param name="e">イベントパラメータ</param>
        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            StartDrag();
        }

        /// <summary>
        /// マウスの左ボタンを離すイベント処理
        /// </summary>
        /// <param name="e">イベントパラメータ</param>
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            EndDrag();
        }

        /// <summary>
        /// コンテンツ移動開始処理
        /// </summary>
        private void StartDrag()
        {
            m_isDragging = true;
            m_initPoint = Mouse.GetPosition(m_container);

            m_container.CaptureMouse();
        }

        /// <summary>
        /// コンテンツ移動終了処理
        /// </summary>
        private void EndDrag()
        {
            if (false == m_isDragging)
            {
                return;
            }

            m_isDragging = false;
            m_container.ReleaseMouseCapture();
        }

        /// <summary>
        /// マウス移動イベント処理
        /// </summary>
        /// <param name="e">イベントパラメータ</param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (true == m_isDragging)
            {
                // 画像の移動ポイントを取得し、コントロールを更新する
                Point pos = e.GetPosition(m_container);
                m_translate.X += pos.X - m_initPoint.X;
                m_translate.Y += pos.Y - m_initPoint.Y;

                BoundTranslate(Zoom);
                UpdateTransform(Zoom, m_translate);
            }
        }

        /// <summary>
        /// マウスキャプチャーを解放するイベント処理
        /// </summary>
        /// <param name="e">イベントパラメータ</param>
        protected override void OnLostMouseCapture(MouseEventArgs e)
        {
            base.OnLostMouseCapture(e);

            EndDrag();
        }

        /// <summary>
        /// コンテンツがコントロールの領域を離れないようにイベント処理
        /// </summary>
        /// <param name="withZoom">イベントパラメータ</param>
        private void BoundTranslate(double withZoom)
        {
            // 画像が左にはみ出ないようにX座標を設定
            if (m_translate.X > 0)
            {
                m_translate.X = 0;
            }

            // 画像が上にはみ出ないようにY座標を設定
            if (m_translate.Y > 0)
            {
                m_translate.Y = 0;
            }

            // 画像が右にはみ出ないようにX座標を設定
            if (m_translate.X < -m_container.ActualWidth * withZoom + m_container.ActualWidth)
            {
                m_translate.X = -m_container.ActualWidth * withZoom + m_container.ActualWidth;
            }

            // 画像が下にはみ出ないようにY座標を設定
            if (m_translate.Y < -m_container.ActualHeight * withZoom + m_container.ActualHeight)
            {
                m_translate.Y = -m_container.ActualHeight * withZoom + m_container.ActualHeight;
            }
        }

        private FrameworkElement m_container;

        /// <summary>
        /// テンプレート指定イベント処理
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            m_container = (FrameworkElement)Template.FindName("GridOwner", this);
        }

        /// <summary>
        /// スタチックコンストラクタ
        /// </summary>
        static ZoomControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ZoomControl), new FrameworkPropertyMetadata(typeof(ZoomControl)));
            BackgroundProperty.OverrideMetadata(typeof(ZoomControl), new FrameworkPropertyMetadata(Brushes.Transparent));
        }
    }
}
