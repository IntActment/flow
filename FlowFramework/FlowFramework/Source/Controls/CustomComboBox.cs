﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FlowFramework.Controls
{
    public class CustomComboBox : ComboBox
    {
        public bool IsSelectedItemNotSet
        {
            get { return (bool)GetValue(IsSelectedItemNotSetProperty); }
        }

        public static readonly DependencyProperty IsSelectedItemNotSetProperty = DependencyProperty.Register(
            nameof(IsSelectedItemNotSet),
            typeof(bool),
            typeof(CustomComboBox),
            new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public object ItemNotSetValue
        {
            get { return (object)GetValue(ItemNotSetValueProperty); }
            set { SetValue(ItemNotSetValueProperty, value); }
        }

        public static readonly DependencyProperty ItemNotSetValueProperty = DependencyProperty.Register(
            nameof(ItemNotSetValue),
            typeof(object),
            typeof(CustomComboBox),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public bool IsSelectedItemInvalid
        {
            get { return (bool)GetValue(IsSelectedItemInvalidProperty); }
        }

        public static readonly DependencyProperty IsSelectedItemInvalidProperty = DependencyProperty.Register(
            nameof(IsSelectedItemInvalid),
            typeof(bool),
            typeof(CustomComboBox),
            new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public string ItemNotSetPlaceholder
        {
            get { return (string)GetValue(ItemNotSetPlaceholderProperty); }
            set { SetValue(ItemNotSetPlaceholderProperty, value); }
        }

        public static readonly DependencyProperty ItemNotSetPlaceholderProperty = DependencyProperty.Register(
            nameof(ItemNotSetPlaceholder),
            typeof(string),
            typeof(CustomComboBox),
            new FrameworkPropertyMetadata("!未設定!", FrameworkPropertyMetadataOptions.AffectsRender, null));

        public string ItemInvalidPlaceholder
        {
            get { return (string)GetValue(ItemInvalidPlaceholderProperty); }
            set { SetValue(ItemInvalidPlaceholderProperty, value); }
        }

        public static readonly DependencyProperty ItemInvalidPlaceholderProperty = DependencyProperty.Register(
            nameof(ItemInvalidPlaceholder),
            typeof(string),
            typeof(CustomComboBox),
            new FrameworkPropertyMetadata("!無効値!", FrameworkPropertyMetadataOptions.AffectsRender, null));

        public CustomComboBox() : base()
        {

        }

        static CustomComboBox()
        {
            SelectedItemProperty.OverrideMetadata(typeof(CustomComboBox), new FrameworkPropertyMetadata(null, null, CoerceSelectedItem));
        }

        private static object CoerceSelectedItem(DependencyObject d, object baseValue)
        {
            CustomComboBox cb = d as CustomComboBox;
            bool isInvalid = true;
            bool isDefault = true;
            bool isInvalidOld = (bool)cb.GetValue(IsSelectedItemInvalidProperty);
            bool isDefaultOld = (bool)cb.GetValue(IsSelectedItemNotSetProperty);
            int index = -1;

            try
            {
                if (null == cb.ItemsSource)
                {
                    return null;
                }

                if (Equals(baseValue, cb.ItemNotSetValue))
                {
                    return null;
                }

                if (null == cb.Items)
                {
                    return null;
                }

                isDefault = false;

                index = cb.Items.IndexOf(baseValue);

                if (-1 == index)
                {
                    return null;
                }

                isInvalid = false;
            }
            finally
            {
                cb.SetValue(SelectedIndexProperty, index);

                if (isInvalid != isInvalidOld)
                {
                    cb.SetValue(IsSelectedItemInvalidProperty, isInvalid);
                }

                if (isDefault != isDefaultOld)
                {
                    cb.SetValue(IsSelectedItemNotSetProperty, isDefault);
                }
            }

            return baseValue;
        }
    }
}
