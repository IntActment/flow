﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlowFramework.Controls
{
    /// <summary>
    /// Interaction logic for CustomToggleButton.xaml
    /// </summary>
    public partial class CustomToggleButton : ToggleButton
    {
        /// <summary>
        /// ボタン半径
        /// </summary>
        public double CornerRadius
        {
            get { return (double)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register(
            nameof(CornerRadius),
            typeof(double),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(3.0, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態の枠色
        /// </summary>
        public Brush BorderBrushPressed
        {
            get { return (Brush)GetValue(BorderBrushPressedProperty); }
            set { SetValue(BorderBrushPressedProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushPressedProperty = DependencyProperty.Register(
            nameof(BorderBrushPressed),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// ホバー状態の枠色
        /// </summary>
        public Brush BorderBrushHover
        {
            get { return (Brush)GetValue(BorderBrushHoverProperty); }
            set { SetValue(BorderBrushHoverProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushHoverProperty = DependencyProperty.Register(
            nameof(BorderBrushHover),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態の枠色
        /// </summary>
        public Brush BorderBrushChecked
        {
            get { return (Brush)GetValue(BorderBrushCheckedProperty); }
            set { SetValue(BorderBrushCheckedProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushCheckedProperty = DependencyProperty.Register(
            nameof(BorderBrushChecked),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態の枠色
        /// </summary>
        public Brush BorderBrushCheckedPressed
        {
            get { return (Brush)GetValue(BorderBrushCheckedPressedProperty); }
            set { SetValue(BorderBrushCheckedPressedProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushCheckedPressedProperty = DependencyProperty.Register(
            nameof(BorderBrushCheckedPressed),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// ホバー状態の枠色
        /// </summary>
        public Brush BorderBrushCheckedHover
        {
            get { return (Brush)GetValue(BorderBrushCheckedHoverProperty); }
            set { SetValue(BorderBrushCheckedHoverProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushCheckedHoverProperty = DependencyProperty.Register(
            nameof(BorderBrushCheckedHover),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// コントロール無効状態の枠色
        /// </summary>
        public Brush BorderBrushDisabled
        {
            get { return (Brush)GetValue(BorderBrushDisabledProperty); }
            set { SetValue(BorderBrushDisabledProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushDisabledProperty = DependencyProperty.Register(
            nameof(BorderBrushDisabled),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// コントロール無効状態の枠色
        /// </summary>
        public Brush BorderBrushCheckedDisabled
        {
            get { return (Brush)GetValue(BorderBrushCheckedDisabledProperty); }
            set { SetValue(BorderBrushCheckedDisabledProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushCheckedDisabledProperty = DependencyProperty.Register(
            nameof(BorderBrushCheckedDisabled),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態の背景色
        /// </summary>
        public Brush BackgroundPressed
        {
            get { return (Brush)GetValue(BackgroundPressedProperty); }
            set { SetValue(BackgroundPressedProperty, value); }
        }

        public static readonly DependencyProperty BackgroundPressedProperty = DependencyProperty.Register(
            nameof(BackgroundPressed),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.OrangeRed, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// ホバー状態の背景色
        /// </summary>
        public Brush BackgroundHover
        {
            get { return (Brush)GetValue(BackgroundHoverProperty); }
            set { SetValue(BackgroundHoverProperty, value); }
        }

        public static readonly DependencyProperty BackgroundHoverProperty = DependencyProperty.Register(
            nameof(BackgroundHover),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態の背景色
        /// </summary>
        public Brush BackgroundChecked
        {
            get { return (Brush)GetValue(BackgroundCheckedProperty); }
            set { SetValue(BackgroundCheckedProperty, value); }
        }

        public static readonly DependencyProperty BackgroundCheckedProperty = DependencyProperty.Register(
            nameof(BackgroundChecked),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.OrangeRed, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態の背景色
        /// </summary>
        public Brush BackgroundCheckedPressed
        {
            get { return (Brush)GetValue(BackgroundCheckedPressedProperty); }
            set { SetValue(BackgroundCheckedPressedProperty, value); }
        }

        public static readonly DependencyProperty BackgroundCheckedPressedProperty = DependencyProperty.Register(
            nameof(BackgroundCheckedPressed),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.OrangeRed, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// ホバー状態の背景色
        /// </summary>
        public Brush BackgroundCheckedHover
        {
            get { return (Brush)GetValue(BackgroundCheckedHoverProperty); }
            set { SetValue(BackgroundCheckedHoverProperty, value); }
        }

        public static readonly DependencyProperty BackgroundCheckedHoverProperty = DependencyProperty.Register(
            nameof(BackgroundCheckedHover),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// コントロール無効状態の背景色
        /// </summary>
        public Brush BackgroundDisabled
        {
            get { return (Brush)GetValue(BackgroundDisabledProperty); }
            set { SetValue(BackgroundDisabledProperty, value); }
        }

        public static readonly DependencyProperty BackgroundDisabledProperty = DependencyProperty.Register(
            nameof(BackgroundDisabled),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// コントロール無効状態の背景色
        /// </summary>
        public Brush BackgroundCheckedDisabled
        {
            get { return (Brush)GetValue(BackgroundCheckedDisabledProperty); }
            set { SetValue(BackgroundCheckedDisabledProperty, value); }
        }

        public static readonly DependencyProperty BackgroundCheckedDisabledProperty = DependencyProperty.Register(
            nameof(BackgroundCheckedDisabled),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態のテキスト色
        /// </summary>
        public Brush ForegroundPressed
        {
            get { return (Brush)GetValue(ForegroundPressedProperty); }
            set { SetValue(ForegroundPressedProperty, value); }
        }

        public static readonly DependencyProperty ForegroundPressedProperty = DependencyProperty.Register(
            nameof(ForegroundPressed),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// ホバー状態のテキスト色
        /// </summary>
        public Brush ForegroundHover
        {
            get { return (Brush)GetValue(ForegroundHoverProperty); }
            set { SetValue(ForegroundHoverProperty, value); }
        }

        public static readonly DependencyProperty ForegroundHoverProperty = DependencyProperty.Register(
            nameof(ForegroundHover),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態のテキスト色
        /// </summary>
        public Brush ForegroundChecked
        {
            get { return (Brush)GetValue(ForegroundCheckedProperty); }
            set { SetValue(ForegroundCheckedProperty, value); }
        }

        public static readonly DependencyProperty ForegroundCheckedProperty = DependencyProperty.Register(
            nameof(ForegroundChecked),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態のテキスト色
        /// </summary>
        public Brush ForegroundCheckedPressed
        {
            get { return (Brush)GetValue(ForegroundCheckedPressedProperty); }
            set { SetValue(ForegroundCheckedPressedProperty, value); }
        }

        public static readonly DependencyProperty ForegroundCheckedPressedProperty = DependencyProperty.Register(
            nameof(ForegroundCheckedPressed),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// ホバー状態のテキスト色
        /// </summary>
        public Brush ForegroundCheckedHover
        {
            get { return (Brush)GetValue(ForegroundCheckedHoverProperty); }
            set { SetValue(ForegroundCheckedHoverProperty, value); }
        }

        public static readonly DependencyProperty ForegroundCheckedHoverProperty = DependencyProperty.Register(
            nameof(ForegroundCheckedHover),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// コントロール無効状態のテキスト色
        /// </summary>
        public Brush ForegroundDisabled
        {
            get { return (Brush)GetValue(ForegroundDisabledProperty); }
            set { SetValue(ForegroundDisabledProperty, value); }
        }

        public static readonly DependencyProperty ForegroundDisabledProperty = DependencyProperty.Register(
            nameof(ForegroundDisabled),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// コントロール無効状態のテキスト色
        /// </summary>
        public Brush ForegroundCheckedDisabled
        {
            get { return (Brush)GetValue(ForegroundCheckedDisabledProperty); }
            set { SetValue(ForegroundCheckedDisabledProperty, value); }
        }

        public static readonly DependencyProperty ForegroundCheckedDisabledProperty = DependencyProperty.Register(
            nameof(ForegroundCheckedDisabled),
            typeof(Brush),
            typeof(CustomToggleButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// スタティックコンストラクタ
        /// </summary>
        static CustomToggleButton()
        {
            BackgroundProperty.OverrideMetadata(typeof(CustomToggleButton), new FrameworkPropertyMetadata(Brushes.White));
            ForegroundProperty.OverrideMetadata(typeof(CustomToggleButton), new FrameworkPropertyMetadata(Brushes.Black));
            BorderBrushProperty.OverrideMetadata(typeof(CustomToggleButton), new FrameworkPropertyMetadata(Brushes.LightGray));
            BorderThicknessProperty.OverrideMetadata(typeof(CustomToggleButton), new FrameworkPropertyMetadata(new Thickness(1.0)));
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CustomToggleButton() : base()
        {
            InitializeComponent();
        }
    }
}
