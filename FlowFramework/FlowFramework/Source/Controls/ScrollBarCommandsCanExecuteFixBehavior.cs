﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace FlowFramework.Controls
{
    public static class ScrollBarCommandsCanExecuteFixBehavior
    {
        #region Nested Types

        public class CommandCanExecuteMonitor<T> where T : UIElement
        {
            protected T Target { get; private set; }

            protected CommandCanExecuteMonitor(T target, RoutedCommand command)
            {
                Target = target;

                var binding = new CommandBinding(command);

                binding.CanExecute += OnCanExecute;

                target.CommandBindings.Add(binding);
            }

            protected virtual void OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
            {

            }
        }

        public class LineLeftCanExecuteMonitor : CommandCanExecuteMonitor<ScrollViewer>
        {
            public LineLeftCanExecuteMonitor(ScrollViewer scrollViewer)
                : base(scrollViewer, ScrollBar.LineLeftCommand)
            {
            }

            protected override void OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
            {
                if (e.Handled)
                {
                    return;
                }

                if (Equals(Target.HorizontalOffset, 0.0))
                {
                    e.CanExecute = false;
                    e.Handled = true;
                }
            }
        }

        public class LineRightCanExecuteMonitor : CommandCanExecuteMonitor<ScrollViewer>
        {
            public LineRightCanExecuteMonitor(ScrollViewer scrollViewer)
                : base(scrollViewer, ScrollBar.LineRightCommand)
            {
            }

            protected override void OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
            {
                if (e.Handled)
                {
                    return;
                }

                if (Equals(Target.HorizontalOffset, Target.ScrollableWidth))
                {
                    e.CanExecute = false;
                    e.Handled = true;
                }
            }
        }

        #endregion

        #region IsEnabled Attached Property

        public static bool GetIsEnabled(DependencyObject obj)
        {
            return (bool)obj.GetValue(IsEnabledProperty);
        }

        public static void SetIsEnabled(DependencyObject obj, bool value)
        {
            obj.SetValue(IsEnabledProperty, value);
        }

        public static readonly DependencyProperty IsEnabledProperty =
        DependencyProperty.RegisterAttached("IsEnabled", typeof (bool), typeof (ScrollBarCommandsCanExecuteFixBehavior), new PropertyMetadata(false, OnIsEnabledChanged));

        private static void OnIsEnabledChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                var scrollViewer = d as ScrollViewer;

                if (scrollViewer != null)
                {
                    OnAttached(scrollViewer);
                }
                else
                {
                    throw new NotSupportedException("This behavior only supports ScrollViewer instances.");
                }
            }
        }

        private static void OnAttached(ScrollViewer target)
        {
            SetLineLeftCanExecuteMonitor(target, new LineLeftCanExecuteMonitor(target));
            SetLineRightCanExecuteMonitor(target, new LineRightCanExecuteMonitor(target));
        }

        #endregion

        #region LineLeftCanExecuteMonitor Attached Property

        private static void SetLineLeftCanExecuteMonitor(DependencyObject obj, LineLeftCanExecuteMonitor value)
        {
            obj.SetValue(LineLeftCanExecuteMonitorProperty, value);
        }

        private static readonly DependencyProperty LineLeftCanExecuteMonitorProperty = DependencyProperty.RegisterAttached(
            "LineLeftCanExecuteMonitor",
            typeof (LineLeftCanExecuteMonitor),
            typeof (ScrollBarCommandsCanExecuteFixBehavior),
            new PropertyMetadata(null));

        #endregion

        #region LineRightCanExecuteMonitor Attached Property

        private static void SetLineRightCanExecuteMonitor(DependencyObject obj, LineRightCanExecuteMonitor value)
        {
            obj.SetValue(LineRightCanExecuteMonitorProperty, value);
        }

        private static readonly DependencyProperty LineRightCanExecuteMonitorProperty = DependencyProperty.RegisterAttached(
            "LineRightCanExecuteMonitor",
            typeof (LineRightCanExecuteMonitor),
            typeof (ScrollBarCommandsCanExecuteFixBehavior),
            new PropertyMetadata(null));

        #endregion
    }
}
