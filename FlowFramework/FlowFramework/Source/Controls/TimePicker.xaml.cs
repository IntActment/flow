﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using FlowFramework.Model;

namespace FlowFramework.Controls
{
    /// <summary>
    /// Interaction logic for TimePicker.xaml
    /// </summary>
    public partial class TimePicker : UserControl
    {
        public Time Time
        {
            get { return (Time)this.GetValue(TimeProperty); }
            set { this.SetValue(TimeProperty, value); }
        }

        private static readonly DependencyProperty TimeProperty = DependencyProperty.Register(
            nameof(Time),
            typeof(Time),
            typeof(TimePicker),
            new FrameworkPropertyMetadata(
                new Time(0, 0, 0),
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                TimeChanged,
                CoerceTime,
                false,
                UpdateSourceTrigger.PropertyChanged));

        private static object CoerceTime(DependencyObject d, object value)
        {
            Time time = (Time)value;

            if (null == time)
            {
                return TimeProperty.DefaultMetadata.DefaultValue;
            }

            if (true == time.IsValid)
            {
                return time;
            }

            return new Time(
                (byte)Math.Min(time.Hour, 23u),
                (byte)Math.Min(time.Minute, 59u),
                (byte)0);
        }

        private static void TimeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Time time = (Time)e.NewValue;

            d.SetCurrentValue(HourProperty, time.Hour);
            d.SetCurrentValue(MinuteProperty, time.Minute);
        }

        public byte Hour
        {
            get { return (byte)this.GetValue(HourProperty); }
            set { this.SetValue(HourProperty, value); }
        }

        private static readonly DependencyProperty HourProperty = DependencyProperty.Register(
            nameof(Hour),
            typeof(byte),
            typeof(TimePicker),
            new FrameworkPropertyMetadata(
                (byte)0,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                HourChanged,
                CoerceHour,
                false,
                UpdateSourceTrigger.PropertyChanged));

        private static object CoerceHour(DependencyObject d, object value)
        {
            byte hour = (byte)value;

            return (byte)Math.Min(hour, 23u);
        }

        private static void HourChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TimePicker picker = (TimePicker)d;
            byte hour = (byte)e.NewValue;

            d.SetCurrentValue(TimeProperty, new Time(hour, picker.Time.Minute, (byte)0));
        }

        public byte Minute
        {
            get { return (byte)this.GetValue(MinuteProperty); }
            set { this.SetValue(MinuteProperty, value); }
        }

        private static readonly DependencyProperty MinuteProperty = DependencyProperty.Register(
            nameof(Minute),
            typeof(byte),
            typeof(TimePicker),
            new FrameworkPropertyMetadata(
                (byte)0,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                MinuteChanged,
                CoerceMinute,
                false,
                UpdateSourceTrigger.PropertyChanged));

        private static object CoerceMinute(DependencyObject d, object value)
        {
            byte minute = (byte)value;

            return (byte)Math.Min(minute, 59u);
        }

        private static void MinuteChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TimePicker picker = (TimePicker)d;
            byte minute = (byte)e.NewValue;

            d.SetCurrentValue(TimeProperty, new Time(picker.Time.Hour, minute, (byte)0));
        }

        public TimePicker()
        {
            InitializeComponent();
        }

        private CustomTextBox m_textHour;
        private CustomTextBox m_textMinute;

        /// <summary>
        /// OnApplyTemplateメソッド(カスタムコントロールにテンプレートが適用されたときに呼び出される)
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            m_textHour = (CustomTextBox)Template.FindName("TextHour", this);
            m_textMinute = (CustomTextBox)Template.FindName("TextMinute", this);

            InputMethod.SetIsInputMethodEnabled(m_textHour, false);
            InputMethod.SetIsInputMethodEnabled(m_textMinute, false);
        }

        public void IncHour()
        {
            byte h = Time.Hour;

            if (h == 23)
            {
                h = 0;
            }
            else
            {
                h++;
            }

            SetCurrentValue(HourProperty, h);
        }

        public void DecHour()
        {
            byte h = Time.Hour;

            if (h == 0)
            {
                h = 23;
            }
            else
            {
                h--;
            }

            SetCurrentValue(HourProperty, h);
        }

        public void IncMinute()
        {
            byte m = Time.Minute;

            if (m == 59)
            {
                m = 0;
            }
            else
            {
                m++;
            }

            SetCurrentValue(MinuteProperty, m);
        }

        public void DecMinute()
        {
            byte m = Time.Minute;

            if (m == 0)
            {
                m = 59;
            }
            else
            {
                m--;
            }

            SetCurrentValue(MinuteProperty, m);
        }

        private void HourUpPress(object sender, MouseButtonEventArgs e)
        {
            IncHour();
        }

        private void MinuteUpPress(object sender, MouseButtonEventArgs e)
        {
            IncMinute();
        }

        private void HourDownPress(object sender, MouseButtonEventArgs e)
        {
            DecHour();
        }

        private void MinuteDownPress(object sender, MouseButtonEventArgs e)
        {
            DecMinute();
        }

        private void TextHourKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up)
            {
                IncHour();
            }
            else if (e.Key == Key.Down)
            {
                DecHour();
            }
        }

        private void TextMinuteKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up)
            {
                IncMinute();
            }
            else if (e.Key == Key.Down)
            {
                DecMinute();
            }
        }

        private void FilterInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;

            var fullText = textBox.Text.Insert(textBox.SelectionStart, e.Text);

            byte val;

            e.Handled = false == byte.TryParse(fullText, out val);
        }
    }
}
