﻿<Button
    x:Class="FlowFramework.Controls.CustomButton"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:local="clr-namespace:FlowFramework.Controls"
    xmlns:ff="clr-namespace:FlowFramework"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    d:DesignHeight="450"
    d:DesignWidth="800"
    FocusVisualStyle="{x:Null}"
    Focusable="True"
    IsTabStop="False"
    mc:Ignorable="d">
    <Button.Resources>
        <ResourceDictionary>
            <Style
                x:Key="ButtonText"
                TargetType="{x:Type TextBlock}"
                BasedOn="{StaticResource {x:Type TextBlock}}">
                <Setter Property="Foreground" Value="{Binding Foreground, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"/>
                <Style.Triggers>
                    <DataTrigger
                        Binding="{Binding IsMouseOver, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"
                        Value="True">
                        <Setter Property="Foreground" Value="{Binding ForegroundHover, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"/>
                    </DataTrigger>
                    <DataTrigger
                        Binding="{Binding IsPressed, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"
                        Value="True">
                        <Setter Property="Foreground" Value="{Binding ForegroundPressed, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"/>
                    </DataTrigger>
                    <DataTrigger
                        Binding="{Binding IsEnabled, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"
                        Value="False">
                        <Setter Property="Foreground" Value="{Binding ForegroundDisabled, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"/>
                    </DataTrigger>
                </Style.Triggers>
            </Style>
        </ResourceDictionary>
    </Button.Resources>

    <Button.Template>
        <ControlTemplate TargetType="{x:Type local:CustomButton}">
            <ControlTemplate.Resources>
                <Style
                    TargetType="{x:Type TextBlock}"
                    BasedOn="{StaticResource ButtonText}"/>

                <Style TargetType="{x:Type Path}">
                    <Setter Property="Fill" Value="{Binding Foreground, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"/>
                    <Setter Property="Stroke" Value="{x:Null}"/>
                    <Setter Property="StrokeThickness" Value="0"/>
                    <Style.Triggers>
                        <DataTrigger
                            Binding="{Binding IsMouseOver, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"
                            Value="True">
                            <Setter Property="Fill" Value="{Binding ForegroundHover, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"/>
                        </DataTrigger>
                        <DataTrigger
                            Binding="{Binding IsPressed, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"
                            Value="True">
                            <Setter Property="Fill" Value="{Binding ForegroundPressed, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"/>
                        </DataTrigger>
                        <DataTrigger
                            Binding="{Binding IsEnabled, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"
                            Value="False">
                            <Setter Property="Fill" Value="{Binding ForegroundDisabled, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"/>
                        </DataTrigger>
                    </Style.Triggers>
                </Style>

                <Style TargetType="{x:Type Ellipse}">
                    <Setter Property="Fill" Value="{DynamicResource FlowFramework.Image.Foreground.Normal}"/>
                    <Setter Property="Stroke" Value="{x:Null}"/>
                    <Setter Property="StrokeThickness" Value="0"/>
                    <Style.Triggers>
                        <DataTrigger
                            Binding="{Binding IsMouseOver, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"
                            Value="True">
                            <Setter Property="Fill" Value="{Binding ForegroundHover, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"/>
                        </DataTrigger>
                        <DataTrigger
                            Binding="{Binding IsPressed, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"
                            Value="True">
                            <Setter Property="Fill" Value="{Binding ForegroundPressed, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"/>
                        </DataTrigger>
                        <DataTrigger
                            Binding="{Binding IsEnabled, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"
                            Value="False">
                            <Setter Property="Fill" Value="{Binding ForegroundDisabled, RelativeSource={RelativeSource Mode=FindAncestor, AncestorType=local:CustomButton}}"/>
                        </DataTrigger>
                    </Style.Triggers>
                </Style>
            </ControlTemplate.Resources>

            <Grid x:Name="ContentSite">
                <Grid
                    x:Name="ContainerGrid"
                    Background="Transparent"
                    Height="{Binding Height, RelativeSource={RelativeSource TemplatedParent}}"
                    Width="{Binding Width, RelativeSource={RelativeSource TemplatedParent}}">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition
                            Width="*"
                            SharedSizeGroup="{Binding Path=(local:AttachedProps.SharedSizeGroup), RelativeSource={RelativeSource TemplatedParent}}"/>
                    </Grid.ColumnDefinitions>

                    <Border
                        Grid.Column="0"
                        x:Name="ButtonBorder"
                        CornerRadius="{Binding CornerRadius, RelativeSource={RelativeSource TemplatedParent}}"
                        Padding="{Binding Padding, RelativeSource={RelativeSource TemplatedParent}}"
                        HorizontalAlignment="Stretch"
                        VerticalAlignment="Stretch"
                        Background="{Binding Background, RelativeSource={RelativeSource TemplatedParent}}"
                        BorderThickness="{Binding BorderThickness, RelativeSource={RelativeSource TemplatedParent}}"
                        BorderBrush="{Binding BorderBrush, RelativeSource={RelativeSource TemplatedParent}}">

                        <ContentPresenter
                            x:Name="Presenter"
                            Margin="0"
                            Content="{TemplateBinding Content}"
                            HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}"
                            VerticalAlignment="{TemplateBinding VerticalContentAlignment}"
                            ClipToBounds="{TemplateBinding ClipToBounds}"
                            TextElement.Foreground="{Binding Foreground, RelativeSource={RelativeSource TemplatedParent}}"/>
                    </Border>
                </Grid>
            </Grid>

            <ControlTemplate.Triggers>
                <Trigger Property="IsMouseOver" Value="True">
                    <Setter TargetName="ButtonBorder" Property="Background" Value="{Binding BackgroundHover, RelativeSource={RelativeSource TemplatedParent}}"/>
                    <Setter TargetName="Presenter" Property="TextElement.Foreground" Value="{Binding ForegroundHover, RelativeSource={RelativeSource TemplatedParent}}"/>
                    <Setter TargetName="ButtonBorder" Property="BorderBrush" Value="{Binding BorderBrushHover, RelativeSource={RelativeSource TemplatedParent}}"/>
                </Trigger>
                <Trigger Property="IsPressed" Value="True">
                    <Setter TargetName="ButtonBorder" Property="Background" Value="{Binding BackgroundPressed, RelativeSource={RelativeSource TemplatedParent}}"/>
                    <Setter TargetName="Presenter" Property="TextElement.Foreground" Value="{Binding ForegroundPressed, RelativeSource={RelativeSource TemplatedParent}}"/>
                    <Setter TargetName="ButtonBorder" Property="BorderBrush" Value="{Binding BorderBrushPressed, RelativeSource={RelativeSource TemplatedParent}}"/>
                </Trigger>
                <Trigger Property="IsEnabled" Value="False">
                    <Setter TargetName="ButtonBorder" Property="Background" Value="{Binding BackgroundDisabled, RelativeSource={RelativeSource TemplatedParent}}"/>
                    <Setter TargetName="Presenter" Property="TextElement.Foreground" Value="{Binding ForegroundDisabled, RelativeSource={RelativeSource TemplatedParent}}"/>
                    <Setter TargetName="ButtonBorder" Property="BorderBrush" Value="{Binding BorderBrushDisabled, RelativeSource={RelativeSource TemplatedParent}}"/>
                </Trigger>
            </ControlTemplate.Triggers>
        </ControlTemplate>
    </Button.Template>
</Button>
