﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace FlowFramework.Controls
{
    public class Tab : FrameworkElement
    {
        public enum TabDirection
        {
            Left,
            Top,
        };

        public TabDirection Direction
        {
            get { return (TabDirection)GetValue(DirectionProperty); }
            set { SetValue(DirectionProperty, value); }
        }

        public static readonly DependencyProperty DirectionProperty = DependencyProperty.Register(
            nameof(Direction),
            typeof(TabDirection),
            typeof(Tab),
            new FrameworkPropertyMetadata(TabDirection.Left, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public bool HasRootBorder
        {
            get { return (bool)GetValue(HasRootBorderProperty); }
            set { SetValue(HasRootBorderProperty, value); }
        }

        public static readonly DependencyProperty HasRootBorderProperty = DependencyProperty.Register(
            nameof(HasRootBorder),
            typeof(bool),
            typeof(Tab),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public Brush Stroke
        {
            get { return (Brush)GetValue(StrokeProperty); }
            set { SetValue(StrokeProperty, value); }
        }

        public static readonly DependencyProperty StrokeProperty = DependencyProperty.Register(
            nameof(Stroke),
            typeof(Brush),
            typeof(Tab),
            new FrameworkPropertyMetadata(Brushes.Black, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public Brush RootBorder
        {
            get { return (Brush)GetValue(RootBorderProperty); }
            set { SetValue(RootBorderProperty, value); }
        }

        public static readonly DependencyProperty RootBorderProperty = DependencyProperty.Register(
            nameof(RootBorder),
            typeof(Brush),
            typeof(Tab),
            new FrameworkPropertyMetadata(Brushes.Red, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public double RootBorderThickness
        {
            get { return (double)GetValue(RootBorderThicknessProperty); }
            set { SetValue(RootBorderThicknessProperty, value); }
        }

        public static readonly DependencyProperty RootBorderThicknessProperty = DependencyProperty.Register(
            nameof(RootBorderThickness),
            typeof(double),
            typeof(Tab),
            new FrameworkPropertyMetadata(3.0, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public Brush Fill
        {
            get { return (Brush)GetValue(FillProperty); }
            set { SetValue(FillProperty, value); }
        }

        public static readonly DependencyProperty FillProperty = DependencyProperty.Register(
            "Fill",
            typeof(Brush),
            typeof(Tab),
            new FrameworkPropertyMetadata(Brushes.Transparent, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public double StrokeThickness
        {
            get { return (double)GetValue(StrokeThicknessProperty); }
            set { SetValue(StrokeThicknessProperty, value); }
        }

        public static readonly DependencyProperty StrokeThicknessProperty = DependencyProperty.Register(
            nameof(StrokeThickness),
            typeof(double),
            typeof(Tab),
            new FrameworkPropertyMetadata(1.0, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public double Bevel
        {
            get { return (double)GetValue(BevelProperty); }
            set { SetValue(BevelProperty, value); }
        }

        public static readonly DependencyProperty BevelProperty = DependencyProperty.Register(
            nameof(Bevel),
            typeof(double),
            typeof(Tab),
            new FrameworkPropertyMetadata(20.0, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public Tab()
        {
            ClipToBounds = false;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if ((ActualWidth == 0) || (ActualHeight == 0))
            {
                return;
            }

            PathSegment[] segments = null;
            PathGeometry g = null;

            if (Direction == TabDirection.Left)
            {
                double width = ActualWidth - RootBorderThickness;

                segments = new PathSegment[]
                {
                    // upper part
                    new ArcSegment(new Point(width * 0.21875, Bevel * 0.6), new Size(width * 0.375, Bevel * 0.6), 90, false, SweepDirection.Clockwise, true),
                    new LineSegment(new Point(ActualWidth, StrokeThickness * 0.5), true),

                    // up to down
                    new LineSegment(new Point(ActualWidth, ActualHeight + StrokeThickness * 0.5), false),

                    // bottom part
                    new LineSegment(new Point(width * 0.21875, ActualHeight - Bevel * 0.6), true),
                    new ArcSegment(new Point(StrokeThickness * 0.5, ActualHeight - Bevel), new Size(width * 0.375, Bevel * 0.6), 90, false, SweepDirection.Clockwise, true),
                };

                g = new PathGeometry(new[] { new PathFigure(new Point(StrokeThickness * 0.5, Bevel), segments, true) });

                drawingContext.DrawGeometry(Fill, new Pen(Stroke, StrokeThickness), g);

                if (true == HasRootBorder)
                {
                    drawingContext.DrawLine(new Pen(RootBorder, RootBorderThickness), new Point(width + RootBorderThickness * 0.5, RootBorderThickness * 0.5), new Point(width + RootBorderThickness * 0.5, ActualHeight + RootBorderThickness * 0.5));
                }
            }
            else if (Direction == TabDirection.Top)
            {
                double height = ActualHeight - RootBorderThickness;

                segments = new PathSegment[]
                {
                    // right part
                    new ArcSegment(new Point(Bevel * 0.6, height * 0.21875), new Size(Bevel * 0.6, height * 0.375), 90, false, SweepDirection.Counterclockwise, true),
                    new LineSegment(new Point(StrokeThickness * 0.5, ActualHeight - StrokeThickness), true),

                    new LineSegment(new Point(StrokeThickness * 0.5, ActualHeight), false),

                    // right to left
                    new LineSegment(new Point(ActualWidth + StrokeThickness * 0.5, ActualHeight), false),

                    // left part
                    new LineSegment(new Point(ActualWidth + StrokeThickness * 0.5, ActualHeight - StrokeThickness), false),
                    new LineSegment(new Point(ActualWidth - Bevel * 0.6, height * 0.21875), true),
                    new ArcSegment(new Point(ActualWidth - Bevel, StrokeThickness * 0.5), new Size(Bevel * 0.6, height * 0.375), 90, false, SweepDirection.Counterclockwise, true),
                };

                g = new PathGeometry(new[] { new PathFigure(new Point(Bevel, StrokeThickness * 0.5), segments, true) });

                drawingContext.DrawGeometry(Fill, new Pen(Stroke, StrokeThickness), g);

                if (true == HasRootBorder)
                {
                    drawingContext.DrawLine(new Pen(RootBorder, RootBorderThickness), new Point(0, height + RootBorderThickness * 0.5), new Point(ActualWidth + RootBorderThickness, height + RootBorderThickness * 0.5));
                }
            }
        }
    }
}
