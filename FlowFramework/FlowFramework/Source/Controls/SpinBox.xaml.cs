﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlowFramework.Controls
{
    /// <summary>
    /// 数字入力コントロール
    /// </summary>
    public partial class SpinBox : ContentControl
    {
        #region MinValue

        /// <summary>
        /// 最小値
        /// </summary>
        public int MinValue
        {
            get { return (int)GetValue(MinValueProperty); }
            set { SetValue(MinValueProperty, value); }
        }

        private static readonly DependencyProperty MinValueProperty = DependencyProperty.Register(
            nameof(MinValue),
            typeof(int),
            typeof(SpinBox),
            new FrameworkPropertyMetadata(
                0,
                FrameworkPropertyMetadataOptions.AffectsRender,
                MinValueChanged,
                CoerceMinValue,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// スピンボックス最小値設定処理
        /// </summary>
        /// <param name="d">スピンボックスコントロール</param>
        /// <param name="value">スピンボックスの最小値</param>
        /// <returns>最小値</returns>
        private static object CoerceMinValue(DependencyObject d, object value)
        {
            int minVal = (int)value;
            SpinBox sb = (SpinBox)d;

            // 該当スピンボックスの最大値より大きい場合は最大値を返す
            if ((sb.ReadLocalValue(MaxValueProperty) != DependencyProperty.UnsetValue) && (sb.MaxValue < minVal))
            {
                return sb.MaxValue;
            }

            // 上記以外は最小値を返す
            return minVal;
        }

        /// <summary>
        /// 最小値変更時処理
        /// </summary>
        /// <param name="d">スピンボックスコントロール</param>
        /// <param name="e">イベントパラメータ</param>
        private static void MinValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            int minVal = (int)e.NewValue;
            SpinBox sb = (SpinBox)d;

            // 前回の値より大きい場合は、最小値を更新する
            if ((sb.ReadLocalValue(MinValueProperty) != DependencyProperty.UnsetValue) && (sb.Value < minVal))
            {
                sb.SetCurrentValue(ValueProperty, minVal);
            }
        }

        #endregion

        #region MaxValue

        /// <summary>
        /// 最大値
        /// </summary>
        public int MaxValue
        {
            get { return (int)GetValue(MaxValueProperty); }
            set { SetValue(MaxValueProperty, value); }
        }

        private static readonly DependencyProperty MaxValueProperty = DependencyProperty.Register(
            nameof(MaxValue),
            typeof(int),
            typeof(SpinBox),
            new FrameworkPropertyMetadata(
                -1,
                FrameworkPropertyMetadataOptions.AffectsRender,
                MaxValueChanged,
                CoerceMaxValue,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// スピンボックスの最大値設定処理
        /// </summary>
        /// <param name="d">スピンボックスコントロール</param>
        /// <param name="value">スピンボックスの最大値</param>
        /// <returns>最小値</returns>
        private static object CoerceMaxValue(DependencyObject d, object value)
        {
            int maxVal = (int)value;
            SpinBox sb = (SpinBox)d;

            // 最大値が-1の場合は-1を返す
            if (-1 == maxVal)
            {
                return maxVal;
            }

            // 最小値より小さい場合は最小値を返す
            if ((sb.ReadLocalValue(MinValueProperty) != DependencyProperty.UnsetValue) && (sb.MinValue > maxVal))
            {
                return sb.MinValue;
            }

            // 上記以外は最大値を返す
            return maxVal;
        }

        /// <summary>
        /// 最大値変更時処理
        /// </summary>
        /// <param name="d">スピンボックスコントロール</param>
        /// <param name="e">イベントパラメータ</param>
        private static void MaxValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            int maxVal = (int)e.NewValue;
            SpinBox sb = (SpinBox)d;

            // 前回の値より小さい場合は、最大値を更新する
            if ((sb.ReadLocalValue(MaxValueProperty) != DependencyProperty.UnsetValue) && (sb.Value > maxVal))
            {
                sb.SetCurrentValue(ValueProperty, maxVal);
            }
        }

        #endregion

        #region Value

        /// <summary>
        /// 値
        /// </summary>
        public int Value
        {
            get { return (int)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        private static readonly DependencyProperty ValueProperty = DependencyProperty.Register(
            nameof(Value),
            typeof(int),
            typeof(SpinBox),
            new FrameworkPropertyMetadata(
                0,
                FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                ValueChanged,
                null,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// 最大値変更時処理
        /// </summary>
        /// <param name="d">スピンボックスコントロール</param>
        /// <param name="e">イベントパラメータ</param>
        private static void ValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            int val = (int)e.NewValue;
            SpinBox sb = (SpinBox)d;

            // 最小値が-1でないかつ入力値が最小値より小さい場合は最小値を設定する
            if ((sb.ReadLocalValue(MinValueProperty) != DependencyProperty.UnsetValue) && (sb.MinValue > val))
            {
                sb.SetCurrentValue(ValueProperty, sb.MinValue);
            }

            // 最大値が-1でないかつ入力値が最大値より大きい場合は最大値を設定する
            if ((sb.ReadLocalValue(MaxValueProperty) != DependencyProperty.UnsetValue) && (sb.MaxValue < val))
            {
                sb.SetCurrentValue(ValueProperty, sb.MaxValue);
            }

            // 上記以外は入力値を設定する
            sb.UpdateButtons(val);
        }

        #endregion

        #region IsValid

        /// <summary>
        /// 値の有効状態フラグ
        /// </summary>
        public bool IsValid
        {
            get { return (bool)GetValue(IsValidProperty); }
            set { SetValue(IsValidProperty, value); }
        }

        private static readonly DependencyProperty IsValidProperty = DependencyProperty.Register(
            nameof(IsValid),
            typeof(bool),
            typeof(SpinBox),
            new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.AffectsRender, null));

        #endregion

        #region TextBoxStyle

        /// <summary>
        /// コントロール内のテキストボックスコントロールのスタイル
        /// </summary>
        public Style TextBoxStyle
        {
            get { return (Style)GetValue(TextBoxStyleProperty); }
            set { SetValue(TextBoxStyleProperty, value); }
        }

        private static readonly DependencyProperty TextBoxStyleProperty = DependencyProperty.Register(
            nameof(TextBoxStyle),
            typeof(Style),
            typeof(SpinBox),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsMeasure, null));

        #endregion

        #region ButtonStyle

        /// <summary>
        /// コントロール内のボタンのスタイル
        /// </summary>
        public Style ButtonStyle
        {
            get { return (Style)GetValue(ButtonStyleProperty); }
            set { SetValue(ButtonStyleProperty, value); }
        }

        private static readonly DependencyProperty ButtonStyleProperty = DependencyProperty.Register(
            nameof(ButtonStyle),
            typeof(Style),
            typeof(SpinBox),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsMeasure, null));

        #endregion

        /// <summary>
        /// 値をインクリメントする処理
        /// </summary>
        private void Increment()
        {
            SetCurrentValue(ValueProperty, Value + 1);
            textbox.Focus();
        }

        /// <summary>
        /// 値をディクリメントする処理
        /// </summary>
        private void Decrement()
        {
            SetCurrentValue(ValueProperty, Value - 1);
            textbox.Focus();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public SpinBox()
        {
            SetCurrentValue(TextBoxStyleProperty, Application.Current.TryFindResource(typeof(CustomTextBox)) as Style);
            SetCurrentValue(ButtonStyleProperty, Application.Current.TryFindResource(typeof(CustomButton)) as Style);

            InitializeComponent();

            InputMethod.SetIsInputMethodEnabled(textbox, false);
            DataObject.AddPastingHandler(textbox, OnPaste);

            UpdateButtons(Value);
        }

        /// <summary>
        /// 貼り付けイベント処理
        /// </summary>
        /// <param name="sender">センダー</param>
        /// <param name="e">イベントパラメータ</param>
        private void OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            var isText = e.SourceDataObject.GetDataPresent(DataFormats.UnicodeText, true);
            if (false == isText)
            {
                e.CancelCommand();
                return;
            }

            var text = e.SourceDataObject.GetData(DataFormats.UnicodeText) as string;

            if (false == ValidateInput(text))
            {
                e.CancelCommand();
            }
        }

        /// <summary>
        /// コントロールの入力フィルター処理
        /// </summary>
        /// <param name="sender">テキストボックスコントロール対象</param>
        /// <param name="e">イベントパラメータ</param>
        private void FilterInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;

            ///選択領域差し替え
            var fullText = textBox.Text.Remove(textBox.SelectionStart, textBox.SelectionLength);
            fullText = fullText.Insert(textBox.SelectionStart, e.Text);

            e.Handled = (false == int.TryParse(fullText, NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out int val));
        }

        /// <summary>
        /// 入力テキストの有効・無効チェック
        /// </summary>
        /// <param name="text">入力テキスト</param>
        /// <returns>
        /// true：有効
        /// false：無効
        /// </returns>
        private bool ValidateInput(string text)
        {
            if (false == int.TryParse(text, NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture, out int val))
            {
                return false;
            }

            // 最小値設定がある場合、最小値チェックもする
            if ((ReadLocalValue(MinValueProperty) != DependencyProperty.UnsetValue) && (val < MinValue))
            {
                return false;
            }

            // 最大値設定がある場合、最大値チェックもする
            if ((ReadLocalValue(MaxValueProperty) != DependencyProperty.UnsetValue) && (val > MaxValue))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// テキストボックスにキーを入力するイベント処理
        /// </summary>
        /// <param name="sender">テキストボックスコントロール対象</param>
        /// <param name="e">イベントパラメータ</param>
        private void InputKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up)
            {
                Increment();
            }
            else if (e.Key == Key.Down)
            {
                Decrement();
            }
            else if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// ボタンの有効/無効状態を更新する処理
        /// </summary>
        /// <param name="value"></param>
        private void UpdateButtons(int value)
        {
            UpButton.IsEnabled = (MaxValue == -1) ^ (value < MaxValue);
            DownButton.IsEnabled = (MinValue == -1) ^ (value > MinValue);
        }

        /// <summary>
        /// 上ボタンのクリックイベント処理
        /// </summary>
        /// <param name="sender">上ボタン対象</param>
        /// <param name="e">イベントパラメータ</param>
        private void UpButton_Click(object sender, RoutedEventArgs e)
        {
            Increment();
        }

        /// <summary>
        /// 下ボタンのクリックイベント処理
        /// </summary>
        /// <param name="sender">下ボタン対象</param>
        /// <param name="e">イベントパラメータ</param>
        private void DownButton_Click(object sender, RoutedEventArgs e)
        {
            Decrement();
        }
    }
}
