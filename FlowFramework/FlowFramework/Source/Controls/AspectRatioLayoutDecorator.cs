﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace FlowFramework.Controls
{
    /// <summary>
    /// アスペクト比をもとに画像コントロールサイズ計算クラス
    /// </summary>
    public class AspectRatioLayoutDecorator : Decorator
    {
        public static readonly DependencyProperty AspectRatioProperty =
           DependencyProperty.Register(
              nameof(AspectRatio),
              typeof(double),
              typeof(AspectRatioLayoutDecorator),
              new FrameworkPropertyMetadata(1d, FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsArrange),
              ValidateAspectRatio);

        /// <summary>
        /// 値有効化
        /// </summary>
        /// <param name="value">アスペクト比</param>
        /// <returns>アスペクト比がdouble型の数値かどうか</returns>
        private static bool ValidateAspectRatio(object value)
        {
            if (!(value is double))
            {
                return false;
            }

            var aspectRatio = (double)value;

            // アスペクト比が0より大きい数値かどうか
            return aspectRatio > 0
                && (false == double.IsInfinity(aspectRatio))
                && (false == double.IsNaN(aspectRatio));
        }

        /// <summary>
        /// アスペクト比
        /// </summary>
        public double AspectRatio
        {
            get { return (double)GetValue(AspectRatioProperty); }
            set { SetValue(AspectRatioProperty, value); }
        }

        /// <summary>
        /// コントロールサイズ計算する処理
        /// </summary>
        /// <param name="constraint"></param>
        /// <returns></returns>
        protected override Size MeasureOverride(Size constraint)
        {
            if (Child != null)
            {
                constraint = SizeToRatio(constraint, false);
                Child.Measure(constraint);

                if (double.IsInfinity(constraint.Width)
                   || double.IsInfinity(constraint.Height))
                {
                    return SizeToRatio(Child.DesiredSize, true);
                }

                return constraint;
            }

            // 子コントロールがないのため、領域が空
            return new Size(0, 0);
        }

        /// <summary>
        /// アスペクト比を計算する処理
        /// </summary>
        /// <param name="size">親コントロール</param>
        /// <param name="expand">拡大フラグ</param>
        /// <returns>計算されたコントロールサイズ</returns>
        public Size SizeToRatio(Size size, bool expand)
        {
            double ratio = AspectRatio;

            // 幅と高さを算出
            double height = size.Width / ratio;
            double width = size.Height * ratio;

            // 拡大フラグありの場合は最大値を設定する
            if (expand)
            {
                width = Math.Max(width, size.Width);
                height = Math.Max(height, size.Height);
            }
            // 拡大フラグなしの場合は最小値を設定する
            else
            {
                width = Math.Min(width, size.Width);
                height = Math.Min(height, size.Height);
            }

            return new Size(width, height);
        }

        /// <summary>
        /// 子コントロールサイズ
        /// </summary>
        /// <param name="arrangeSize">入力サイズ</param>
        /// <returns>出力サイズ</returns>
        protected override Size ArrangeOverride(Size arrangeSize)
        {
            if (Child != null)
            {
                // アスペクト比を取得する(拡大フラグなし)
                var newSize = SizeToRatio(arrangeSize, false);

                // 幅、高さを算出
                double widthDelta = arrangeSize.Width - newSize.Width;
                double heightDelta = arrangeSize.Height - newSize.Height;

                double top = 0;
                double left = 0;

                // 幅が正常値の場合コントロールの左側のサイズを算出
                if ((false == double.IsNaN(widthDelta))
                   && (false == double.IsInfinity(widthDelta)))
                {
                    left = widthDelta / 2.0;
                }

                // 幅が正常値の場合コントロールの右側のサイズを算出
                if ((false == double.IsNaN(heightDelta))
                   && (false == double.IsInfinity(heightDelta)))
                {
                    top = heightDelta / 2.0;
                }

                var finalRect = new Rect(new Point(left, top), newSize);
                Child.Arrange(finalRect);
            }

            return arrangeSize;
        }
    }
}
