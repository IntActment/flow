﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using FlowFramework.Model;
using FlowFramework.Utils;

namespace FlowFramework.Controls
{
    /// <summary>
    /// Interaction logic for IPv4Control.xaml
    /// </summary>
    public partial class IPv4Control : UserControl
    {
        /// <summary>
        /// 変換メソッドであるCoerceの呼び出しが無限ループに入らないようにフラグ
        /// </summary>
        private bool m_internalChange = false;

        #region IsValid

        /// <summary>
        /// 有効状態フラグ
        /// </summary>
        public bool IsValid
        {
            get { return (bool)GetValue(IsValidProperty); }
            set { SetValue(IsValidProperty, value); }
        }

        private static readonly DependencyProperty IsValidProperty = DependencyProperty.Register(
            nameof(IsValid),
            typeof(bool),
            typeof(IPv4Control),
            new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.AffectsRender, null));

        #endregion

        #region IPAddress

        /// <summary>
        /// IPアドレス（無効なアドレスはnull）
        /// </summary>
        public IPv4? IPAddress
        {
            get { return (IPv4?)GetValue(IPAddressProperty); }
            set { SetValue(IPAddressProperty, value); }
        }

        public static readonly DependencyProperty IPAddressProperty = DependencyProperty.Register(
            nameof(IPAddress),
            typeof(IPv4?),
            typeof(IPv4Control),
            new FrameworkPropertyMetadata(
                (IPv4?)null,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                IPAddressChanged,
                CoerceIPAddress,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// IPアドレスチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>IPアドレス</returns>
        private static object CoerceIPAddress(DependencyObject d, object value)
        {
            IPv4? val = (IPv4?)value;

            if ((null != val) && (false == val.Value.IsValid))
            {
                return null;
            }

            return val;
        }

        /// <summary>
        /// IPアドレス変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void IPAddressChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            IPv4? val = (IPv4?)e.NewValue;
            IPv4Control c = (IPv4Control)d;

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            // 入力されたIPアドレスが無効の場合は何も設定しない
            if ((null == val) || (false == val.Value.IsValid))
            {
                c.SetCurrentValue(Oct0Property, string.Empty);
                c.SetCurrentValue(Oct1Property, string.Empty);
                c.SetCurrentValue(Oct2Property, string.Empty);
                c.SetCurrentValue(Oct3Property, string.Empty);
            }
            // 入力されたIPアドレスが有効の場合は入力された値を設定
            else
            {
                c.SetCurrentValue(Oct0Property, val.Value.Oct0.ToString());
                c.SetCurrentValue(Oct1Property, val.Value.Oct1.ToString());
                c.SetCurrentValue(Oct2Property, val.Value.Oct2.ToString());
                c.SetCurrentValue(Oct3Property, val.Value.Oct3.ToString());
            }

            c.m_internalChange = false;
        }

        #endregion

        /// <summary>
        /// IPアドレス変更時処理
        /// </summary>
        /// <param name="oldValue">前回の入力値(第2～第4オクテット)</param>
        /// <param name="newValue">今回の入力値(第2～第4オクテット)</param>
        /// <param name="min">オクテット最小値</param>
        /// <param name="max">オクテット最大値</param>
        /// <returns>オクテット</returns>
        private string CoercOct(string oldValue, string newValue, uint min, uint max)
        {
            string oct = newValue;

            // オクテット任意文字が0より小さいまたは9より大きい場合は前回値を設定
            if (oct.Any(n => (n < '0') || (n > '9')))
            {
                return oldValue;
            }

            // オクテットの長さが2より大きい場合
            if (oct.Length > 2)
            {
                // 第3文字まで取得
                oct = oct.Substring(0, 3);

                // 正数値でない場合は前回値を設定
                if (false == uint.TryParse(oct, out uint val))
                {
                    return oldValue;
                }

                // 第1オクテットが1より小さい場合は1を設定
                string errorVal = null;
                if (val < min)
                {
                    errorVal = oct;
                    oct = min.ToString();
                }
                // 第1オクテットが223より大きい場合は223を設定
                else if (val > max)
                {
                    errorVal = oct;
                    oct = max.ToString();
                }

                // 第1オクテットが1～223以外の場合はエラーポップを表示する
                // OKボタン押下で第1オクテットが1または223に設定される
                if (null != errorVal)
                {
                    switch (Message.ShowError(ErrorCode.Find("IPv4Control.Error1"), errorVal, min, max))
                    {
                        case Message.ErrorButton.OK:
                            // 何もしない
                            break;

                        default:
                            throw new NotSupportedException();
                    }
                }
            }

            return oct;
        }

        #region Oct0

        /// <summary>
        /// 0オクテットテキスト
        /// </summary>
        public string Oct0
        {
            get { return (string)GetValue(Oct0Property); }
            set { SetValue(Oct0Property, value); }
        }

        public static readonly DependencyProperty Oct0Property = DependencyProperty.Register(
            nameof(Oct0),
            typeof(string),
            typeof(IPv4Control),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                Oct0Changed,
                CoerceOct0,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// 第1オクテットチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>IPアドレス変更時処理呼出</returns>
        private static object CoerceOct0(DependencyObject d, object value)
        {
            string oct = (string)value;
            IPv4Control c = (IPv4Control)d;

            return c.CoercOct(c.Oct0, oct, 1, 223);
        }

        /// <summary>
        /// 第1オクテット変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void Oct0Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            string oct0 = (string)e.NewValue;
            IPv4Control c = (IPv4Control)d;

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            // IPアドレスのフォーマットが正しいかつ有効なIPアドレスである場合はIPアドレスを設定する
            IPv4? ip = null;
            if (IPv4.TryParse($"{oct0}.{c.Oct1}.{c.Oct2}.{c.Oct3}", out IPv4 outIP) && (outIP.IsValid))
            {
                ip = outIP;
            }

            d.SetCurrentValue(IPAddressProperty, ip);

            // 第1オクテットが3文字設定された場合はフォーカスを第2オクテットに移す
            if ((null != c.m_textOct0.Text)
                && (oct0.Length == 3)
                && (c.m_textOct0.SelectionStart == 3)
                && (c.m_textOct0.SelectionLength == 0))
            {
                c.m_textOct1.Focus();
                c.m_textOct1.CaretIndex = 0;
            }

            c.m_internalChange = false;
        }

        #endregion

        #region Oct1

        /// <summary>
        /// 1オクテットテキスト
        /// </summary>
        public string Oct1
        {
            get { return (string)GetValue(Oct1Property); }
            set { SetValue(Oct1Property, value); }
        }

        public static readonly DependencyProperty Oct1Property = DependencyProperty.Register(
            nameof(Oct1),
            typeof(string),
            typeof(IPv4Control),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                Oct1Changed,
                CoerceOct1,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// 第2オクテットチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>IPアドレス変更時処理呼出</returns>
        private static object CoerceOct1(DependencyObject d, object value)
        {
            string oct = (string)value;
            IPv4Control c = (IPv4Control)d;

            return c.CoercOct(c.Oct1, oct, 0, 255);
        }

        /// <summary>
        /// 第2オクテット変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void Oct1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            string oct1 = (string)e.NewValue;
            IPv4Control c = (IPv4Control)d;

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            // IPアドレスのフォーマットが正しいかつ有効なIPアドレスである場合はIPアドレスを設定する
            IPv4? ip = null;
            if (IPv4.TryParse($"{c.Oct0}.{oct1}.{c.Oct2}.{c.Oct3}", out IPv4 outIP) && (outIP.IsValid))
            {
                ip = outIP;
            }

            d.SetCurrentValue(IPAddressProperty, ip);

            // 第2オクテットが3文字設定された場合はフォーカスを第3オクテットに移す
            if ((null != c.m_textOct1.Text)
                && (oct1.Length == 3)
                && (c.m_textOct1.SelectionStart == 3)
                && (c.m_textOct1.SelectionLength == 0))
            {
                c.m_textOct2.Focus();
                c.m_textOct2.CaretIndex = 0;
            }

            c.m_internalChange = false;
        }

        #endregion

        #region Oct2

        /// <summary>
        /// 2オクテットテキスト
        /// </summary>
        public string Oct2
        {
            get { return (string)GetValue(Oct2Property); }
            set { SetValue(Oct2Property, value); }
        }

        public static readonly DependencyProperty Oct2Property = DependencyProperty.Register(
            nameof(Oct2),
            typeof(string),
            typeof(IPv4Control),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                Oct2Changed,
                CoerceOct2,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// 第3オクテットチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>IPアドレス変更時処理呼出</returns>
        private static object CoerceOct2(DependencyObject d, object value)
        {
            string oct = (string)value;
            IPv4Control c = (IPv4Control)d;

            return c.CoercOct(c.Oct2, oct, 0, 255);
        }

        /// <summary>
        /// 第3オクテット変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void Oct2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            string oct2 = (string)e.NewValue;
            IPv4Control c = (IPv4Control)d;

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            // IPアドレスのフォーマットが正しいかつ有効なIPアドレスである場合はIPアドレスを設定する
            IPv4? ip = null;
            if (IPv4.TryParse($"{c.Oct0}.{c.Oct1}.{oct2}.{c.Oct3}", out IPv4 outIP) && (outIP.IsValid))
            {
                ip = outIP;
            }

            d.SetCurrentValue(IPAddressProperty, ip);

            // 第3オクテットが3文字設定された場合はフォーカスを第4オクテットに移す
            if ((null != c.m_textOct2.Text)
                && (oct2.Length == 3)
                && (c.m_textOct2.SelectionStart == 3)
                && (c.m_textOct2.SelectionLength == 0))
            {
                c.m_textOct3.Focus();
                c.m_textOct3.CaretIndex = 0;
            }

            c.m_internalChange = false;
        }

        #endregion

        #region Oct3

        /// <summary>
        /// 3オクテットテキスト
        /// </summary>
        public string Oct3
        {
            get { return (string)GetValue(Oct3Property); }
            set { SetValue(Oct3Property, value); }
        }

        public static readonly DependencyProperty Oct3Property = DependencyProperty.Register(
            nameof(Oct3),
            typeof(string),
            typeof(IPv4Control),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                Oct3Changed,
                CoerceOct3,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// 第4オクテットチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>IPアドレス変更時処理呼出</returns>
        private static object CoerceOct3(DependencyObject d, object value)
        {
            string oct = (string)value;
            IPv4Control c = (IPv4Control)d;

            return c.CoercOct(c.Oct3, oct, 0, 255);
        }

        /// <summary>
        /// 第4オクテット変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void Oct3Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            string oct3 = (string)e.NewValue;
            IPv4Control c = (IPv4Control)d;

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            // IPアドレスのフォーマットが正しいかつ有効なIPアドレスである場合はIPアドレスを設定する
            IPv4? ip = null;
            if (IPv4.TryParse($"{c.Oct0}.{c.Oct1}.{c.Oct2}.{oct3}", out IPv4 outIP) && (outIP.IsValid))
            {
                ip = outIP;
            }

            d.SetCurrentValue(IPAddressProperty, ip);

            c.m_internalChange = false;
        }

        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public IPv4Control()
        {
            InitializeComponent();
        }

        private CustomTextBox m_textOct0;
        private CustomTextBox m_textOct1;
        private CustomTextBox m_textOct2;
        private CustomTextBox m_textOct3;

        /// <summary>
        /// OnApplyTemplateメソッド(カスタムコントロールにテンプレートが適用されたときに呼び出される)
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            m_textOct0 = (CustomTextBox)Template.FindName("TextOct0", this);
            m_textOct1 = (CustomTextBox)Template.FindName("TextOct1", this);
            m_textOct2 = (CustomTextBox)Template.FindName("TextOct2", this);
            m_textOct3 = (CustomTextBox)Template.FindName("TextOct3", this);

            InputMethod.SetIsInputMethodEnabled(m_textOct0, false);
            InputMethod.SetIsInputMethodEnabled(m_textOct1, false);
            InputMethod.SetIsInputMethodEnabled(m_textOct2, false);
            InputMethod.SetIsInputMethodEnabled(m_textOct3, false);
        }

        /// <summary>
        /// テキストフィールドの入力フィルター処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void FilterInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;

            ///選択領域差し替え
            var fullText = textBox.Text.Remove(textBox.SelectionStart, textBox.SelectionLength);
            fullText = fullText.Insert(textBox.SelectionStart, e.Text);

            byte val;

            bool failed = (false == byte.TryParse(fullText, NumberStyles.None, CultureInfo.InvariantCulture, out val));

            e.Handled = failed;
        }

        /// <summary>
        /// 第1オクテットの該当テキストフィールドのキー入力イベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void Oct0PreviewKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                if ((null != m_textOct0.Text)
                    && (m_textOct0.SelectionStart == m_textOct0.Text.Length)
                    && (m_textOct0.SelectionLength == 0))
                {
                    // 第2オクテットにフォーカスを移す
                    m_textOct1.Focus();
                    m_textOct1.CaretIndex = 0;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Space)
            {
                // スペースが押された場合はもFilterInput関数が呼ばれるようにする
                e.Handled = true;
            }
        }

        /// <summary>
        /// 第2オクテットの該当テキストフィールドのキー入力イベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void Oct1PreviewKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                if ((null != m_textOct1.Text)
                    && (m_textOct1.SelectionStart == m_textOct1.Text.Length)
                    && (m_textOct1.SelectionLength == 0))
                {
                    // 第3オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct2.Focus();
                    m_textOct2.CaretIndex = 0;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Left)
            {
                if ((null != m_textOct0.Text)
                    && (m_textOct1.SelectionStart == 0)
                    && (m_textOct1.SelectionLength == 0))
                {
                    // 第1オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct0.Focus();
                    m_textOct0.CaretIndex = m_textOct0.Text.Length;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Space)
            {
                // スペースが押された場合はもFilterInput関数が呼ばれるようにする
                e.Handled = true;
            }
        }

        /// <summary>
        /// 第3オクテットの該当テキストフィールドのキー入力イベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void Oct2PreviewKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                if ((null != m_textOct2.Text)
                    && (m_textOct2.SelectionStart == m_textOct2.Text.Length)
                    && (m_textOct2.SelectionLength == 0))
                {
                    // 第4オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct3.Focus();
                    m_textOct3.CaretIndex = 0;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Left)
            {
                if ((null != m_textOct1.Text)
                    && (m_textOct2.SelectionStart == 0)
                    && (m_textOct2.SelectionLength == 0))
                {
                    // 第2オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct1.Focus();
                    m_textOct1.CaretIndex = m_textOct1.Text.Length;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Space)
            {
                // スペースが押された場合はもFilterInput関数が呼ばれるようにする
                e.Handled = true;
            }
        }

        /// <summary>
        /// 第4オクテットの該当テキストフィールドのキー入力イベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void Oct3PreviewKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                if ((null != m_textOct2.Text)
                    && (m_textOct3.SelectionStart == 0)
                    && (m_textOct3.SelectionLength == 0))
                {
                    // 第3オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct2.Focus();
                    m_textOct2.CaretIndex = m_textOct2.Text.Length;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Space)
            {
                // スペースが押された場合はもFilterInput関数が呼ばれるようにする
                e.Handled = true;
            }
        }

        /// <summary>
        /// 第1オクテットの該当テキストフィールドのフォカス失う時のイベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void TextOct0_LostFocus(object sender, RoutedEventArgs e)
        {
            if (false == uint.TryParse(Oct0, out uint val))
            {
                return;
            }

            int min = 1;
            int max = 223;

            // 第1オクテットが1より小さい場合は1を設定
            string errorVal = null;
            if (val < min)
            {
                errorVal = Oct0;
                SetCurrentValue(Oct0Property, min.ToString());
            }
            // 第1オクテットが223より大きい場合は223を設定
            else if (val > max)
            {
                errorVal = Oct0;
                SetCurrentValue(Oct0Property, max.ToString());
            }

            // 第1オクテットが1～223以外の場合はエラーポップを表示する
            // OKボタン押下で第1オクテットが1または223に設定される
            if (null != errorVal)
            {
                switch (Message.ShowError(ErrorCode.Find("IPv4Control.Error1"), errorVal, min, max))
                {
                    case Message.ErrorButton.OK:
                        // 何もしない
                        break;

                    default:
                        throw new NotSupportedException();
                }
            }
        }
    }
}
