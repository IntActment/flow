﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using FlowFramework.Model;
using FlowFramework.Utils;

namespace FlowFramework.Controls
{
    /// <summary>
    /// Interaction logic for MACAddressControl.xaml
    /// </summary>
    public partial class MACAddressControl : UserControl
    {
        /// <summary>
        /// 変換メソッドであるCoerceの呼び出しが無限ループに入らないようにフラグ
        /// </summary>
        private bool m_internalChange = false;

        #region IsValid

        /// <summary>
        /// 有効状態フラグ
        /// </summary>
        public bool IsValid
        {
            get { return (bool)GetValue(IsValidProperty); }
            set { SetValue(IsValidProperty, value); }
        }

        private static readonly DependencyProperty IsValidProperty = DependencyProperty.Register(
            nameof(IsValid),
            typeof(bool),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.AffectsRender, null));

        #endregion

        #region ValidateOct0Command

        public IValidateCommand<byte> ValidateOct0Command
        {
            get { return (IValidateCommand<byte>)GetValue(ValidateOct0CommandProperty); }
            set { SetValue(ValidateOct0CommandProperty, value); }
        }

        public static readonly DependencyProperty ValidateOct0CommandProperty = DependencyProperty.Register(
            nameof(ValidateOct0Command),
            typeof(IValidateCommand<byte>),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(default(IValidateCommand<byte>)));

        #endregion

        #region ValidateOct1Command

        public IValidateCommand<byte> ValidateOct1Command
        {
            get { return (IValidateCommand<byte>)GetValue(ValidateOct1CommandProperty); }
            set { SetValue(ValidateOct1CommandProperty, value); }
        }

        public static readonly DependencyProperty ValidateOct1CommandProperty = DependencyProperty.Register(
            nameof(ValidateOct1Command),
            typeof(IValidateCommand<byte>),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(default(IValidateCommand<byte>)));

        #endregion

        #region ValidateOct2Command

        public IValidateCommand<byte> ValidateOct2Command
        {
            get { return (IValidateCommand<byte>)GetValue(ValidateOct2CommandProperty); }
            set { SetValue(ValidateOct2CommandProperty, value); }
        }

        public static readonly DependencyProperty ValidateOct2CommandProperty = DependencyProperty.Register(
            nameof(ValidateOct2Command),
            typeof(IValidateCommand<byte>),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(default(IValidateCommand<byte>)));

        #endregion

        #region ValidateOct3Command

        public IValidateCommand<byte> ValidateOct3Command
        {
            get { return (IValidateCommand<byte>)GetValue(ValidateOct3CommandProperty); }
            set { SetValue(ValidateOct3CommandProperty, value); }
        }

        public static readonly DependencyProperty ValidateOct3CommandProperty = DependencyProperty.Register(
            nameof(ValidateOct3Command),
            typeof(IValidateCommand<byte>),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(default(IValidateCommand<byte>)));

        #endregion

        #region ValidateOct4Command

        public IValidateCommand<byte> ValidateOct4Command
        {
            get { return (IValidateCommand<byte>)GetValue(ValidateOct4CommandProperty); }
            set { SetValue(ValidateOct4CommandProperty, value); }
        }

        public static readonly DependencyProperty ValidateOct4CommandProperty = DependencyProperty.Register(
            nameof(ValidateOct4Command),
            typeof(IValidateCommand<byte>),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(default(IValidateCommand<byte>)));

        #endregion

        #region ValidateOct5Command

        public IValidateCommand<byte> ValidateOct5Command
        {
            get { return (IValidateCommand<byte>)GetValue(ValidateOct5CommandProperty); }
            set { SetValue(ValidateOct5CommandProperty, value); }
        }

        public static readonly DependencyProperty ValidateOct5CommandProperty = DependencyProperty.Register(
            nameof(ValidateOct5Command),
            typeof(IValidateCommand<byte>),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(default(IValidateCommand<byte>)));

        #endregion

        #region MACAddress

        /// <summary>
        /// MACアドレス（無効なアドレスはnull）
        /// </summary>
        public MACAddress? Address
        {
            get { return (MACAddress?)GetValue(MACAddressProperty); }
            set { SetValue(MACAddressProperty, value); }
        }

        public static readonly DependencyProperty MACAddressProperty = DependencyProperty.Register(
            nameof(Address),
            typeof(MACAddress?),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(
                (MACAddress?)null,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                MACAddressChanged,
                CoerceMACAddress,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// MACアドレスチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>MACアドレス</returns>
        private static object CoerceMACAddress(DependencyObject d, object value)
        {
            MACAddress? val = (MACAddress?)value;

            if ((null != val) && (false == val.Value.IsValid))
            {
                return null;
            }

            return val;
        }

        /// <summary>
        /// MACアドレス変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void MACAddressChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MACAddress? val = (MACAddress?)e.NewValue;
            MACAddressControl c = (MACAddressControl)d;

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            // 入力されたMACアドレスが無効の場合は何も設定しない
            if ((null == val) || (false == val.Value.IsValid))
            {
                c.SetCurrentValue(Oct0Property, string.Empty);
                c.SetCurrentValue(Oct1Property, string.Empty);
                c.SetCurrentValue(Oct2Property, string.Empty);
                c.SetCurrentValue(Oct3Property, string.Empty);
                c.SetCurrentValue(Oct4Property, string.Empty);
                c.SetCurrentValue(Oct5Property, string.Empty);
            }
            // 入力されたMACアドレスが有効の場合は入力された値を設定
            else
            {
                c.SetCurrentValue(Oct0Property, val.Value.Oct0.ToString("X2"));
                c.SetCurrentValue(Oct1Property, val.Value.Oct1.ToString("X2"));
                c.SetCurrentValue(Oct2Property, val.Value.Oct2.ToString("X2"));
                c.SetCurrentValue(Oct3Property, val.Value.Oct3.ToString("X2"));
                c.SetCurrentValue(Oct4Property, val.Value.Oct4.ToString("X2"));
                c.SetCurrentValue(Oct5Property, val.Value.Oct5.ToString("X2"));
            }

            c.m_internalChange = false;
        }

        #endregion

        /// <summary>
        /// MACアドレス変更時処理
        /// </summary>
        /// <param name="oldValue">前回の入力値(第2～第4オクテット)</param>
        /// <param name="newValue">今回の入力値(第2～第4オクテット)</param>
        /// <returns>オクテット</returns>
        private string CoercOct(string oldValue, string newValue)
        {
            string oct = newValue.ToUpper();

            // オクテット任意文字がHEX数値にならない場合は前回値を設定
            if (false == oct.All(n => ((n >= '0') && (n <= '9')) || ((n >= 'A') && (n <= 'F'))))
            {
                return oldValue;
            }

            return oct;
        }

        #region Oct0

        /// <summary>
        /// 0オクテットテキスト
        /// </summary>
        public string Oct0
        {
            get { return (string)GetValue(Oct0Property); }
            set { SetValue(Oct0Property, value); }
        }

        public static readonly DependencyProperty Oct0Property = DependencyProperty.Register(
            nameof(Oct0),
            typeof(string),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                Oct0Changed,
                CoerceOct0,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// 第1オクテットチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>MACアドレス変更時処理呼出</returns>
        private static object CoerceOct0(DependencyObject d, object value)
        {
            string oct = (string)value;
            MACAddressControl c = (MACAddressControl)d;

            return c.CoercOct(c.Oct0, oct);
        }

        /// <summary>
        /// 第1オクテット変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void Oct0Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            string oct0 = (string)e.NewValue;
            MACAddressControl c = (MACAddressControl)d;

            string oct = oct0.Length == 1
                ? $"0{oct0}"
                : oct0;

            // MACアドレスのフォーマットが正しいかつ有効なMACアドレスである場合はMACアドレスを設定する
            MACAddress? mac = null;

            if (true == string.IsNullOrEmpty(oct))
            {
                c.m_textOct0.ToolTip = null;
                c.m_textOct0.SetCurrentValue(CustomTextBox.IsValidProperty, true);
            }
            else
            {
                string error = null;
                if (true == byte.TryParse(oct, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
                {
                    error = c.ValidateOct0Command?.GetError(val);
                }
                else
                {
                    Debug.Assert(false);
                }

                c.m_textOct0.ToolTip = error;
                c.m_textOct0.SetCurrentValue(CustomTextBox.IsValidProperty, error == null);

                if ((null == error) && Model.MACAddress.TryParse($"{oct}:{c.Oct1}:{c.Oct2}:{c.Oct3}:{c.Oct4}:{c.Oct5}", out MACAddress outMAC) && (outMAC.IsValid))
            {
                mac = outMAC;
            }
            }

            c.SetCurrentValue(IsValidProperty, null != mac);

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            d.SetCurrentValue(MACAddressProperty, mac);

            // 第1オクテットが3文字設定された場合はフォーカスを第2オクテットに移す
            if ((null != c.m_textOct0.Text)
                && (oct0.Length == 2)
                && (c.m_textOct0.SelectionStart == 2)
                && (c.m_textOct0.SelectionLength == 0))
            {
                c.m_textOct1.Focus();
                c.m_textOct1.CaretIndex = 0;
            }

            c.m_internalChange = false;
        }

        #endregion

        #region Oct1

        /// <summary>
        /// 1オクテットテキスト
        /// </summary>
        public string Oct1
        {
            get { return (string)GetValue(Oct1Property); }
            set { SetValue(Oct1Property, value); }
        }

        public static readonly DependencyProperty Oct1Property = DependencyProperty.Register(
            nameof(Oct1),
            typeof(string),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                Oct1Changed,
                CoerceOct1,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// 第2オクテットチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>MACアドレス変更時処理呼出</returns>
        private static object CoerceOct1(DependencyObject d, object value)
        {
            string oct = (string)value;
            MACAddressControl c = (MACAddressControl)d;

            return c.CoercOct(c.Oct1, oct);
        }

        /// <summary>
        /// 第2オクテット変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void Oct1Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            string oct1 = (string)e.NewValue;
            MACAddressControl c = (MACAddressControl)d;

            string oct = oct1.Length == 1
                ? $"0{oct1}"
                : oct1;

            // MACアドレスのフォーマットが正しいかつ有効なMACアドレスである場合はMACアドレスを設定する
            MACAddress? mac = null;

            if (true == string.IsNullOrEmpty(oct))
            {
                c.m_textOct1.ToolTip = null;
                c.m_textOct1.SetCurrentValue(CustomTextBox.IsValidProperty, true);
            }
            else
            {
                string error = null;
                if (true == byte.TryParse(oct, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
                {
                    error = c.ValidateOct1Command?.GetError(val);
                }
                else
                {
                    Debug.Assert(false);
                }

                c.m_textOct1.ToolTip = error;
                c.m_textOct1.SetCurrentValue(CustomTextBox.IsValidProperty, error == null);

                if ((null == error) && Model.MACAddress.TryParse($"{c.Oct0}:{oct}:{c.Oct2}:{c.Oct3}:{c.Oct4}:{c.Oct5}", out MACAddress outMAC) && (outMAC.IsValid))
            {
                mac = outMAC;
            }
            }

            c.SetCurrentValue(IsValidProperty, null != mac);

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            d.SetCurrentValue(MACAddressProperty, mac);

            // 第2オクテットが3文字設定された場合はフォーカスを第3オクテットに移す
            if ((null != c.m_textOct1.Text)
                && (oct1.Length == 2)
                && (c.m_textOct1.SelectionStart == 2)
                && (c.m_textOct1.SelectionLength == 0))
            {
                c.m_textOct2.Focus();
                c.m_textOct2.CaretIndex = 0;
            }

            c.m_internalChange = false;
        }

        #endregion

        #region Oct2

        /// <summary>
        /// 2オクテットテキスト
        /// </summary>
        public string Oct2
        {
            get { return (string)GetValue(Oct2Property); }
            set { SetValue(Oct2Property, value); }
        }

        public static readonly DependencyProperty Oct2Property = DependencyProperty.Register(
            nameof(Oct2),
            typeof(string),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                Oct2Changed,
                CoerceOct2,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// 第3オクテットチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>MACアドレス変更時処理呼出</returns>
        private static object CoerceOct2(DependencyObject d, object value)
        {
            string oct = (string)value;
            MACAddressControl c = (MACAddressControl)d;

            return c.CoercOct(c.Oct2, oct);
        }

        /// <summary>
        /// 第3オクテット変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void Oct2Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            string oct2 = (string)e.NewValue;
            MACAddressControl c = (MACAddressControl)d;

            string oct = oct2.Length == 1
                ? $"0{oct2}"
                : oct2;

            // MACアドレスのフォーマットが正しいかつ有効なMACアドレスである場合はMACアドレスを設定する
            MACAddress? mac = null;

            if (true == string.IsNullOrEmpty(oct))
            {
                c.m_textOct2.ToolTip = null;
                c.m_textOct2.SetCurrentValue(CustomTextBox.IsValidProperty, true);
            }
            else
            {
                string error = null;
                if (true == byte.TryParse(oct, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
                {
                    error = c.ValidateOct2Command?.GetError(val);
                }
                else
                {
                    Debug.Assert(false);
                }

                c.m_textOct2.ToolTip = error;
                c.m_textOct2.SetCurrentValue(CustomTextBox.IsValidProperty, error == null);

                if ((null == error) && Model.MACAddress.TryParse($"{c.Oct0}:{c.Oct1}:{oct}:{c.Oct3}:{c.Oct4}:{c.Oct5}", out MACAddress outMAC) && (outMAC.IsValid))
            {
                mac = outMAC;
            }
            }

            c.SetCurrentValue(IsValidProperty, null != mac);

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            d.SetCurrentValue(MACAddressProperty, mac);

            // 第3オクテットが3文字設定された場合はフォーカスを第4オクテットに移す
            if ((null != c.m_textOct2.Text)
                && (oct2.Length == 2)
                && (c.m_textOct2.SelectionStart == 2)
                && (c.m_textOct2.SelectionLength == 0))
            {
                c.m_textOct3.Focus();
                c.m_textOct3.CaretIndex = 0;
            }

            c.m_internalChange = false;
        }

        #endregion

        #region Oct3

        /// <summary>
        /// 3オクテットテキスト
        /// </summary>
        public string Oct3
        {
            get { return (string)GetValue(Oct3Property); }
            set { SetValue(Oct3Property, value); }
        }

        public static readonly DependencyProperty Oct3Property = DependencyProperty.Register(
            nameof(Oct3),
            typeof(string),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                Oct3Changed,
                CoerceOct3,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// 第4オクテットチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>MACアドレス変更時処理呼出</returns>
        private static object CoerceOct3(DependencyObject d, object value)
        {
            string oct = (string)value;
            MACAddressControl c = (MACAddressControl)d;

            return c.CoercOct(c.Oct3, oct);
        }

        /// <summary>
        /// 第4オクテット変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void Oct3Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            string oct3 = (string)e.NewValue;
            MACAddressControl c = (MACAddressControl)d;

            string oct = oct3.Length == 1
                ? $"0{oct3}"
                : oct3;

            // MACアドレスのフォーマットが正しいかつ有効なMACアドレスである場合はMACアドレスを設定する
            MACAddress? mac = null;

            if (true == string.IsNullOrEmpty(oct))
            {
                c.m_textOct3.ToolTip = null;
                c.m_textOct3.SetCurrentValue(CustomTextBox.IsValidProperty, true);
            }
            else
            {
                string error = null;
                if (true == byte.TryParse(oct, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
                {
                    error = c.ValidateOct3Command?.GetError(val);
                }
                else
                {
                    Debug.Assert(false);
                }

                c.m_textOct3.ToolTip = error;
                c.m_textOct3.SetCurrentValue(CustomTextBox.IsValidProperty, error == null);

                if ((null == error) && Model.MACAddress.TryParse($"{c.Oct0}:{c.Oct1}:{c.Oct2}:{oct}:{c.Oct4}:{c.Oct5}", out MACAddress outMAC) && (outMAC.IsValid))
            {
                mac = outMAC;
            }
            }

            c.SetCurrentValue(IsValidProperty, null != mac);

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            d.SetCurrentValue(MACAddressProperty, mac);

            // 第4オクテットが2文字設定された場合はフォーカスを第5オクテットに移す
            if ((null != c.m_textOct2.Text)
                && (oct3.Length == 2)
                && (c.m_textOct3.SelectionStart == 2)
                && (c.m_textOct3.SelectionLength == 0))
            {
                c.m_textOct4.Focus();
                c.m_textOct4.CaretIndex = 0;
            }

            c.m_internalChange = false;
        }

        #endregion

        #region Oct4

        /// <summary>
        /// 4オクテットテキスト
        /// </summary>
        public string Oct4
        {
            get { return (string)GetValue(Oct4Property); }
            set { SetValue(Oct4Property, value); }
        }

        public static readonly DependencyProperty Oct4Property = DependencyProperty.Register(
            nameof(Oct4),
            typeof(string),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                Oct4Changed,
                CoerceOct4,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// 第5オクテットチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>MACアドレス変更時処理呼出</returns>
        private static object CoerceOct4(DependencyObject d, object value)
        {
            string oct = (string)value;
            MACAddressControl c = (MACAddressControl)d;

            return c.CoercOct(c.Oct4, oct);
        }

        /// <summary>
        /// 第5オクテット変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void Oct4Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            string oct4 = (string)e.NewValue;
            MACAddressControl c = (MACAddressControl)d;

            string oct = oct4.Length == 1
                ? $"0{oct4}"
                : oct4;

            // MACアドレスのフォーマットが正しいかつ有効なMACアドレスである場合はMACアドレスを設定する
            MACAddress? mac = null;

            if (true == string.IsNullOrEmpty(oct))
            {
                c.m_textOct4.ToolTip = null;
                c.m_textOct4.SetCurrentValue(CustomTextBox.IsValidProperty, true);
            }
            else
            {
                string error = null;
                if (true == byte.TryParse(oct, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
                {
                    error = c.ValidateOct4Command?.GetError(val);
                }
                else
                {
                    Debug.Assert(false);
                }

                c.m_textOct4.ToolTip = error;
                c.m_textOct4.SetCurrentValue(CustomTextBox.IsValidProperty, error == null);

                if ((null == error) && Model.MACAddress.TryParse($"{c.Oct0}:{c.Oct1}:{c.Oct2}:{c.Oct3}:{oct}:{c.Oct5}", out MACAddress outMAC) && (outMAC.IsValid))
            {
                mac = outMAC;
            }
            }

            c.SetCurrentValue(IsValidProperty, null != mac);

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            d.SetCurrentValue(MACAddressProperty, mac);

            // 第5オクテットが2文字設定された場合はフォーカスを第5オクテットに移す
            if ((null != c.m_textOct2.Text)
                && (oct4.Length == 2)
                && (c.m_textOct4.SelectionStart == 2)
                && (c.m_textOct4.SelectionLength == 0))
            {
                c.m_textOct5.Focus();
                c.m_textOct5.CaretIndex = 0;
            }

            c.m_internalChange = false;
        }

        #endregion

        #region Oct5

        /// <summary>
        /// 5オクテットテキスト
        /// </summary>
        public string Oct5
        {
            get { return (string)GetValue(Oct5Property); }
            set { SetValue(Oct5Property, value); }
        }

        public static readonly DependencyProperty Oct5Property = DependencyProperty.Register(
            nameof(Oct5),
            typeof(string),
            typeof(MACAddressControl),
            new FrameworkPropertyMetadata(
                string.Empty,
                FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
                Oct5Changed,
                CoerceOct5,
                false,
                UpdateSourceTrigger.PropertyChanged));

        /// <summary>
        /// 第6オクテットチェック処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="value">オブジェクト(未使用)</param>
        /// <returns>MACアドレス変更時処理呼出</returns>
        private static object CoerceOct5(DependencyObject d, object value)
        {
            string oct = (string)value;
            MACAddressControl c = (MACAddressControl)d;

            return c.CoercOct(c.Oct5, oct);
        }

        /// <summary>
        /// 第6オクテット変更時処理
        /// </summary>
        /// <param name="d">関連オブジェクト(未使用)</param>
        /// <param name="e">イベントデータ(未使用)</param>
        private static void Oct5Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            string oct5 = (string)e.NewValue;
            MACAddressControl c = (MACAddressControl)d;

            string oct = oct5.Length == 1
                ? $"0{oct5}"
                : oct5;

            // MACアドレスのフォーマットが正しいかつ有効なMACアドレスである場合はMACアドレスを設定する
            MACAddress? mac = null;

            if (true == string.IsNullOrEmpty(oct))
            {
                c.m_textOct5.ToolTip = null;
                c.m_textOct5.SetCurrentValue(CustomTextBox.IsValidProperty, true);
            }
            else
            {
                string error = null;
                if (true == byte.TryParse(oct, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
                {
                    error = c.ValidateOct5Command?.GetError(val);
                }
                else
                {
                    Debug.Assert(false);
                }

                c.m_textOct5.ToolTip = error;
                c.m_textOct5.SetCurrentValue(CustomTextBox.IsValidProperty, error == null);

                if ((null == error) && Model.MACAddress.TryParse($"{c.Oct0}:{c.Oct1}:{c.Oct2}:{c.Oct3}:{c.Oct4}:{oct}", out MACAddress outMAC) && (outMAC.IsValid))
            {
                mac = outMAC;
            }
            }

            c.SetCurrentValue(IsValidProperty, null != mac);

            if (true == c.m_internalChange)
            {
                return;
            }

            c.m_internalChange = true;

            d.SetCurrentValue(MACAddressProperty, mac);

            c.m_internalChange = false;
        }

        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MACAddressControl()
        {
            InitializeComponent();
        }

        private CustomTextBox m_textOct0;
        private CustomTextBox m_textOct1;
        private CustomTextBox m_textOct2;
        private CustomTextBox m_textOct3;
        private CustomTextBox m_textOct4;
        private CustomTextBox m_textOct5;

        /// <summary>
        /// OnApplyTemplateメソッド(カスタムコントロールにテンプレートが適用されたときに呼び出される)
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            m_textOct0 = (CustomTextBox)Template.FindName("TextOct0", this);
            m_textOct1 = (CustomTextBox)Template.FindName("TextOct1", this);
            m_textOct2 = (CustomTextBox)Template.FindName("TextOct2", this);
            m_textOct3 = (CustomTextBox)Template.FindName("TextOct3", this);
            m_textOct4 = (CustomTextBox)Template.FindName("TextOct4", this);
            m_textOct5 = (CustomTextBox)Template.FindName("TextOct5", this);

            InputMethod.SetIsInputMethodEnabled(m_textOct0, false);
            InputMethod.SetIsInputMethodEnabled(m_textOct1, false);
            InputMethod.SetIsInputMethodEnabled(m_textOct2, false);
            InputMethod.SetIsInputMethodEnabled(m_textOct3, false);
            InputMethod.SetIsInputMethodEnabled(m_textOct4, false);
            InputMethod.SetIsInputMethodEnabled(m_textOct5, false);
        }

        /// <summary>
        /// テキストフィールドの入力フィルター処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void FilterInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;

            ///選択領域差し替え
            var fullText = textBox.Text.Remove(textBox.SelectionStart, textBox.SelectionLength);
            fullText = fullText.Insert(textBox.SelectionStart, e.Text);

            bool failed = (false == byte.TryParse(fullText, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out _));

            e.Handled = failed;
        }

        /// <summary>
        /// 第1オクテットの該当テキストフィールドのキー入力イベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void Oct0PreviewKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                if ((null != m_textOct0.Text)
                    && (m_textOct0.SelectionStart == m_textOct0.Text.Length)
                    && (m_textOct0.SelectionLength == 0))
                {
                    // 第2オクテットにフォーカスを移す
                    m_textOct1.Focus();
                    m_textOct1.CaretIndex = 0;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Space)
            {
                // スペースが押された場合はもFilterInput関数が呼ばれるようにする
                e.Handled = true;
            }
        }

        /// <summary>
        /// 第2オクテットの該当テキストフィールドのキー入力イベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void Oct1PreviewKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                if ((null != m_textOct1.Text)
                    && (m_textOct1.SelectionStart == m_textOct1.Text.Length)
                    && (m_textOct1.SelectionLength == 0))
                {
                    // 第3オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct2.Focus();
                    m_textOct2.CaretIndex = 0;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Left)
            {
                if ((null != m_textOct0.Text)
                    && (m_textOct1.SelectionStart == 0)
                    && (m_textOct1.SelectionLength == 0))
                {
                    // 第1オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct0.Focus();
                    m_textOct0.CaretIndex = m_textOct0.Text.Length;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Space)
            {
                // スペースが押された場合はもFilterInput関数が呼ばれるようにする
                e.Handled = true;
            }
        }

        /// <summary>
        /// 第3オクテットの該当テキストフィールドのキー入力イベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void Oct2PreviewKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                if ((null != m_textOct2.Text)
                    && (m_textOct2.SelectionStart == m_textOct2.Text.Length)
                    && (m_textOct2.SelectionLength == 0))
                {
                    // 第4オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct3.Focus();
                    m_textOct3.CaretIndex = 0;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Left)
            {
                if ((null != m_textOct1.Text)
                    && (m_textOct2.SelectionStart == 0)
                    && (m_textOct2.SelectionLength == 0))
                {
                    // 第2オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct1.Focus();
                    m_textOct1.CaretIndex = m_textOct1.Text.Length;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Space)
            {
                // スペースが押された場合はもFilterInput関数が呼ばれるようにする
                e.Handled = true;
            }
        }

        /// <summary>
        /// 第4オクテットの該当テキストフィールドのキー入力イベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void Oct3PreviewKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                if ((null != m_textOct3.Text)
                    && (m_textOct3.SelectionStart == m_textOct3.Text.Length)
                    && (m_textOct3.SelectionLength == 0))
                {
                    // 第5オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct4.Focus();
                    m_textOct4.CaretIndex = 0;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Left)
            {
                if ((null != m_textOct2.Text)
                    && (m_textOct3.SelectionStart == 0)
                    && (m_textOct3.SelectionLength == 0))
                {
                    // 第3オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct2.Focus();
                    m_textOct2.CaretIndex = m_textOct2.Text.Length;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Space)
            {
                // スペースが押された場合はもFilterInput関数が呼ばれるようにする
                e.Handled = true;
            }
        }

        /// <summary>
        /// 第5オクテットの該当テキストフィールドのキー入力イベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void Oct4PreviewKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Right)
            {
                if ((null != m_textOct4.Text)
                    && (m_textOct4.SelectionStart == m_textOct4.Text.Length)
                    && (m_textOct4.SelectionLength == 0))
                {
                    // 第5オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct5.Focus();
                    m_textOct5.CaretIndex = 0;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Left)
            {
                if ((null != m_textOct3.Text)
                    && (m_textOct4.SelectionStart == 0)
                    && (m_textOct4.SelectionLength == 0))
                {
                    // 第3オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct3.Focus();
                    m_textOct3.CaretIndex = m_textOct3.Text.Length;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Space)
            {
                // スペースが押された場合はもFilterInput関数が呼ばれるようにする
                e.Handled = true;
            }
        }

        /// <summary>
        /// 第6オクテットの該当テキストフィールドのキー入力イベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void Oct5PreviewKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                if ((null != m_textOct4.Text)
                    && (m_textOct5.SelectionStart == 0)
                    && (m_textOct5.SelectionLength == 0))
                {
                    // 第6オクテットの該当テキストフィールドにフォカスを移す
                    m_textOct4.Focus();
                    m_textOct4.CaretIndex = m_textOct4.Text.Length;

                    e.Handled = true;
                }
            }
            else if (e.Key == Key.Space)
            {
                // スペースが押された場合はもFilterInput関数が呼ばれるようにする
                e.Handled = true;
            }
        }

        /// <summary>
        /// 第1オクテットの該当テキストフィールドのフォカス失う時のイベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void TextOct0_LostFocus(object sender, RoutedEventArgs e)
        {
            if (false == byte.TryParse(Oct0, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
            {
                return;
            }

            SetCurrentValue(Oct0Property, $"{val:X2}");
        }

        /// <summary>
        /// 第2オクテットの該当テキストフィールドのフォカス失う時のイベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void TextOct1_LostFocus(object sender, RoutedEventArgs e)
        {
            if (false == byte.TryParse(Oct1, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
            {
                return;
            }

            SetCurrentValue(Oct1Property, $"{val:X2}");
        }

        /// <summary>
        /// 第3オクテットの該当テキストフィールドのフォカス失う時のイベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void TextOct2_LostFocus(object sender, RoutedEventArgs e)
        {
            if (false == byte.TryParse(Oct2, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
            {
                return;
            }

            SetCurrentValue(Oct2Property, $"{val:X2}");
        }

        /// <summary>
        /// 第4オクテットの該当テキストフィールドのフォカス失う時のイベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void TextOct3_LostFocus(object sender, RoutedEventArgs e)
        {
            if (false == byte.TryParse(Oct3, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
            {
                return;
            }

            SetCurrentValue(Oct3Property, $"{val:X2}");
        }

        /// <summary>
        /// 第5オクテットの該当テキストフィールドのフォカス失う時のイベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void TextOct4_LostFocus(object sender, RoutedEventArgs e)
        {
            if (false == byte.TryParse(Oct4, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
            {
                return;
            }

            SetCurrentValue(Oct4Property, $"{val:X2}");
        }

        /// <summary>
        /// 第6オクテットの該当テキストフィールドのフォカス失う時のイベントハンドラ処理
        /// </summary>
        /// <param name="sender">テキストフィールド</param>
        /// <param name="e">イベントパラメータ</param>
        private void TextOct5_LostFocus(object sender, RoutedEventArgs e)
        {
            if (false == byte.TryParse(Oct5, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte val))
            {
                return;
            }

            SetCurrentValue(Oct5Property, $"{val:X2}");
        }
    }
}
