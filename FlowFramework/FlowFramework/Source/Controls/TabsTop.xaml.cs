﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlowFramework.Controls
{
    /// <summary>
    /// Interaction logic for TabsTop.xaml
    /// </summary>
    public partial class TabsTop : ListView
    {
        public Brush RootBorder
        {
            get { return (Brush)GetValue(RootBorderProperty); }
            set { SetValue(RootBorderProperty, value); }
        }

        public static readonly DependencyProperty RootBorderProperty = DependencyProperty.Register(
            nameof(RootBorder),
            typeof(Brush),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(Brushes.Red, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public double RootBorderThickness
        {
            get { return (double)GetValue(RootBorderThicknessProperty); }
            set { SetValue(RootBorderThicknessProperty, value); }
        }

        public static readonly DependencyProperty RootBorderThicknessProperty = DependencyProperty.Register(
            nameof(RootBorderThickness),
            typeof(double),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(3.0, FrameworkPropertyMetadataOptions.AffectsRender, UpdateRootBorderThickness));

        private static void UpdateRootBorderThickness(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            double value = (double)e.NewValue;
            TabsTop tabsTop = d as TabsTop;

            tabsTop.SetValue(TabMarginPropertyKey, new Thickness(0, 0, 0, -value));
            tabsTop.SetValue(TabSizePropertyKey, tabsTop.ActualHeight);
        }

        public Thickness TabMargin
        {
            get { return (Thickness)GetValue(TabMarginProperty); }
            protected set { SetValue(TabMarginPropertyKey, value); }
        }

        private static readonly DependencyPropertyKey TabMarginPropertyKey = DependencyProperty.RegisterReadOnly(
            nameof(TabMargin),
            typeof(Thickness),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(default(Thickness), FrameworkPropertyMetadataOptions.AffectsArrange));

        public static readonly DependencyProperty TabMarginProperty = TabMarginPropertyKey.DependencyProperty;

        public double TabSize
        {
            get { return (double)GetValue(TabSizeProperty); }
            protected set { SetValue(TabSizeProperty, value); }
        }

        private static readonly DependencyPropertyKey TabSizePropertyKey = DependencyProperty.RegisterReadOnly(
            nameof(TabSize),
            typeof(double),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(32.0, FrameworkPropertyMetadataOptions.AffectsArrange));

        public static readonly DependencyProperty TabSizeProperty = TabSizePropertyKey.DependencyProperty;

        public double TabBorderThickness
        {
            get { return (double)GetValue(TabBorderThicknessProperty); }
            set { SetValue(TabBorderThicknessProperty, value); }
        }

        private static readonly DependencyProperty TabBorderThicknessProperty = DependencyProperty.Register(
            nameof(TabBorderThickness),
            typeof(double),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(default(double), FrameworkPropertyMetadataOptions.AffectsRender, null));

        public Brush TabBorderBrushNormal
        {
            get { return (Brush)GetValue(TabBorderBrushNormalProperty); }
            set { SetValue(TabBorderBrushNormalProperty, value); }
        }

        private static readonly DependencyProperty TabBorderBrushNormalProperty = DependencyProperty.Register(
            nameof(TabBorderBrushNormal),
            typeof(Brush),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(Brushes.LightGray, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public Brush TabBorderBrushSelected
        {
            get { return (Brush)GetValue(TabBorderBrushSelectedProperty); }
            set { SetValue(TabBorderBrushSelectedProperty, value); }
        }

        private static readonly DependencyProperty TabBorderBrushSelectedProperty = DependencyProperty.Register(
            nameof(TabBorderBrushSelected),
            typeof(Brush),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public Brush TabBorderBrushHover
        {
            get { return (Brush)GetValue(TabBorderBrushHoverProperty); }
            set { SetValue(TabBorderBrushHoverProperty, value); }
        }

        private static readonly DependencyProperty TabBorderBrushHoverProperty = DependencyProperty.Register(
            nameof(TabBorderBrushHover),
            typeof(Brush),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));
        
        public Brush TabBackgroundBrushNormal
        {
            get { return (Brush)GetValue(TabBackgroundBrushNormalProperty); }
            set { SetValue(TabBackgroundBrushNormalProperty, value); }
        }

        private static readonly DependencyProperty TabBackgroundBrushNormalProperty = DependencyProperty.Register(
            nameof(TabBackgroundBrushNormal),
            typeof(Brush),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public Brush TabBackgroundBrushSelected
        {
            get { return (Brush)GetValue(TabBackgroundBrushSelectedProperty); }
            set { SetValue(TabBackgroundBrushSelectedProperty, value); }
        }

        private static readonly DependencyProperty TabBackgroundBrushSelectedProperty = DependencyProperty.Register(
            nameof(TabBackgroundBrushSelected),
            typeof(Brush),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(Brushes.OrangeRed, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public Brush TabBackgroundBrushHover
        {
            get { return (Brush)GetValue(TabBackgroundBrushHoverProperty); }
            set { SetValue(TabBackgroundBrushHoverProperty, value); }
        }

        private static readonly DependencyProperty TabBackgroundBrushHoverProperty = DependencyProperty.Register(
            nameof(TabBackgroundBrushHover),
            typeof(Brush),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public Brush ForegroundSelected
        {
            get { return (Brush)GetValue(ForegroundSelectedProperty); }
            set { SetValue(ForegroundSelectedProperty, value); }
        }

        private static readonly DependencyProperty ForegroundSelectedProperty = DependencyProperty.Register(
            nameof(ForegroundSelected),
            typeof(Brush),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public Brush ForegroundHover
        {
            get { return (Brush)GetValue(ForegroundHoverProperty); }
            set { SetValue(ForegroundHoverProperty, value); }
        }

        private static readonly DependencyProperty ForegroundHoverProperty = DependencyProperty.Register(
            nameof(ForegroundHover),
            typeof(Brush),
            typeof(TabsTop),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        static TabsTop()
        {
            ForegroundProperty.OverrideMetadata(typeof(TabsTop), new FrameworkPropertyMetadata(Brushes.Black));
            BackgroundProperty.OverrideMetadata(typeof(TabsTop), new FrameworkPropertyMetadata(null));
        }

        public TabsTop() : base()
        {
            InitializeComponent();
            SelectionMode = SelectionMode.Single;

            SizeChanged += TabsTop_SizeChanged;
        }

        private void TabsTop_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetValue(TabSizePropertyKey, e.NewSize.Height);
        }

        private void ScrollTabLeft(object sender, RoutedEventArgs e)
        {
            ScrollViewer scroll = Template.FindName("TabScroll", this) as ScrollViewer;
            scroll.LineLeft();
        }

        private void ScrollTabRight(object sender, RoutedEventArgs e)
        {
            ScrollViewer scroll = Template.FindName("TabScroll", this) as ScrollViewer;
            scroll.LineRight();
        }

        protected override Size MeasureOverride(Size constraint)
        {
            Size controlSize = constraint;

            Size desired = base.MeasureOverride(constraint);

            if (double.IsPositiveInfinity(controlSize.Width))
            {
                controlSize.Width = desired.Width;
            }
            else
            {
                controlSize.Width = Math.Min(controlSize.Width, desired.Width);
            }

            if (double.IsPositiveInfinity(controlSize.Height))
            {
                controlSize.Height = desired.Height;
            }
            else
            {
                controlSize.Height = Math.Min(controlSize.Height, desired.Height);
            }

            return controlSize;
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            Size controlSize = arrangeBounds;

            Size desired = base.ArrangeOverride(arrangeBounds);

            if (double.IsPositiveInfinity(controlSize.Width))
            {
                controlSize.Width = desired.Width;
            }
            else
            {
                controlSize.Width = Math.Min(controlSize.Width, desired.Width);
            }

            if (double.IsPositiveInfinity(controlSize.Height))
            {
                controlSize.Height = desired.Height;
            }
            else
            {
                controlSize.Height = Math.Min(controlSize.Height, desired.Height);
            }

            return controlSize;
        }
    }
}
