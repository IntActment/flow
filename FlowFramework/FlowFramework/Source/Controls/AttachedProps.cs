﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace FlowFramework.Controls
{
    /// <summary>
    /// Attachedプロパティクラス
    /// </summary>
    public static class AttachedProps
    {
        /// <summary>
        /// ラジオボタンのブラッシュ
        /// </summary>
        public static readonly DependencyProperty MarkBrushProperty = DependencyProperty.RegisterAttached(
            "MarkBrush",
            typeof(Brush),
            typeof(AttachedProps),
            new FrameworkPropertyMetadata(default(Brush), FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// ラジオボタンのブラッシュセッター
        /// </summary>
        /// <param name="element">RadioButton</param>
        /// <param name="value">ブラッシュ</param>
        public static void SetMarkBrush(DependencyObject element, Brush value)
        {
            element.SetValue(MarkBrushProperty, value);
        }

        /// <summary>
        /// ラジオボタンのブラッシュゲッター
        /// </summary>
        /// <param name="element">RadioButton</param>
        /// <returns>ブラッシュ</returns>
        public static Brush GetMarkBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(MarkBrushProperty);
        }

        /// <summary>
        /// ラジオボタンのホーバー状態ブラッシュ
        /// </summary>
        public static readonly DependencyProperty MarkHoverBrushProperty = DependencyProperty.RegisterAttached(
            "MarkHoverBrush",
            typeof(Brush),
            typeof(AttachedProps),
            new FrameworkPropertyMetadata(default(Brush), FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// ラジオボタンのホーバー状態ブラッシュセッター
        /// </summary>
        /// <param name="element">Radiobutton</param>
        /// <param name="value">ホーバー状態ブラッシュ</param>
        public static void SetMarkHoverBrush(DependencyObject element, Brush value)
        {
            element.SetValue(MarkHoverBrushProperty, value);
        }

        /// <summary>
        /// ラジオボタンのホーバー状態ブラッシュゲッター
        /// </summary>
        /// <param name="element">Radiobutton</param>
        /// <returns>ホーバー状態ブラッシュ</returns>
        public static Brush GetMarkHoverBrush(DependencyObject element)
        {
            return (Brush)element.GetValue(MarkHoverBrushProperty);
        }

        /// <summary>
        /// ラジオボタンの枠ブラッシュ
        /// </summary>
        public static readonly DependencyProperty MarkStrokeProperty = DependencyProperty.RegisterAttached(
            "MarkStroke",
            typeof(Brush),
            typeof(AttachedProps),
            new FrameworkPropertyMetadata(default(Brush), FrameworkPropertyMetadataOptions.AffectsRender));

        /// <summary>
        /// ラジオボタンの枠ブラッシュセッター
        /// </summary>
        /// <param name="element">Radiobutton</param>
        /// <param name="value">枠ブラッシュ</param>
        public static void SetMarkStroke(DependencyObject element, Brush value)
        {
            element.SetValue(MarkStrokeProperty, value);
        }

        /// <summary>
        /// ラジオボタンの枠ブラッシュゲッター
        /// </summary>
        /// <param name="element">Radiobutton</param>
        /// <returns>枠ブラッシュ</returns>
        public static Brush GetMarkStroke(DependencyObject element)
        {
            return (Brush)element.GetValue(MarkStrokeProperty);
        }

        /// <summary>
        /// テキストサイズ（相対値）
        /// </summary>
        public static readonly DependencyProperty FontSizeRelativeProperty = DependencyProperty.RegisterAttached(
            "FontSizeRelative",
            typeof(double),
            typeof(AttachedProps),
            new FrameworkPropertyMetadata(default(double), FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsParentArrange, FontSizeRelativeChanged));

        /// <summary>
        /// テキストサイズ（相対値）セッター
        /// </summary>
        /// <param name="element">UIコントロール</param>
        /// <param name="value">相対値</param>
        public static void SetFontSizeRelative(DependencyObject element, double value)
        {
            element.SetValue(FontSizeRelativeProperty, value);
        }

        /// <summary>
        /// テキストサイズ（相対値）ゲッター
        /// </summary>
        /// <param name="element">UIコントロール</param>
        /// <returns>相対値</returns>
        public static double GetFontSizeRelative(DependencyObject element)
        {
            return (double)element.GetValue(FontSizeRelativeProperty);
        }

        /// <summary>
        /// テキストサイズ（相対値）変更イベント処理
        /// </summary>
        /// <param name="d">UIコントロール</param>
        /// <param name="e">イベントパラメータ</param>
        private static void FontSizeRelativeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.Property != FontSizeRelativeProperty)
            {
                return;
            }

            if (!(d is FrameworkElement fe))
            {
                return;
            }

            FrameworkElement p = fe.Parent as FrameworkElement;
            double parentFontSize = 0;

            // フォントサイズ自動計算
            while (p != null)
            {

                if (p is Control pc)
                {
                    parentFontSize = pc.FontSize;

                    break;
                }

                p = p.Parent as FrameworkElement;
            }

            if(p == null)
            {
                return;
            }

            // コントロールフォントサイズ計算
            if (d is Control c)
            {
                c.FontSize = Math.Max(parentFontSize + (double)e.NewValue, 0);

                return;
            }

            // テキストボックスフォントサイズ計算
            if (d is TextBlock t)
            {
                t.FontSize = Math.Max(parentFontSize + (double)e.NewValue, 0);

                return;
            }

            throw new InvalidOperationException("Only those types allowed: Control, TextBlock.");
        }

        /// <summary>
        /// テキストサイズ（相対値）
        /// </summary>
        public static readonly DependencyProperty WidthForLengthProperty = DependencyProperty.RegisterAttached(
            "WidthForLength",
            typeof(int),
            typeof(AttachedProps),
            new FrameworkPropertyMetadata(default(int), FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsParentArrange, WidthForLengthChanged));

        /// <summary>
        /// テキストサイズ（相対値）セッター
        /// </summary>
        /// <param name="element">UIコントロール</param>
        /// <param name="value">相対値</param>
        public static void SetWidthForLength(DependencyObject element, int value)
        {
            element.SetValue(WidthForLengthProperty, value);
        }

        /// <summary>
        /// テキストサイズ（相対値）ゲッター
        /// </summary>
        /// <param name="element">UIコントロール</param>
        /// <returns>相対値</returns>
        public static int GetWidthForLength(DependencyObject element)
        {
            return (int)element.GetValue(WidthForLengthProperty);
        }

        /// <summary>
        /// テキストサイズ（相対値）変更イベント処理
        /// </summary>
        /// <param name="d">UIコントロール</param>
        /// <param name="e">イベントパラメータ</param>
        private static void WidthForLengthChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (e.Property != WidthForLengthProperty)
            {
                return;
            }

            if (!(d is TextBox tb))
            {
                throw new InvalidOperationException("Only those types allowed: TextBox.");
            }

            var formattedText = new FormattedText(
                new string('W', (int)e.NewValue),
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface(tb.FontFamily, tb.FontStyle, tb.FontWeight, tb.FontStretch),
                tb.FontSize,
                Brushes.Black,
                new NumberSubstitution(),
                VisualTreeHelper.GetDpi(tb).PixelsPerDip);

            tb.Width = formattedText.Width;
        }

        /// <summary>
        /// 共通グループ
        /// </summary>
        public static readonly DependencyProperty SharedSizeGroupProperty = DependencyProperty.RegisterAttached(
            "SharedSizeGroup",
            typeof(string),
            typeof(AttachedProps),
            new FrameworkPropertyMetadata(default(string), FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.AffectsMeasure, null));

        /// <summary>
        /// 共通グループセッター
        /// </summary>
        /// <param name="element">UIコントロール</param>
        /// <param name="value">グループ</param>
        public static void SetSharedSizeGroup(DependencyObject element, string value)
        {
            element.SetValue(SharedSizeGroupProperty, value);
        }

        /// <summary>
        /// 共通グループゲッター
        /// </summary>
        /// <param name="element">UIコントロール</param>
        /// <returns>グループ</returns>
        public static string GetSharedSizeGroup(DependencyObject element)
        {
            return (string)element.GetValue(SharedSizeGroupProperty);
        }
    }
}
