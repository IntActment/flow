﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FlowFramework.Controls
{
    /// <summary>
    /// Interaction logic for CustomButton.xaml
    /// </summary>
    public partial class CustomButton : Button
    {
        /// <summary>
        /// ボタン半径
        /// </summary>
        public double CornerRadius
        {
            get { return (double)GetValue(CornerRadiusProperty); }
            set { SetValue(CornerRadiusProperty, value); }
        }

        public static readonly DependencyProperty CornerRadiusProperty = DependencyProperty.Register(
            nameof(CornerRadius),
            typeof(double),
            typeof(CustomButton),
            new FrameworkPropertyMetadata(3.0, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態の枠色
        /// </summary>
        public Brush BorderBrushPressed
        {
            get { return (Brush)GetValue(BorderBrushPressedProperty); }
            set { SetValue(BorderBrushPressedProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushPressedProperty = DependencyProperty.Register(
            nameof(BorderBrushPressed),
            typeof(Brush),
            typeof(CustomButton),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// ホバー状態の枠色
        /// </summary>
        public Brush BorderBrushHover
        {
            get { return (Brush)GetValue(BorderBrushHoverProperty); }
            set { SetValue(BorderBrushHoverProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushHoverProperty = DependencyProperty.Register(
            nameof(BorderBrushHover),
            typeof(Brush),
            typeof(CustomButton),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// コントロール無効状態の枠色
        /// </summary>
        public Brush BorderBrushDisabled
        {
            get { return (Brush)GetValue(BorderBrushDisabledProperty); }
            set { SetValue(BorderBrushDisabledProperty, value); }
        }

        public static readonly DependencyProperty BorderBrushDisabledProperty = DependencyProperty.Register(
            nameof(BorderBrushDisabled),
            typeof(Brush),
            typeof(CustomButton),
            new FrameworkPropertyMetadata(Brushes.Orange, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態の背景色
        /// </summary>
        public Brush BackgroundPressed
        {
            get { return (Brush)GetValue(BackgroundPressedProperty); }
            set { SetValue(BackgroundPressedProperty, value); }
        }

        public static readonly DependencyProperty BackgroundPressedProperty = DependencyProperty.Register(
            nameof(BackgroundPressed),
            typeof(Brush),
            typeof(CustomButton),
            new FrameworkPropertyMetadata(Brushes.OrangeRed, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// ホバー状態の背景色
        /// </summary>
        public Brush BackgroundHover
        {
            get { return (Brush)GetValue(BackgroundHoverProperty); }
            set { SetValue(BackgroundHoverProperty, value); }
        }

        public static readonly DependencyProperty BackgroundHoverProperty = DependencyProperty.Register(
            nameof(BackgroundHover),
            typeof(Brush),
            typeof(CustomButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// コントロール無効状態の背景色
        /// </summary>
        public Brush BackgroundDisabled
        {
            get { return (Brush)GetValue(BackgroundDisabledProperty); }
            set { SetValue(BackgroundDisabledProperty, value); }
        }

        public static readonly DependencyProperty BackgroundDisabledProperty = DependencyProperty.Register(
            nameof(BackgroundDisabled),
            typeof(Brush),
            typeof(CustomButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// 押下中状態のテキスト色
        /// </summary>
        public Brush ForegroundPressed
        {
            get { return (Brush)GetValue(ForegroundPressedProperty); }
            set { SetValue(ForegroundPressedProperty, value); }
        }

        public static readonly DependencyProperty ForegroundPressedProperty = DependencyProperty.Register(
            nameof(ForegroundPressed),
            typeof(Brush),
            typeof(CustomButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// ホバー状態のテキスト色
        /// </summary>
        public Brush ForegroundHover
        {
            get { return (Brush)GetValue(ForegroundHoverProperty); }
            set { SetValue(ForegroundHoverProperty, value); }
        }

        public static readonly DependencyProperty ForegroundHoverProperty = DependencyProperty.Register(
            nameof(ForegroundHover),
            typeof(Brush),
            typeof(CustomButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// コントロール無効状態のテキスト色
        /// </summary>
        public Brush ForegroundDisabled
        {
            get { return (Brush)GetValue(ForegroundDisabledProperty); }
            set { SetValue(ForegroundDisabledProperty, value); }
        }

        public static readonly DependencyProperty ForegroundDisabledProperty = DependencyProperty.Register(
            nameof(ForegroundDisabled),
            typeof(Brush),
            typeof(CustomButton),
            new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.AffectsRender, null));

        /// <summary>
        /// スタティックコンストラクタ
        /// </summary>
        static CustomButton()
        {
            BackgroundProperty.OverrideMetadata(typeof(CustomButton), new FrameworkPropertyMetadata(Brushes.White));
            ForegroundProperty.OverrideMetadata(typeof(CustomButton), new FrameworkPropertyMetadata(Brushes.Black));
            BorderBrushProperty.OverrideMetadata(typeof(CustomButton), new FrameworkPropertyMetadata(Brushes.LightGray));
            BorderThicknessProperty.OverrideMetadata(typeof(CustomButton), new FrameworkPropertyMetadata(new Thickness(1.0)));
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public CustomButton() : base()
        {
            InitializeComponent();
        }
    }
}
