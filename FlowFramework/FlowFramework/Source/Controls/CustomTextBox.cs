﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;

namespace FlowFramework.Controls
{
    /// <summary>
    /// 拡張されたテキストボックスコントロールクラス
    /// </summary>
    public class CustomTextBox : TextBox
    {
        public bool ScrollToEndOnChange
        {
            get { return (bool)GetValue(ScrollToEndOnChangeProperty); }
            set { SetValue(ScrollToEndOnChangeProperty, value); }
        }

        public static readonly DependencyProperty ScrollToEndOnChangeProperty = DependencyProperty.Register(
            nameof(ScrollToEndOnChange),
            typeof(bool),
            typeof(CustomTextBox),
            new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public bool IsValid
        {
            get { return (bool)GetValue(IsValidProperty); }
            set { SetValue(IsValidProperty, value); }
        }

        public static readonly DependencyProperty IsValidProperty = DependencyProperty.Register(
            nameof(IsValid),
            typeof(bool),
            typeof(CustomTextBox),
            new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public IFilterCommand<string> FilterCommand
        {
            get { return (IFilterCommand<string>)GetValue(FilterCommandProperty); }
            set { SetValue(FilterCommandProperty, value); }
        }

        public static readonly DependencyProperty FilterCommandProperty = DependencyProperty.Register(
            nameof(FilterCommand),
            typeof(IFilterCommand<string>),
            typeof(CustomTextBox),
            new FrameworkPropertyMetadata(default(IFilterCommand<string>)));

        public CustomTextBox() : base()
        {
            DataObject.AddPastingHandler(this, OnPaste);

            TextCompositionManager.AddPreviewTextInputStartHandler(this, new TextCompositionEventHandler(PreviewTextInputHandler));
            PreviewKeyDown += PreviewKeyDownHandler;
            TextChanged += CustomTextBox_TextChanged;
        }

        private void CustomTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (true == ScrollToEndOnChange)
            {
                CaretIndex = (null == Text) ? 0 : Text.Length;
                var rect = GetRectFromCharacterIndex(CaretIndex);
                ScrollToHorizontalOffset(rect.Right);
            }
        }

        private void PreviewTextInputHandler(object sender, TextCompositionEventArgs e)
        {
            string resText = Text
                .Remove(SelectionStart, SelectionLength)
                .Insert(SelectionStart, e.TextComposition.Text);

            if (false == ValidateText(resText))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// 貼り付けイベント処理
        /// </summary>
        /// <param name="sender">センダー</param>
        /// <param name="e">イベントパラメータ</param>
        private void OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(DataFormats.Text))
            {
                string text = Convert.ToString(e.DataObject.GetData(DataFormats.Text));

                string resText = Text
                    .Remove(SelectionStart, SelectionLength)
                    .Insert(SelectionStart, text);

                if (false == ValidateText(resText))
                {
                    e.CancelCommand();
                }
            }
            else
            {
                e.CancelCommand();
            }
        }

        /// <summary>
        /// 入力テキストの有効・無効チェック
        /// </summary>
        /// <param name="text">入力テキスト</param>
        /// <returns>
        /// true：有効
        /// false：無効
        /// </returns>
        private bool ValidateText(string text)
        {
            if (null == FilterCommand)
            {
                return true;
            }

            return FilterCommand.CanPassValue(in text);
        }

        private void PreviewKeyDownHandler(object sender, KeyEventArgs e)
        {
            string sym;

            if (e.Key == Key.Space)
            {
                sym = " ";
            }
            else if ((e.Key == Key.ImeProcessed) && (e.ImeProcessedKey == Key.Space))
            {
                sym = "　";
            }
            else
            {
                return;
            }

            string resText = Text
                .Remove(SelectionStart, SelectionLength)
                .Insert(SelectionStart, sym);

            if (false == ValidateText(resText))
            {
                e.Handled = true;
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (e.Key == Key.Return)
            {
                if (e.Key == Key.Enter)
                {
                    // Kill logical focus
                    FocusManager.SetFocusedElement(FocusManager.GetFocusScope(this), null);

                    // Kill keyboard focus
                    Keyboard.ClearFocus();
                }
            }
        }
    }
}
