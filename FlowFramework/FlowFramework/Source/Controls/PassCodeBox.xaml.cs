﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using FlowFramework.Utils;
using FlowFramework.ViewModel;

namespace FlowFramework.Controls
{
    /// <summary>
    /// Interaction logic for PassCodeBox.xaml
    /// </summary>
    public partial class PassCodeBox : Control, INotifyPropertyChanged, IDisposable, IPassCode
    {
        public class Symbol
        {
            public char Sym { get; private set; }

            public bool IsFiller { get; private set; }

            public Symbol(char sym, bool isFiller)
            {
                Sym = sym;
                IsFiller = isFiller;
            }
        }

        private ushort m_passCount = 0;
        private ObservableCollection<Symbol> m_passList = new ObservableCollection<Symbol>();

        private bool m_internalChange = false;

        private void AddChar(char sym, bool update = true)
        {
            if(m_passCount >= MaxLength)
            {
                return;
            }

            m_passCodeInternal.RemoveAt(0);
            m_passList.RemoveAt(0);
            m_passCount++;

            m_passCodeInternal.AppendChar(sym);
            m_passList.Add(new Symbol(sym, false));

            m_internalChange = true;

            if (true == update)
            {
                SetValue(PassCodeProperty, m_passCodeInternal);
                OnPropertyRaised(nameof(Symbols));
            }
            else
            {
                SetCurrentValue(PassCodeProperty, m_passCodeInternal);
            }

            m_internalChange = false;
        }

        private void DeleteLastChar(bool update = true)
        {
            if (m_passCount == 0)
            {
                return;
            }

            m_passCodeInternal.RemoveAt(m_passCodeInternal.Length - 1);
            m_passList.RemoveAt(m_passList.Count - 1);
            m_passCount--;

            m_passCodeInternal.InsertAt(0, '0');
            m_passList.Insert(0, new Symbol('0', true));

            m_internalChange = true;

            if (true == update)
            {
                SetValue(PassCodeProperty, m_passCodeInternal);
                OnPropertyRaised(nameof(Symbols));
            }
            else
            {
                SetCurrentValue(PassCodeProperty, m_passCodeInternal);
            }

            m_internalChange = false;
        }

        public void Clear()
        {
            Clear(true);
        }

        public void Clear(bool update)
        {
            m_passCodeInternal.Clear();
            m_passList.Clear();
            m_passCount = 0;

            for(int i = 0; i < MaxLength; i++)
            {
                m_passCodeInternal.AppendChar('0');
                m_passList.Add(new Symbol('0', true));
            }

            m_internalChange = true;

            if (true == update)
            {
                SetValue(PassCodeProperty, m_passCodeInternal);
                OnPropertyRaised(nameof(Symbols));
            }
            else
            {
                SetCurrentValue(PassCodeProperty, m_passCodeInternal);
            }

            m_internalChange = false;
        }

        public ObservableCollection<Symbol> Symbols
        {
            get { return m_passList; }
        }

        private SecureString m_passCodeInternal;

        public SecureString PassCode
        {
            get { return (SecureString)GetValue(PassCodeProperty); }
            set { SetValue(PassCodeProperty, value); }
        }

        private static readonly DependencyProperty PassCodeProperty = DependencyProperty.Register(
            "PassCode",
            typeof(SecureString),
            typeof(PassCodeBox),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.AffectsRender, null, CoercePassCode));

        private static object CoercePassCode(DependencyObject d, object value)
        {
            PassCodeBox pcb = (PassCodeBox)d;

            if (null == pcb.PassCode)
            {
                return value;
            }

            SecureString pass = (SecureString)value;

            if (null != pass)
            {
                if (false == pcb.m_internalChange)
                {
                    pcb.UpdatePassCode(pass);
                }

                pass.CopyTo(pcb.PassCode);
            }

            return pcb.PassCode;
        }

        public ICommand EnterCommand
        {
            get { return (ICommand)GetValue(EnterCommandProperty); }
            set { SetValue(EnterCommandProperty, value); }
        }

        private static readonly DependencyProperty EnterCommandProperty = DependencyProperty.Register(
            nameof(EnterCommand),
            typeof(ICommand),
            typeof(PassCodeBox),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None, null, null));

        public Brush FillerForeground
        {
            get { return (Brush)GetValue(FillerForegroundProperty); }
            set { SetValue(FillerForegroundProperty, value); }
        }

        private static readonly DependencyProperty FillerForegroundProperty = DependencyProperty.Register(
            "FillerForeground",
            typeof(Brush),
            typeof(PassCodeBox),
            new FrameworkPropertyMetadata(Brushes.Gray, FrameworkPropertyMetadataOptions.AffectsRender, null));

        public bool ShowPass
        {
            get { return (bool)GetValue(ShowPassProperty); }
            set { SetValue(ShowPassProperty, value); }
        }

        private static readonly DependencyProperty ShowPassProperty = DependencyProperty.Register(
            "ShowPass",
            typeof(bool),
            typeof(PassCodeBox),
            new FrameworkPropertyMetadata(true, FrameworkPropertyMetadataOptions.AffectsMeasure, null));

        public char MaskChar
        {
            get { return (char)GetValue(MaskCharProperty); }
            set { SetValue(MaskCharProperty, value); }
        }

        private static readonly DependencyProperty MaskCharProperty = DependencyProperty.Register(
            "MaskChar",
            typeof(char),
            typeof(PassCodeBox),

            new FrameworkPropertyMetadata('*', FrameworkPropertyMetadataOptions.AffectsMeasure, null));

        public ushort MaxLength
        {
            get { return (ushort)GetValue(MaxLengthProperty); }
            set { SetValue(MaxLengthProperty, value); }
        }

        private static readonly DependencyProperty MaxLengthProperty = DependencyProperty.Register(
            "MaxLength",
            typeof(ushort),
            typeof(PassCodeBox),
            new FrameworkPropertyMetadata((ushort)7, FrameworkPropertyMetadataOptions.AffectsMeasure, UpdateTextWidth));

        public double CharWidth
        {
            get { return (double)GetValue(CharWidthProperty); }
            set { SetValue(CharWidthProperty, value); }
        }

        private static readonly DependencyProperty CharWidthProperty = DependencyProperty.Register(
            "CharWidth",
            typeof(double),
            typeof(PassCodeBox),
            new FrameworkPropertyMetadata((double)15.0, FrameworkPropertyMetadataOptions.AffectsMeasure, UpdateTextWidth));


        private static void UpdateTextWidth(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PassCodeBox pcb = d as PassCodeBox;

            pcb.OnPropertyRaised(nameof(TextWidth));

            pcb.Clear();
        }

        public double TextWidth
        {
            get { return CharWidth * MaxLength; }
        }

        public PassCodeBox() : base()
        {
            m_passCodeInternal = new SecureString();
            SetCurrentValue(PassCodeProperty, new SecureString());

            Clear(false);

            InitializeComponent();
            Focusable = true;
            FocusVisualStyle = null;

            IsVisibleChanged += PassCodeBox_IsVisibleChanged;

            Dispatcher.ShutdownStarted += DispatcherOnShutdownStarted;
            Application.Current.Exit += Current_Exit;

            InputMethod.SetIsInputMethodEnabled(this, false);
        }

        private void PassCodeBox_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            bool isVisible = (bool)e.NewValue;

            if (false == isVisible)
            {
                return;
            }

            Focusable = true;
            Keyboard.Focus(this);
            Focus();
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);

            Keyboard.Focus(this);
            Focus();
        }

        protected override void OnLostKeyboardFocus(KeyboardFocusChangedEventArgs e)
        {
            base.OnLostKeyboardFocus(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (Keyboard.Modifiers != ModifierKeys.None)
            {
                return;
            }

            e.Handled = true;

            if (e.Key == Key.Enter)
            {
                if ((null != EnterCommand) && (EnterCommand.CanExecute(PassCode)))
                {
                    EnterCommand.Execute(this);
                }
                else
                {
                    // Kill logical focus
                    FocusManager.SetFocusedElement(FocusManager.GetFocusScope(this), null);

                    // Kill keyboard focus
                    Keyboard.ClearFocus();
                }
            }
            else if (e.Key == Key.Back)  //< Backspace
            {
                DeleteLastChar();
            }
            else if (e.Key == Key.Delete)
            {
                Clear();
            }
            else if ((((e.Key >= Key.D0) && (e.Key <= Key.D9))              //< 0 ~ 9
                || ((e.Key >= Key.NumPad0) && (e.Key <= Key.NumPad9)))      //< numpad 0 ~ 9
                && (m_passCount < MaxLength))
            {
                switch (e.Key)
                {
                    case Key.D0: case Key.NumPad0: AddChar('0'); break;
                    case Key.D1: case Key.NumPad1: AddChar('1'); break;
                    case Key.D2: case Key.NumPad2: AddChar('2'); break;
                    case Key.D3: case Key.NumPad3: AddChar('3'); break;
                    case Key.D4: case Key.NumPad4: AddChar('4'); break;
                    case Key.D5: case Key.NumPad5: AddChar('5'); break;
                    case Key.D6: case Key.NumPad6: AddChar('6'); break;
                    case Key.D7: case Key.NumPad7: AddChar('7'); break;
                    case Key.D8: case Key.NumPad8: AddChar('8'); break;
                    case Key.D9: case Key.NumPad9: AddChar('9'); break;
                }
            }
            else
            {
                SystemSounds.Beep.Play();
            }
        }

        private bool IsValidCode(string strCode)
        {
            if (strCode.Length > MaxLength)
            {
                // パスコードの長さは無効
                return false;
            }

            foreach (char sym in strCode)
            {
                if((sym < '0') || (sym > '9'))
                {
                    return false;
                }
            }

            return true;
        }

        public void UpdatePassCode(SecureString source)
        {
            IntPtr unmanagedString = IntPtr.Zero;
            string text;

            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(source);
                text = Marshal.PtrToStringUni(unmanagedString);
            }
            catch
            {
                return;
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }

            if( false == IsValidCode(text))
            {
                return;
            }

            Clear(false);

            foreach(char sym in text)
            {
                AddChar(sym, false);
            }

            OnPropertyRaised(nameof(Symbols));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyRaised(string propertyname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }

        private bool m_isDisposed = false;

        private void DispatcherOnShutdownStarted(object sender, EventArgs eventArgs)
        {
            Dispose();
            Dispatcher.ShutdownStarted -= DispatcherOnShutdownStarted;
        }

        private void Current_Exit(object sender, ExitEventArgs e)
        {
            Dispose();
            Application.Current.Exit -= Current_Exit;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        ~PassCodeBox()
        {

        }

        void Dispose(bool isDisposing)
        {
            if (m_isDisposed) return;

            PassCode.Dispose();
            m_passCodeInternal.Dispose();

            m_isDisposed = true;
            if (true == isDisposing)
            {
                GC.SuppressFinalize(this);
            }
        }
    }
}
