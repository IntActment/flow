﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;

using FlowFramework.Utils;
using FlowFramework.ViewModel;

namespace FlowFramework.View
{
    /// <summary>
    /// 画面用ウィンドウのベースクラス
    /// </summary>
    public class WindowBase : Window
    {
        /// <summary>
        /// ウィンドウ内のViewModel対象一覧
        /// </summary>
        public Dictionary<Type, IViewModel> ViewModels { get; private set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public WindowBase()
        {
            PreviewMouseDown += MainWindow_PreviewMouseDown;
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            HwndSource source = PresentationSource.FromVisual(this) as HwndSource;
            source.AddHook(WndProc);
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            const int WM_ENABLE = 0x000A;

            if (msg == WM_ENABLE)
            {
                bool isEnabled = ((uint)wParam) != 0;

                if (null != m_backLayer)
                m_backLayer.Visibility = isEnabled ? Visibility.Collapsed : Visibility.Visible;
            }

            return IntPtr.Zero;
        }

        /// <summary>
        /// ウィンドウ初期化終了処理
        /// </summary>
        /// <param name="e">イベントパラメータ</param>
        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            ViewModels = Resources.Values.OfType<IViewModel>().ToDictionary(k => k.GetType());

            ViewModels.ForEach(vm => vm.Value.SetWindowOwner(this));
        }

        /// <summary>
        /// 黒いレイヤーインスタンス
        /// </summary>
        private UIElement m_backLayer;

        /// <summary>
        /// テンプレート設定イベント処理
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            m_backLayer = (UIElement)Template.FindName("BackLayer", this);
        }

        /// <summary>
        /// マウス押下プレビューイベントハンドラ処理
        /// </summary>
        /// <param name="sender">センダー</param>
        /// <param name="e">イベントパラメータ</param>
        private void MainWindow_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if ((null != Keyboard.FocusedElement) && (true == Keyboard.FocusedElement.GetType().IsSubclassOf(typeof(TextBox))))
            {
                if (!(e.OriginalSource is Visual) || (((Visual)e.OriginalSource).FindParentOfType<TextBox>() != Keyboard.FocusedElement))
                {
                    // Kill logical focus
                    FocusManager.SetFocusedElement(FocusManager.GetFocusScope((TextBox)Keyboard.FocusedElement), null);

                    // Kill keyboard focus
                    Keyboard.ClearFocus();
                }
            }
        }

        /// <summary>
        /// アプリ終了要求フラグ
        /// </summary>
        private bool m_closeRequest;

        /// <summary>
        /// ウィンドウを閉じるイベント処理
        /// </summary>
        /// <param name="e">イベントパラメータ</param>
        protected override void OnClosing(CancelEventArgs e)
        {
            if (true == m_closeRequest)
            {
                // アプリ終了
                e.Cancel = false;
                return;
            }

            base.OnClosing(e);
        }

        /// <summary>
        /// アプリを終了させる処理
        /// </summary>
        public void ForceClose()
        {
            m_closeRequest = true;

            Close();
        }
    }
}
