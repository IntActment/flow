﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using FlowFramework.Controls;
using FlowFramework.Utils;
using static FlowFramework.Utils.Message;

namespace FlowFramework.View
{
    /// <summary>
    /// Interaction logic for ErrorDialog.xaml
    /// </summary>
    public partial class ErrorDialog : Window
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ErrorDialog()
        {
            InitializeComponent();

            Resources.MergedDictionaries.Add(Application.Current.Resources);
        }

        /// <summary>
        /// ボタンマップ
        /// </summary>
        private Dictionary<ErrorButton, CustomButton> m_buttonMap = new Dictionary<ErrorButton, CustomButton>();

        private TextBlock m_desc;

        /// <summary>
        /// ダイアログ結果
        /// </summary>
        private ErrorButton? m_result;

        /// <summary>
        /// ダイアログを移動させるようにマウスイベントハンドラ処理
        /// </summary>
        /// <param name="sender">UI対象</param>
        /// <param name="e">イベントパラメータ</param>
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                // ダイアログ移動開始
                DragMove();
            }
        }

        /// <summary>
        /// ボタンにEnum値を合わせる処理
        /// </summary>
        /// <param name="buttonType">Enum値</param>
        /// <param name="buttonName">ボタン名</param>
        public void RegisterButton(ErrorButton buttonType, string buttonName)
        {
            Debug.Assert(null != buttonName);

            CustomButton button = this.FindFirstChild<CustomButton>(buttonName);

            Debug.Assert(null != button);

            m_buttonMap.Add(buttonType, button);

            // マウスクリックイベントハンドラ処理
            void Click(object _, RoutedEventArgs e)
            {
                // ボタンクリックがダイアログの移動を開始されないように
                e.Handled = true;

                m_result = buttonType;
                m_closeRequest = true;

                // ダイアログを閉じる
                Close();
            };

            button.Click += Click;
        }

        private string m_message;
        private ErrorButtons m_buttons;

        /// <summary>
        /// ダイアログのモーダル表示処理
        /// </summary>
        /// <param name="errorName">エラーテキスト</param>
        /// <param name="descriptionFormat">エラー説明テキスト（フォーマット）</param>
        /// <param name="code">エラーコードテキスト</param>
        /// <param name="buttons">ボタン組み合わせ</param>
        /// <param name="descParams">エラー説明テキスト（フォーマット）のパラメータ</param>
        /// <returns>ダイアログ表示結果</returns>
        public static ErrorButton ShowError(string descriptionFormat, ErrorButtons buttons, params object[] descParams)
        {
            // ダイアログ作成
            ErrorDialog dialog = new ErrorDialog();
            dialog.Loaded += Dialog_Loaded;

            if (false == string.IsNullOrWhiteSpace(descriptionFormat))
            {
                dialog.m_message = string.Format(descriptionFormat, descParams);
            }

            dialog.m_buttons = buttons;

            // モーダル表示に親ウィンドウを指定する（親ウィンドウを自動的に無効されるように）
            if (Application.Current.MainWindow != dialog)
            {
                dialog.Owner = Application.Current.MainWindow;
            }

            // ダイアログ表示
            dialog.ShowDialog();
            dialog.Loaded -= Dialog_Loaded;

            Debug.Assert(null != dialog.m_result);

            return dialog.m_result.Value;
        }

        private static void Dialog_Loaded(object sender, RoutedEventArgs e)
        {
            ErrorDialog dialog = (ErrorDialog)sender;

            dialog.m_desc = dialog.FindFirstChild<TextBlock>("description");

            // 各ボタンに該当ErrorButtonを指定する
            dialog.RegisterButton(ErrorButton.Close,    "button_close");
            dialog.RegisterButton(ErrorButton.Cancel,   "button_cancel");
            dialog.RegisterButton(ErrorButton.Continue, "button_continue");
            dialog.RegisterButton(ErrorButton.OK,       "button_ok");
            dialog.RegisterButton(ErrorButton.Retry,    "button_retry");
            dialog.RegisterButton(ErrorButton.Shutdown, "button_shutdown");
            dialog.RegisterButton(ErrorButton.Ignore,   "button_ignore");

            // 説明テキスト設定
            if (null == dialog.m_message)
            {
                dialog.m_desc.Visibility = Visibility.Collapsed;
            }
            else
            {
                dialog.m_desc.Text = dialog.m_message;
            }

            // 選択されたボタンだけを表示する
            foreach (var pair in dialog.m_buttonMap)
            {
                pair.Value.Visibility = dialog.m_buttons.HasFlag((ErrorButtons)pair.Key) ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        // ダイアログを強制的に閉じられないように（例：Alt+F4）フラグ
        private bool m_closeRequest = false;

        /// <summary>
        /// ダイアログを閉じる時の処理
        /// </summary>
        /// <param name="e">イベントパラメータ</param>
        protected override void OnClosing(CancelEventArgs e)
        {
            if (false == m_closeRequest)
            {
                // 閉じられない
                e.Cancel = true;
            }
        }
    }
}
