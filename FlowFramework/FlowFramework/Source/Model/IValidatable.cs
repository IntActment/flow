﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowFramework.Model
{
    /// <summary>
    /// 有効/無効状態インターフェース
    /// </summary>
    public interface IValidatable
    {
        /// <summary>
        /// 有効状態フラグ
        /// </summary>
        bool IsValid { get; }
    }
}
