﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowFramework.Model
{
    public struct TimeData :
        IComparable,
        IComparable<TimeData>,
        IEquatable<TimeData>
    {
        public byte hour;

        public byte minute;

        public byte second;

        public static readonly TimeData Invalid = new TimeData(255, 255, 255);

        public TimeData(byte h, byte m, byte s)
        {
            hour = h;
            minute = m;
            second = s;
        }

        public int CompareTo(object obj)
        {
            if (obj is TimeData rhs)
            {
                return CompareTo(rhs);
            }

            return -1;
        }

        public int CompareTo(TimeData other)
        {
            int res;
            if ((res = hour.CompareTo(other.hour)) != 0)
            {
                return res;
            }

            if ((res = minute.CompareTo(other.minute)) != 0)
            {
                return res;
            }

            return second.CompareTo(other.second);
        }

        public bool Equals(TimeData other)
        {
            return (hour == other.hour)
                && (minute == other.minute)
                && (second == other.second);
        }
    }

    [TypeDescriptionProvider(typeof(Converters.ValueWrapperDescriptionProvider))]
    public class Time : ValueWrapper<TimeData>
    {
        public const string DELIMITER = ":";

        public byte Hour => Value.hour;

        public byte Minute => Value.minute;

        public byte Second => Value.second;

        public Time(byte hour, byte minute, byte second)
            : base(new TimeData(hour, minute, second))
        {
        }

        public Time(int hour, int minute, int second)
            : this((byte)hour, (byte)minute, (byte)second)
        {
        }

        public Time (TimeSpan span)
            : this(span.Hours, span.Minutes, span.Seconds)
        {
            if (span.Days > 0)
            {
                throw new InvalidOperationException(nameof(span));
            }
        }

        static Time()
        {
            ValueWrapperMetadata.Defaults.Add(typeof(Time), Time.Invalid);
            ValueWrapperMetadata.DefaultValues.Add(typeof(Time), TimeData.Invalid);
            ValueWrapperMetadata.Constructors.Add(typeof(Time), Parse);
        }

        public static readonly Time Invalid = new Time(TimeData.Invalid.hour, TimeData.Invalid.minute, TimeData.Invalid.second);


        public static Time Parse(string text)
        {
            if (null == text)
            {
                throw new ArgumentNullException(nameof(text));
            }

            if (text == InvalidDataText)
            {
                return Time.Invalid;
            }

            int delimPos1 = text.IndexOf(DELIMITER);
            if (-1 == delimPos1)
            {
                throw new FormatException();
            }

            int delimPos2 = text.IndexOf(DELIMITER, delimPos1 + 1);
            if (-1 == delimPos2)
            {
                throw new FormatException();
            }

            if (false == byte.TryParse(text.Substring(0, delimPos1), out byte h))
            {
                throw new FormatException();
            }

            if (false == byte.TryParse(text.Substring(delimPos1 + 1, delimPos2 - delimPos1 - 1), out byte m))
            {
                throw new FormatException();
            }

            if (false == byte.TryParse(text.Substring(delimPos2 + 1), out byte s))
            {
                throw new FormatException();
            }

            return new Time(h, m, s);
        }

        #region Interface implementations

        public override string GetRawValueString()
        {
            StringBuilder sb = new StringBuilder(8, 8);

            sb.Append(Hour.ToString("D2"));
            sb.Append(DELIMITER);
            sb.Append(Minute.ToString("D2"));
            sb.Append(DELIMITER);
            sb.Append(Second.ToString("D2"));

            return sb.ToString();
        }

        /// <summary>
        /// HHmmss文字列へ変換する処理。
        /// </summary>
        /// <returns>HHmmss文字列</returns>
        /// <exception cref="InvalidOperationException" />
        public string ToHHmmssString()
        {
            if (false == IsValid)
            {
                throw new InvalidOperationException();
            }

            StringBuilder sb = new StringBuilder(6, 6);

            sb.Append(Hour.ToString("D2"));
            sb.Append(Minute.ToString("D2"));
            sb.Append(Second.ToString("D2"));

            return sb.ToString();
        }

        public override string ToString()
        {
            if (false == IsValid)
            {
                const string invalidTime = "--" + DELIMITER + "--" + DELIMITER + "--";

                return invalidTime;
            }

            StringBuilder sb = new StringBuilder(8, 8);

            sb.Append(Hour.ToString("D2"));
            sb.Append(DELIMITER);
            sb.Append(Minute.ToString("D2"));
            sb.Append(DELIMITER);
            sb.Append(Second.ToString("D2"));

            return sb.ToString();
        }

        public static bool operator >(Time lhs, Time rhs)
        {
            long lhsVal = lhs.Hour * 60 * 60 + lhs.Minute * 60 + lhs.Second;
            long rhsVal = rhs.Hour * 60 * 60 + rhs.Minute * 60 + rhs.Second;

            return (lhsVal > rhsVal);
        }

        public static bool operator <(Time lhs, Time rhs)
        {
            long lhsVal = lhs.Hour * 60 * 60 + lhs.Minute * 60 + lhs.Second;
            long rhsVal = rhs.Hour * 60 * 60 + rhs.Minute * 60 + rhs.Second;

            return (lhsVal < rhsVal);
        }

        public override bool IsValid
        {
            get
            {
                if (Hour > 23)
                {
                    return false;
                }

                if (Minute > 59)
                {
                    return false;
                }

                if (Second > 59)
                {
                    return false;
                }

                return true;
            }
        }

        #endregion
    }
}
