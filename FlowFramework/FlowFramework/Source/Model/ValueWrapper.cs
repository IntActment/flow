﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowFramework.Model
{
    public static class ValueWrapperMetadata
    {
        public static Dictionary<Type, object> Defaults { get; } = new Dictionary<Type, object>();

        public static Dictionary<Type, object> DefaultValues { get; } = new Dictionary<Type, object>();

        public static Dictionary<Type, Func<string, object>> Constructors { get; } = new Dictionary<Type, Func<string, object>>();
    }

    public interface IValue
    {
        string GetRawValueString();
    }

    public abstract class ValueWrapperBase<T> :
        NotifyStatic,
        IValue,
        IEquatable<ValueWrapperBase<T>>,
        IValidatable
    {
        internal T Value { get; }

        public const string InvalidDataText = "<無効値>";

        protected ValueWrapperBase(T value)
        {
            Value = value;
        }

        #region Interface implementations

        public virtual string GetRawValueString()
        {
            return Value.ToString();
        }

        public virtual bool IsValid { get { return true; } }

        public bool Equals(ValueWrapperBase<T> rhs)
        {
            if (ReferenceEquals(rhs, null))
            {
                return false;
            }

            return Value.Equals(rhs.Value);
        }

        public override bool Equals(object obj)
        {
            if ((null == obj) || (obj.GetType() != GetType()))
            {
                return false;
            }

            ValueWrapperBase<T> rhs = (ValueWrapperBase<T>)obj;

            return Equals(rhs);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static bool operator ==(ValueWrapperBase<T> lhs, ValueWrapperBase<T> rhs)
        {
            if (ReferenceEquals(lhs, null))
            {
                if (ReferenceEquals(rhs, null))
                {
                    return true;
                }

                return false;
            }

            return lhs.Equals(rhs);
        }

        public static bool operator !=(ValueWrapperBase<T> lhs, ValueWrapperBase<T> rhs)
        {
            return !(lhs == rhs);
        }

        public override string ToString()
        {
            if (false == IsValid)
            {
                return InvalidDataText;
            }

            return Value.ToString();
        }

        #endregion
    }

    public abstract class ValueWrapper<T> :
        ValueWrapperBase<T>,
        IComparable,
        IComparable<ValueWrapper<T>>
        where T : IComparable, IComparable<T>
    {
        protected ValueWrapper(T value)
            : base(value)
        {

        }

        public int CompareTo(object obj)
        {
            if (obj is ValueWrapper<T> rhs)
            {
                return CompareTo(rhs);
            }

            return -1;
        }

        public int CompareTo(ValueWrapper<T> rhs)
        {
            if (Value is IComparable<T> thisVal)
            {
                return thisVal.CompareTo(rhs.Value);
            }

            return -1;
        }
    }

    public abstract class ValueWrapperNullable<T> :
        ValueWrapperBase<T?>,
        IComparable,
        IComparable<ValueWrapperNullable<T>>
        where T : struct, IComparable, IComparable<T>
    {
        protected ValueWrapperNullable(T? value)
            : base(value)
        {

        }

        public int CompareTo(object obj)
        {
            if (obj is ValueWrapperNullable<T> rhs)
            {
                return CompareTo(rhs);
            }

            return -1;
        }

        public int CompareTo(ValueWrapperNullable<T> rhs)
        {
            return Nullable.Compare(Value, rhs.Value);
        }
    }
}
