﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace FlowFramework.Model
{
    /// <summary>
    /// MACアドレス構造体
    /// </summary>
    [Serializable]
    public struct MACAddress : IValidatable, IXmlSerializable, IEquatable<MACAddress>
    {
        private byte m_oct0;
        private byte m_oct1;
        private byte m_oct2;
        private byte m_oct3;
        private byte m_oct4;
        private byte m_oct5;

        /// <summary>
        /// 第1オクテット
        /// </summary>
        public byte Oct0 => m_oct0;

        /// <summary>
        /// 第2オクテット
        /// </summary>
        public byte Oct1 => m_oct1;

        /// <summary>
        /// 第3オクテット
        /// </summary>
        public byte Oct2 => m_oct2;

        /// <summary>
        /// 第4オクテット
        /// </summary>
        public byte Oct3 => m_oct3;

        /// <summary>
        /// 第5オクテット
        /// </summary>
        public byte Oct4 => m_oct4;

        /// <summary>
        /// 第6オクテット
        /// </summary>
        public byte Oct5 => m_oct5;

        /// <summary>
        /// 有効状態
        /// </summary>
        public bool IsValid => (true);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="oct0">0オクテット</param>
        /// <param name="oct1">1オクテット</param>
        /// <param name="oct2">2オクテット</param>
        /// <param name="oct3">3オクテット</param>
        /// <param name="oct4">4オクテット</param>
        /// <param name="oct5">5オクテット</param>
        public MACAddress(byte oct0, byte oct1, byte oct2, byte oct3, byte oct4, byte oct5)
        {
            m_oct0 = oct0;
            m_oct1 = oct1;
            m_oct2 = oct2;
            m_oct3 = oct3;
            m_oct4 = oct4;
            m_oct5 = oct5;
        }

        /// <summary>
        /// 文字列へ変換する処理
        /// </summary>
        /// <returns>文字列</returns>
        public override string ToString()
        {
             return $"{m_oct0:X2}:{m_oct1:X2}:{m_oct2:X2}:{m_oct3:X2}:{m_oct4:X2}:{m_oct5:X2}";
        }

        /// <summary>
        /// イコールチェック処理
        /// </summary>
        /// <param name="obj">右辺</param>
        /// <returns>イコール結果</returns>
        public override bool Equals(object obj)
        {
            return (obj is MACAddress mac) && Equals(mac);
        }

        /// <summary>
        /// イコールチェック処理
        /// </summary>
        /// <param name="other">右辺</param>
        /// <returns>イコール結果</returns>
        public bool Equals(MACAddress other)
        {
            return (m_oct0 == other.m_oct0) &&
                   (m_oct1 == other.m_oct1) &&
                   (m_oct2 == other.m_oct2) &&
                   (m_oct3 == other.m_oct3) &&
                   (m_oct4 == other.m_oct4) &&
                   (m_oct5 == other.m_oct5);
        }

        /// <summary>
        /// ハッシュコード計算する処理
        /// </summary>
        /// <returns>ハッシュコード</returns>
        public override int GetHashCode()
        {
            var hashCode = 603471138;
            hashCode = hashCode * -1521134295 + m_oct0.GetHashCode();
            hashCode = hashCode * -1521134295 + m_oct1.GetHashCode();
            hashCode = hashCode * -1521134295 + m_oct2.GetHashCode();
            hashCode = hashCode * -1521134295 + m_oct3.GetHashCode();
            hashCode = hashCode * -1521134295 + m_oct4.GetHashCode();
            hashCode = hashCode * -1521134295 + m_oct5.GetHashCode();
            return hashCode;
        }

        /// <summary>
        /// イコールオペレーター
        /// </summary>
        /// <param name="pv1">左辺</param>
        /// <param name="pv2">右辺</param>
        /// <returns>イコール結果</returns>
        public static bool operator ==(MACAddress pv1, MACAddress pv2)
        {
            return pv1.Equals(pv2);
        }

        public static bool operator !=(MACAddress pv1, MACAddress pv2)
        {
            return !(pv1 == pv2);
        }

        /// <summary>
        /// 文字列からMACアドレスを生成する処理
        /// </summary>
        /// <param name="macAddress">MACアドレス文字列</param>
        /// <param name="result">計算結果対象</param>
        /// <returns>
        /// 処理結果：
        /// 成功：true
        /// 失敗：false
        /// </returns>
        public static bool TryParse(string macAddress, out MACAddress result)
        {
            macAddress = macAddress ?? throw new NullReferenceException(nameof(macAddress));

            char sep;
            if (macAddress[2] == ':')
            {
                sep = ':';
            }
            else if (macAddress[2] == '-')
            {
                sep = '-';
            }
            else
            {
                result = default;
                return false;
            }

            // MACアドレスを6オクテットに変換
            string[] arrOctets = macAddress.Split(sep);

            // オクテットが6つでな場合はデフォルトのMACアドレスを返す
            if (arrOctets.Length != 6)
            {
                result = default;
                return false;
            }

            // 第1クオテットをバイト列に変換不可の場合はデフォルトのMACアドレスを返す
            if (false == byte.TryParse(arrOctets[0], NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte oct0))
            {
                result = default;
                return false;
            }

            // 第2クオテットをバイト列に変換不可の場合はデフォルトのMACアドレスを返す
            if (false == byte.TryParse(arrOctets[1], NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte oct1))
            {
                result = default;
                return false;
            }

            // 第3クオテットをバイト列に変換不可の場合はデフォルトのMACアドレスを返す
            if (false == byte.TryParse(arrOctets[2], NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte oct2))
            {
                result = default;
                return false;
            }

            // 第4クオテットをバイト列に変換不可の場合はデフォルトのMACアドレスを返す
            if (false == byte.TryParse(arrOctets[3], NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte oct3))
            {
                result = default;
                return false;
            }

            // 第5クオテットをバイト列に変換不可の場合はデフォルトのMACアドレスを返す
            if (false == byte.TryParse(arrOctets[4], NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte oct4))
            {
                result = default;
                return false;
            }

            // 第6クオテットをバイト列に変換不可の場合はデフォルトのMACアドレスを返す
            if (false == byte.TryParse(arrOctets[5], NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out byte oct5))
            {
                result = default;
                return false;
            }

            // 全てのオクテットをバイト列に変換した場合、MACアドレス生成する
            result = new MACAddress(oct0, oct1, oct2, oct3, oct4, oct5);
            return true;
        }

        /// <summary>
        /// 文字列からMACアドレスを生成する処理
        /// </summary>
        /// <param name="macAddress">MACアドレス文字列</param>
        /// <returns>計算結果対象</returns>
        public static MACAddress Parse(string macAddress)
        {
            // 文字列からMACアドレスを生成する
            if (false == TryParse(macAddress ?? throw new NullReferenceException(nameof(macAddress)), out MACAddress result))
            {
                throw new FormatException(macAddress);
            }

            return result;
        }

        #region IXmlSerializable

        /// <summary>
        /// XmlSerializer用ダミー処理
        /// </summary>
        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Xml構成からMACAddressを生成する処理
        /// </summary>
        /// <param name="reader">XMLリーダー</param>
        public void ReadXml(XmlReader reader)
        {
            if (reader.MoveToContent() == XmlNodeType.Element)
            {
                // 設定ファイルからMACアドレスを読み出す
                var val = reader.ReadElementContentAsString();

                // 読みだした値がnullの場合はMACアドレスを生成する
                if (null == val)
                {
                    this = new MACAddress();
                }
                // 読みだした値がnullでない場合は文字列からMACアドレスを生成する
                else
                {
                    this = Parse(val);
                }
            }
        }

        /// <summary>
        /// MACAddressからXml構成を生成する処理
        /// </summary>
        /// <param name="writer">XMLライター</param>
        public void WriteXml(XmlWriter writer)
        {
            // MACアドレスを文字列に変換し、設定ファイルに保存
            writer.WriteValue(ToString());
        }

        #endregion
    }
}
