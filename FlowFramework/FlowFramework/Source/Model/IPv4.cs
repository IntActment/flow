﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace FlowFramework.Model
{
    /// <summary>
    /// IPアドレス構造体
    /// </summary>
    [Serializable]
    public struct IPv4 : IValidatable, IXmlSerializable, IEquatable<IPv4>
    {
        private byte m_oct0;
        private byte m_oct1;
        private byte m_oct2;
        private byte m_oct3;

        /// <summary>
        /// 第1オクテット
        /// </summary>
        public byte Oct0 => m_oct0;

        /// <summary>
        /// 第2オクテット
        /// </summary>
        public byte Oct1 => m_oct1;

        /// <summary>
        /// 第3オクテット
        /// </summary>
        public byte Oct2 => m_oct2;

        /// <summary>
        /// 第4オクテット
        /// </summary>
        public byte Oct3 => m_oct3;

        /// <summary>
        /// IPAddressへ変換
        /// </summary>
        public IPAddress IPAddress => new IPAddress(new byte[] { m_oct0, m_oct1, m_oct2, m_oct3 });

        /// <summary>
        /// 有効状態
        /// </summary>
        public bool IsValid => ((m_oct0 >= 1) && (m_oct0 <= 223));

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="oct0">0オクテット</param>
        /// <param name="oct1">1オクテット</param>
        /// <param name="oct2">2オクテット</param>
        /// <param name="oct3">3オクテット</param>
        public IPv4(byte oct0, byte oct1, byte oct2, byte oct3)
        {
            m_oct0 = oct0;
            m_oct1 = oct1;
            m_oct2 = oct2;
            m_oct3 = oct3;
        }

        /// <summary>
        /// 文字列へ変換する処理
        /// </summary>
        /// <returns>文字列</returns>
        public override string ToString()
        {
             return $"{m_oct0}.{m_oct1}.{m_oct2}.{m_oct3}";
        }

        /// <summary>
        /// イコールチェック処理
        /// </summary>
        /// <param name="obj">右辺</param>
        /// <returns>イコール結果</returns>
        public override bool Equals(object obj)
        {
            return (obj is IPv4 ip) && Equals(ip);
        }

        /// <summary>
        /// イコールチェック処理
        /// </summary>
        /// <param name="other">右辺</param>
        /// <returns>イコール結果</returns>
        public bool Equals(IPv4 other)
        {
            return (m_oct0 == other.m_oct0) &&
                   (m_oct1 == other.m_oct1) &&
                   (m_oct2 == other.m_oct2) &&
                   (m_oct3 == other.m_oct3);
        }

        /// <summary>
        /// ハッシュコード計算する処理
        /// </summary>
        /// <returns>ハッシュコード</returns>
        public override int GetHashCode()
        {
            var hashCode = 603471138;
            hashCode = hashCode * -1521134295 + m_oct0.GetHashCode();
            hashCode = hashCode * -1521134295 + m_oct1.GetHashCode();
            hashCode = hashCode * -1521134295 + m_oct2.GetHashCode();
            hashCode = hashCode * -1521134295 + m_oct3.GetHashCode();
            return hashCode;
        }

        /// <summary>
        /// イコールオペレーター
        /// </summary>
        /// <param name="pv1">左辺</param>
        /// <param name="pv2">右辺</param>
        /// <returns>イコール結果</returns>
        public static bool operator ==(IPv4 pv1, IPv4 pv2)
        {
            return pv1.Equals(pv2);
        }

        public static bool operator !=(IPv4 pv1, IPv4 pv2)
        {
            return !(pv1 == pv2);
        }

        /// <summary>
        /// 文字列からIPアドレスを生成する処理
        /// </summary>
        /// <param name="ipAddress">IPアドレス文字列</param>
        /// <param name="result">計算結果対象</param>
        /// <returns>
        /// 処理結果：
        /// 成功：true
        /// 失敗：false
        /// </returns>
        public static bool TryParse(string ipAddress, out IPv4 result)
        {
            ipAddress = ipAddress ?? throw new NullReferenceException(nameof(ipAddress));

            // IPアドレスを4オクテットに変換
            string[] arrOctets = ipAddress.Split('.');

            // オクテットが4つでな場合はデフォルトのIPアドレスを返す
            if (arrOctets.Length != 4)
            {
                result = default;
                return false;
            }

            // 第1クオテットをバイト列に変換不可の場合はデフォルトのIPアドレスを返す
            if (false == byte.TryParse(arrOctets[0], out byte oct0))
            {
                result = default;
                return false;
            }

            // 第2クオテットをバイト列に変換不可の場合はデフォルトのIPアドレスを返す
            if (false == byte.TryParse(arrOctets[1], out byte oct1))
            {
                result = default;
                return false;
            }

            // 第3クオテットをバイト列に変換不可の場合はデフォルトのIPアドレスを返す
            if (false == byte.TryParse(arrOctets[2], out byte oct2))
            {
                result = default;
                return false;
            }

            // 第4クオテットをバイト列に変換不可の場合はデフォルトのIPアドレスを返す
            if (false == byte.TryParse(arrOctets[3], out byte oct3))
            {
                result = default;
                return false;
            }

            // 全てのオクテットをバイト列に変換した場合、IPアドレス生成する
            result = new IPv4(oct0, oct1, oct2, oct3);
            return true;
        }

        /// <summary>
        /// 文字列からIPアドレスを生成する処理
        /// </summary>
        /// <param name="ipAddress">IPアドレス文字列</param>
        /// <returns>計算結果対象</returns>
        public static IPv4 Parse(string ipAddress)
        {
            // 文字列からIPアドレスを生成する
            if (false == TryParse(ipAddress ?? throw new NullReferenceException(nameof(ipAddress)), out IPv4 result))
            {
                throw new FormatException(ipAddress);
            }

            return result;
        }

        #region IXmlSerializable

        /// <summary>
        /// XmlSerializer用ダミー処理
        /// </summary>
        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Xml構成からIPv4を生成する処理
        /// </summary>
        /// <param name="reader">XMLリーダー</param>
        public void ReadXml(XmlReader reader)
        {
            if (reader.MoveToContent() == XmlNodeType.Element)
            {
                // 設定ファイルからIPアドレスを読み出す
                var val = reader.ReadElementContentAsString();

                // 読みだした値がnullの場合はIPアドレスを生成する
                if (null == val)
                {
                    this = new IPv4();
                }
                // 読みだした値がnullでない場合は文字列からIPアドレスを生成する
                else
                {
                    this = Parse(val);
                }
            }
        }

        /// <summary>
        /// IPv4からXml構成を生成する処理
        /// </summary>
        /// <param name="writer">XMLライター</param>
        public void WriteXml(XmlWriter writer)
        {
            // IPアドレスを文字列に変換し、設定ファイルに保存
            writer.WriteValue(ToString());
        }

        #endregion
    }
}
