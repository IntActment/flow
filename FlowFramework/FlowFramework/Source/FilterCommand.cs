﻿using System;

namespace FlowFramework
{
    public interface IFilterCommand<T>
    {
        bool CanPassValue(in T val);
    }

    public class FilterCommand<T> : IFilterCommand<T>
    {
        public delegate bool CanPassValueDelegate(in T val);

        protected CanPassValueDelegate m_filter;

        public FilterCommand(CanPassValueDelegate canPassValue)
        {
            m_filter = canPassValue ?? throw new NullReferenceException(nameof(canPassValue));
        }

        public bool CanPassValue(in T val)
        {
            try
            {
                return m_filter(in val);
            }
            catch
            {
                return false;
            }
        }
    }
}
